FROM maven:3-openjdk-18
ADD target/*.jar is-project-2.jar
ENTRYPOINT ["java", "-jar", "is-project-2.jar"]
EXPOSE 8080