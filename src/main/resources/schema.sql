DROP TABLE IF EXISTS professors;
DROP TABLE IF EXISTS students;
DROP TABLE IF EXISTS relationships;
CREATE TABLE IF NOT EXISTS professors (id SERIAL PRIMARY KEY, name VARCHAR(255));
CREATE TABLE IF NOT EXISTS students (id SERIAL PRIMARY KEY, name VARCHAR(255), birth_date varchar(64), completed_credits int, average_grade numeric(16, 3));
CREATE TABLE IF NOT EXISTS relationships (id SERIAL PRIMARY KEY, professor_id int not null, student_id int not null);