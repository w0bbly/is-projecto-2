INSERT INTO professors (id,name)
VALUES
    (1,'Dieter Caldwell'),
    (2,'Tanek Meyer'),
    (3,'Irma Merrill'),
    (4,'Merrill Burke'),
    (5,'Cody Mccarthy'),
    (6,'Ciaran Key'),
    (7,'Callie Mclean'),
    (8,'Josephine Jenkins'),
    (9,'Kylan Mcpherson'),
    (10,'Neil Mcdowell');
INSERT INTO professors (id,name)
VALUES
    (11,'Fiona Molina'),
    (12,'Chaim Wade'),
    (13,'Cailin Pollard'),
    (14,'Alec Brock'),
    (15,'Chiquita Mckenzie'),
    (16,'Bianca Blake'),
    (17,'Jael Rutledge'),
    (18,'Justine Dickson'),
    (19,'Kasper Kramer'),
    (20,'Burton Nash');
INSERT INTO professors (id,name)
VALUES
    (21,'Jillian Mullins'),
    (22,'Edward Salazar'),
    (23,'Candace Brown'),
    (24,'Ferdinand Knapp'),
    (25,'Adam Kramer'),
    (26,'Ashton Rodgers'),
    (27,'Brielle Whitehead'),
    (28,'Samuel Evans'),
    (29,'Ethan Schultz'),
    (30,'Isaac Figueroa');
INSERT INTO professors (id,name)
VALUES
    (31,'Lev Witt'),
    (32,'Ali Mueller'),
    (33,'Morgan Mooney'),
    (34,'Basil Todd'),
    (35,'Judith Kemp'),
    (36,'Adrienne Barrera'),
    (37,'Echo Mosley'),
    (38,'Alika Garner'),
    (39,'Courtney Schroeder'),
    (40,'Elliott French');
INSERT INTO professors (id,name)
VALUES
    (41,'Axel Nichols'),
    (42,'Nora Cochran'),
    (43,'Reagan Palmer'),
    (44,'Martena Benson'),
    (45,'Amir Brock'),
    (46,'Indira Parsons'),
    (47,'Nora Landry'),
    (48,'Hamilton Doyle'),
    (49,'Hilda Lee'),
    (50,'Reuben Simon');
INSERT INTO professors (id,name)
VALUES
    (51,'Wang Logan'),
    (52,'Hop Salas'),
    (53,'Zachery Velazquez'),
    (54,'Robin Park'),
    (55,'Julian Pittman'),
    (56,'Yoko Cline'),
    (57,'Diana Charles'),
    (58,'Fiona Munoz'),
    (59,'Josiah Hodge'),
    (60,'Darrel Noel');
INSERT INTO professors (id,name)
VALUES
    (61,'Stuart Cruz'),
    (62,'Chantale Pierce'),
    (63,'Timon Hernandez'),
    (64,'Christine Beck'),
    (65,'Quamar Grimes'),
    (66,'Angela Booker'),
    (67,'Mira Woodward'),
    (68,'Fallon Campos'),
    (69,'Harper Holt'),
    (70,'Trevor Noble');
INSERT INTO professors (id,name)
VALUES
    (71,'Evelyn Conrad'),
    (72,'Brian Lancaster'),
    (73,'Quamar Molina'),
    (74,'Zephania Griffin'),
    (75,'Whitney Crosby'),
    (76,'Hilda Trevino'),
    (77,'Magee Gardner'),
    (78,'Beverly Mckenzie'),
    (79,'Dolan Phillips'),
    (80,'Inez Holden');
INSERT INTO professors (id,name)
VALUES
    (81,'Zelenia Wong'),
    (82,'Piper Pruitt'),
    (83,'Maite Kim'),
    (84,'Quamar Garcia'),
    (85,'Jasper Fields'),
    (86,'Baxter Mckay'),
    (87,'Allistair Duffy'),
    (88,'Solomon Skinner'),
    (89,'Maris Bean'),
    (90,'Thaddeus Dean');
INSERT INTO professors (id,name)
VALUES
    (91,'Imelda Moon'),
    (92,'Xanthus Meyers'),
    (93,'Kevyn Mays'),
    (94,'Zephr Ayers'),
    (95,'Donna Dunlap'),
    (96,'Lee Brewer'),
    (97,'Hayley Livingston'),
    (98,'Rinah Salinas'),
    (99,'Josiah Baxter'),
    (100,'Brendan Blackburn');
INSERT INTO professors (id,name)
VALUES
    (101,'Ezra Kirk'),
    (102,'Raya Blankenship'),
    (103,'Joy Thompson'),
    (104,'Zenaida Browning'),
    (105,'Raya Doyle'),
    (106,'Julie Stewart'),
    (107,'Winifred Haley'),
    (108,'Aiko Pollard'),
    (109,'Leigh Villarreal'),
    (110,'Addison Cruz');
INSERT INTO professors (id,name)
VALUES
    (111,'Kay Glover'),
    (112,'Wang Cline'),
    (113,'Charity Morrow'),
    (114,'Leo Dillard'),
    (115,'Zahir Simmons'),
    (116,'Roary Doyle'),
    (117,'Keegan Rose'),
    (118,'Roary Mccormick'),
    (119,'Zenaida Wheeler'),
    (120,'Michael Weeks');
INSERT INTO professors (id,name)
VALUES
    (121,'Alfonso Patton'),
    (122,'Gavin Daugherty'),
    (123,'Madison Barnes'),
    (124,'Grady Bean'),
    (125,'Burke Donaldson'),
    (126,'Richard Lamb'),
    (127,'Rhoda Lindsey'),
    (128,'Athena Holt'),
    (129,'Cassandra Wiley'),
    (130,'Jacob Floyd');
INSERT INTO professors (id,name)
VALUES
    (131,'Lara Schmidt'),
    (132,'Daquan Wyatt'),
    (133,'Orla Riley'),
    (134,'Fitzgerald Perez'),
    (135,'Warren Ruiz'),
    (136,'Shana Mueller'),
    (137,'Avram Rivas'),
    (138,'Olga Mcdonald'),
    (139,'Emery Bullock'),
    (140,'Ivy Duran');
INSERT INTO professors (id,name)
VALUES
    (141,'Laura Anthony'),
    (142,'Jane Reed'),
    (143,'Glenna Mccoy'),
    (144,'Marshall Greer'),
    (145,'Brett Wilson'),
    (146,'Kennedy Tyson'),
    (147,'Kai Sutton'),
    (148,'Jade Rosales'),
    (149,'Carl Armstrong'),
    (150,'Sybil Cunningham');
INSERT INTO professors (id,name)
VALUES
    (151,'Nash Sharp'),
    (152,'Farrah Morse'),
    (153,'Carlos Salas'),
    (154,'Geoffrey Garza'),
    (155,'Lenore Douglas'),
    (156,'Paki Johnston'),
    (157,'Cheyenne Kerr'),
    (158,'Daphne Roach'),
    (159,'Nyssa Hodge'),
    (160,'Colorado Hopkins');
INSERT INTO professors (id,name)
VALUES
    (161,'Paloma Evans'),
    (162,'Brooke Mccormick'),
    (163,'Keith Mullen'),
    (164,'Kieran Stevenson'),
    (165,'Rosalyn Boyd'),
    (166,'Chancellor Meyers'),
    (167,'Amena Blanchard'),
    (168,'Dorian Hanson'),
    (169,'Irene Reeves'),
    (170,'Xander Kinney');
INSERT INTO professors (id,name)
VALUES
    (171,'Thane Lynn'),
    (172,'Valentine Lott'),
    (173,'Colt Brooks'),
    (174,'Luke Whitfield'),
    (175,'Amanda Kerr'),
    (176,'Cara Vaughan'),
    (177,'Arsenio Mooney'),
    (178,'Hayes Curtis'),
    (179,'Signe Bolton'),
    (180,'Dora Giles');
INSERT INTO professors (id,name)
VALUES
    (181,'Tamara Pollard'),
    (182,'Zelenia Stein'),
    (183,'Lucius Patrick'),
    (184,'Penelope Christian'),
    (185,'Mufutau Hays'),
    (186,'Melanie Ewing'),
    (187,'Hyacinth Smith'),
    (188,'Aphrodite Castro'),
    (189,'Ocean Branch'),
    (190,'Adrienne Mann');
INSERT INTO professors (id,name)
VALUES
    (191,'Kirk French'),
    (192,'Riley Powell'),
    (193,'Clark Gamble'),
    (194,'Hunter Foster'),
    (195,'Amity Lewis'),
    (196,'May Mclean'),
    (197,'Bevis Strickland'),
    (198,'Basil Massey'),
    (199,'Driscoll Delgado'),
    (200,'Indira Dixon');
INSERT INTO professors (id,name)
VALUES
    (201,'Brenda Dawson'),
    (202,'Nissim Jarvis'),
    (203,'Kaseem Ray'),
    (204,'Ima Roach'),
    (205,'Lydia Carney'),
    (206,'Alvin Velasquez'),
    (207,'Leonard Osborn'),
    (208,'Patricia Garrett'),
    (209,'Bertha Willis'),
    (210,'Daryl Browning');
INSERT INTO professors (id,name)
VALUES
    (211,'Keely Kane'),
    (212,'Coby Bradley'),
    (213,'Scarlet Whitfield'),
    (214,'Vance Mann'),
    (215,'Tara Wallace'),
    (216,'Harriet Contreras'),
    (217,'Uma Mcneil'),
    (218,'Ralph Alvarez'),
    (219,'Amela Howell'),
    (220,'Kibo Henry');
INSERT INTO professors (id,name)
VALUES
    (221,'Harrison Lynch'),
    (222,'Brianna Harrington'),
    (223,'Harlan Myers'),
    (224,'Colin Tillman'),
    (225,'Arsenio Ortega'),
    (226,'Ray England'),
    (227,'Cade Kaufman'),
    (228,'Kaseem Gilbert'),
    (229,'Dacey Kidd'),
    (230,'Olga Randall');




INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1,'Ariana Vaughn','2003-01-23 03:17:21',88,17),
    (2,'Vaughan Garner','2003-06-15 06:05:21',13,14),
    (3,'Pascale Cunningham','1996-07-27 05:17:25',110,15),
    (4,'Tamekah Vang','1998-08-09 20:05:16',6,15),
    (5,'Fallon Horne','1998-10-02 02:32:57',62,16),
    (6,'Ivan Beasley','1998-09-21 07:29:21',49,15),
    (7,'Ulysses Mendez','1997-08-26 17:32:57',175,11),
    (8,'Phelan Moore','1997-08-02 00:26:51',6,14),
    (9,'Kerry Wilcox','2000-01-29 07:17:37',166,11),
    (10,'Ivor Best','1995-06-15 06:20:26',63,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (11,'Lionel Workman','1999-10-26 05:13:57',139,14),
    (12,'Graham Sanchez','1998-07-19 00:22:07',92,17),
    (13,'Dale Wyatt','2002-09-27 02:54:25',79,17),
    (14,'Macey Potts','1999-04-30 18:09:47',142,15),
    (15,'Macey Knapp','2002-08-27 04:50:01',144,14),
    (16,'Montana Carroll','2002-01-02 16:28:41',131,20),
    (17,'Luke Peck','2003-08-22 18:32:04',17,13),
    (18,'Lev Thompson','2002-11-04 21:38:07',61,13),
    (19,'Seth Montgomery','1997-08-11 17:19:30',17,18),
    (20,'Teagan Lowe','2004-06-10 03:59:38',89,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (21,'Hadassah Walsh','2003-05-30 08:15:36',48,16),
    (22,'Basia Diaz','1996-05-02 15:47:15',6,15),
    (23,'Knox Mooney','2003-01-18 22:51:38',67,15),
    (24,'Martina Sharp','2001-07-26 11:22:11',87,17),
    (25,'Madonna Hatfield','2002-05-22 21:01:05',102,17),
    (26,'Helen Norton','1999-02-16 06:04:34',30,13),
    (27,'Shea Vega','2004-05-04 21:18:04',87,20),
    (28,'Myra Christensen','2003-11-09 06:51:05',137,19),
    (29,'Blake Pope','1999-01-07 12:40:28',36,14),
    (30,'George Hobbs','2001-06-27 21:43:20',121,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (31,'Audrey Richardson','1996-11-09 23:26:13',96,20),
    (32,'Shannon Richmond','1999-01-15 07:46:19',39,19),
    (33,'Fiona Dawson','1995-06-17 09:05:02',108,10),
    (34,'Beck Rollins','2004-01-18 02:08:46',133,11),
    (35,'Thane Clay','1999-08-12 15:38:57',70,11),
    (36,'Darius Tyson','2001-08-13 03:17:19',79,19),
    (37,'Cody Mccarty','1998-03-24 08:43:56',121,17),
    (38,'Leroy James','2001-08-22 00:54:02',128,11),
    (39,'Raymond Petersen','1999-06-21 21:33:49',118,19),
    (40,'Justina Garza','1998-04-15 01:34:11',150,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (41,'Aspen Farrell','2000-07-24 09:27:33',102,10),
    (42,'Aurora Malone','2003-02-07 13:52:51',76,12),
    (43,'Maggy Miranda','2004-02-14 21:06:40',47,16),
    (44,'Natalie Harper','1998-10-27 01:49:16',169,16),
    (45,'Luke Ball','1997-07-18 07:58:09',31,19),
    (46,'Pearl Fields','1998-11-05 07:17:20',88,11),
    (47,'Hanna Pacheco','1995-11-13 09:42:24',142,16),
    (48,'Ifeoma Boone','2004-08-05 15:52:27',127,16),
    (49,'Quinlan Holder','1996-03-05 06:34:52',172,20),
    (50,'Graiden Miranda','1996-03-09 18:15:50',73,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (51,'Allistair Sargent','2004-06-17 04:55:52',136,20),
    (52,'Hannah Mathews','2001-11-09 18:26:40',144,18),
    (53,'Dylan Clarke','1998-09-06 22:49:44',4,19),
    (54,'Alexander Pittman','2003-04-14 06:30:21',11,18),
    (55,'Savannah Mercer','2002-02-02 13:02:35',139,14),
    (56,'Stacey David','1998-05-03 02:48:58',88,18),
    (57,'Regan Morin','1996-04-14 04:50:56',155,18),
    (58,'Nicholas Battle','2003-05-14 03:10:05',66,19),
    (59,'Hop Burton','1996-06-08 04:22:45',41,10),
    (60,'Sylvia Flores','1995-04-19 09:18:33',179,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (61,'Keefe Vaughan','1999-05-17 00:03:57',125,17),
    (62,'Kasper Herrera','2000-02-04 10:16:04',111,10),
    (63,'Meredith Cortez','2003-07-07 22:13:54',17,13),
    (64,'Rae Mullen','2001-04-19 01:28:36',71,19),
    (65,'Scott Booker','1998-03-25 04:29:43',37,11),
    (66,'Louis Miller','2004-09-06 05:22:59',68,17),
    (67,'Gemma Garza','2001-08-20 11:11:19',34,14),
    (68,'Stuart Watkins','1998-07-23 20:17:38',135,18),
    (69,'Wing Cain','2004-08-20 16:20:42',65,20),
    (70,'Gray Ayers','1998-01-17 16:56:01',142,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (71,'Steven Rodgers','2004-06-11 07:33:19',4,16),
    (72,'Alexander Buck','1999-03-31 03:35:44',160,20),
    (73,'Chester Davenport','1998-08-26 12:11:45',24,10),
    (74,'Marshall Mcfadden','1997-07-28 07:08:37',21,14),
    (75,'Damon Newton','1997-02-20 22:54:50',153,11),
    (76,'James Ortega','2004-12-15 04:46:44',125,10),
    (77,'Rahim Bowen','1999-10-18 14:34:04',64,18),
    (78,'Todd Kim','2000-05-14 08:03:26',52,13),
    (79,'Chloe Poole','2002-07-15 10:14:17',89,19),
    (80,'Lucius Buckner','1996-09-02 00:43:07',18,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (81,'Hyatt Valentine','1998-05-27 05:07:46',169,17),
    (82,'Ethan Casey','2000-09-02 07:13:12',40,15),
    (83,'Chantale Lewis','2004-01-15 02:00:55',168,10),
    (84,'Stone Sims','1997-05-14 21:24:27',18,12),
    (85,'Stephanie Rhodes','1996-08-07 07:24:07',1,20),
    (86,'Darius Knapp','1996-05-09 11:52:27',46,10),
    (87,'Randall Aguirre','1997-10-28 21:37:48',81,11),
    (88,'Kirsten Riggs','1995-01-04 20:07:24',103,11),
    (89,'Vernon Wolf','2000-08-24 04:40:37',111,19),
    (90,'Zahir Howell','1998-06-24 00:42:37',73,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (91,'Alec Hanson','2000-05-24 23:00:55',72,15),
    (92,'Zeus Clay','2001-09-12 10:47:02',167,18),
    (93,'Regina Hobbs','2003-09-10 11:02:34',83,15),
    (94,'Rachel Oliver','1996-01-06 02:44:22',90,16),
    (95,'Lacy Bradley','2004-11-16 08:20:17',146,15),
    (96,'Gary Wallace','2003-10-29 01:52:49',80,17),
    (97,'Raymond Matthews','2003-08-21 10:31:52',1,13),
    (98,'Guinevere Campos','2000-11-03 10:52:10',88,12),
    (99,'Naida Harper','1995-08-13 21:31:44',137,15),
    (100,'Germane Baird','2004-07-15 00:54:49',79,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (101,'Cailin Evans','2003-06-20 08:51:02',43,16),
    (102,'Thane Steele','2001-06-29 21:12:55',128,19),
    (103,'Owen Riggs','1999-10-21 01:38:51',99,20),
    (104,'Randall Odom','1998-04-16 14:40:58',155,12),
    (105,'Brianna Vincent','1997-12-12 02:47:57',86,12),
    (106,'Philip Fitzgerald','2003-07-09 22:39:23',46,15),
    (107,'Stuart Johns','1999-02-04 06:31:34',171,18),
    (108,'Declan Gardner','2003-04-22 10:34:33',156,18),
    (109,'Addison Avila','2001-02-24 21:47:38',122,15),
    (110,'Signe Greene','2002-04-04 07:23:10',11,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (111,'Hayley Hickman','1996-02-19 08:25:01',8,13),
    (112,'Rae Whitehead','1998-08-13 13:21:11',24,15),
    (113,'Elaine Mccoy','2004-09-29 11:50:36',42,17),
    (114,'Cassandra Simmons','2004-12-27 01:30:40',72,15),
    (115,'MacKenzie Maldonado','1995-03-10 20:41:50',119,12),
    (116,'Lenore Cantrell','2003-02-27 00:13:59',173,15),
    (117,'Brenda Owens','1995-12-31 03:33:59',146,11),
    (118,'Nita Wise','1996-02-27 17:00:54',88,15),
    (119,'Sheila Gamble','1998-04-30 10:33:31',112,14),
    (120,'Lenore Hebert','2002-09-15 11:30:31',148,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (121,'Iliana Vega','1999-08-23 13:20:13',9,13),
    (122,'Reese Stewart','2000-06-15 20:47:13',141,12),
    (123,'Sylvia Rivas','1997-09-07 18:56:16',15,11),
    (124,'Fatima Wilkinson','2000-03-16 04:01:42',57,14),
    (125,'Ulla Henry','1995-01-28 20:50:09',67,14),
    (126,'Kellie Sandoval','2001-11-23 04:25:38',139,16),
    (127,'Paul Malone','1996-06-16 10:41:26',150,15),
    (128,'Laith Montoya','1996-01-29 14:38:23',111,16),
    (129,'Michelle Pate','2004-09-29 17:44:29',139,11),
    (130,'Jermaine Leach','2003-10-25 17:40:37',68,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (131,'Courtney Padilla','1995-05-17 21:07:40',42,13),
    (132,'Samson Meyer','2003-10-17 01:16:00',63,15),
    (133,'Skyler Mathews','2000-11-08 11:36:41',13,19),
    (134,'Suki Santiago','1996-07-13 18:28:57',34,15),
    (135,'Jakeem Rollins','2004-08-09 13:45:46',55,19),
    (136,'Hayes Salinas','2004-12-25 20:45:17',75,14),
    (137,'Judith England','1996-07-04 06:49:00',117,17),
    (138,'Mohammad Hoffman','2000-04-04 13:20:35',180,20),
    (139,'Kasimir Castaneda','2004-07-05 14:20:44',14,14),
    (140,'Jemima Sutton','2000-03-19 02:27:31',10,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (141,'Maia Bass','1995-02-12 11:49:16',21,17),
    (142,'Shaine Buchanan','2001-06-19 17:16:05',61,13),
    (143,'Gillian Serrano','1995-02-16 23:57:20',70,20),
    (144,'Jayme Beach','2003-04-28 02:01:17',83,14),
    (145,'Alexander Banks','1995-12-09 20:35:57',104,10),
    (146,'Danielle White','2004-04-04 00:44:13',131,10),
    (147,'Hiroko England','1996-02-29 21:15:31',47,13),
    (148,'May Hays','1997-09-04 00:29:41',127,14),
    (149,'Belle Robbins','1998-08-24 13:10:48',137,19),
    (150,'Vladimir Glenn','2000-05-08 16:12:00',138,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (151,'Brock Avery','1997-02-20 22:07:01',89,14),
    (152,'Lavinia Swanson','1996-11-11 20:02:17',22,18),
    (153,'Gisela Guthrie','2003-11-16 10:35:25',35,15),
    (154,'Tallulah Mckee','2002-02-13 18:15:11',40,17),
    (155,'Elliott Le','2002-12-13 03:33:47',36,19),
    (156,'April Harris','1996-06-04 13:05:50',163,15),
    (157,'Tana Black','1999-10-02 23:23:23',5,18),
    (158,'Darius Mckay','2000-01-04 16:27:05',11,10),
    (159,'Kiona Booker','2004-04-04 20:13:58',12,13),
    (160,'Ronan Colon','1999-08-12 23:25:04',35,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (161,'Breanna Molina','2003-05-20 10:49:33',122,15),
    (162,'Jermaine Bowen','2000-03-25 22:31:27',10,14),
    (163,'Amanda Cohen','2002-05-05 20:51:57',157,10),
    (164,'Ezekiel Church','2000-03-01 02:43:16',134,19),
    (165,'Karen Mooney','2004-01-29 03:21:55',123,19),
    (166,'Camden Sherman','2002-01-09 14:30:12',68,10),
    (167,'Christopher Chandler','2004-06-05 12:23:03',61,11),
    (168,'Trevor Lawson','1995-11-13 14:41:31',121,18),
    (169,'Olivia Hahn','1996-06-26 20:50:31',90,20),
    (170,'Leilani Lowe','1995-03-06 01:04:31',113,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (171,'Leila Campbell','2001-05-19 10:37:32',101,19),
    (172,'Lael Ray','1996-12-19 14:35:54',104,17),
    (173,'Gary Hurst','2002-08-01 10:33:33',135,19),
    (174,'Melinda Casey','1995-07-23 05:58:52',167,11),
    (175,'Christopher Jacobs','1997-05-05 15:51:09',30,15),
    (176,'Bruce Church','2000-07-04 17:09:48',47,17),
    (177,'Leila Marquez','2003-06-20 02:18:08',45,18),
    (178,'Leandra Bates','1996-09-30 13:20:30',57,11),
    (179,'Igor Bradford','2004-11-11 18:26:54',74,10),
    (180,'Keane Mcdowell','1999-11-23 04:10:39',121,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (181,'Daquan Harris','2003-07-05 12:51:36',39,18),
    (182,'Quail Carr','2003-12-18 08:37:49',5,13),
    (183,'Christian Mullen','2001-03-24 02:10:37',85,15),
    (184,'Arthur Wong','1995-07-12 02:43:56',106,17),
    (185,'Maryam Callahan','1999-11-10 09:13:19',154,17),
    (186,'Galena Serrano','1999-05-31 00:08:28',30,14),
    (187,'Alika Mccormick','2000-07-11 21:15:34',145,10),
    (188,'Damian Santos','2003-07-03 03:32:51',131,15),
    (189,'Addison Henderson','1998-12-19 10:28:14',175,12),
    (190,'Xander Wooten','1997-10-11 06:49:58',165,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (191,'Maggy Silva','2001-12-24 00:22:04',87,11),
    (192,'Nevada Dejesus','2003-10-06 14:30:43',50,13),
    (193,'Darius Jordan','2000-11-11 05:33:31',63,16),
    (194,'Murphy Moran','1999-12-26 10:10:08',163,14),
    (195,'Signe Randolph','1997-05-22 15:07:34',142,15),
    (196,'Sasha Francis','1996-10-10 10:25:47',82,17),
    (197,'Byron Hewitt','1997-01-14 16:47:15',141,11),
    (198,'Merritt Blackwell','1999-04-20 02:48:19',8,17),
    (199,'Boris Banks','2000-02-05 17:44:55',167,14),
    (200,'Solomon Craft','1996-11-06 14:17:30',78,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (201,'Paul Lara','1995-12-11 05:29:27',40,15),
    (202,'Fulton Kim','1997-12-22 09:31:20',160,19),
    (203,'Rebecca Gay','1997-12-01 23:23:48',176,17),
    (204,'Paloma Solis','2003-11-01 06:48:42',72,16),
    (205,'Velma Rios','1997-09-21 15:19:10',141,11),
    (206,'Wallace Ortega','1997-05-26 17:17:55',158,17),
    (207,'Xanthus Holt','2000-02-05 22:22:06',71,20),
    (208,'Lunea Ewing','1996-11-30 22:35:03',9,18),
    (209,'Aaron Cox','1999-01-25 16:37:29',2,12),
    (210,'Martina Gomez','1997-01-22 11:47:56',25,10);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (211,'Karly Jackson','2000-09-01 08:26:27',54,15),
    (212,'Barclay Bray','1996-11-29 19:48:17',1,14),
    (213,'Colby Leonard','1998-09-04 03:13:35',46,15),
    (214,'Haley Hancock','2000-06-27 09:42:08',81,11),
    (215,'Ila Leon','2004-04-29 15:55:43',164,10),
    (216,'Honorato Bradford','1995-06-06 15:27:18',9,16),
    (217,'Merritt Evans','1998-02-14 10:09:02',80,13),
    (218,'Doris Solomon','2003-08-03 10:45:48',146,19),
    (219,'Raymond Ingram','2002-04-03 20:09:23',3,19),
    (220,'Justine Mccullough','1996-03-21 14:33:53',153,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (221,'Graiden Bennett','2004-04-04 07:28:38',126,15),
    (222,'Eugenia Finch','2004-03-07 17:15:29',170,14),
    (223,'Dexter Mcguire','2001-12-13 03:14:18',110,17),
    (224,'Maxwell Sparks','1998-04-19 23:44:54',2,11),
    (225,'McKenzie Mccoy','2001-03-09 21:53:10',12,19),
    (226,'Aristotle Allison','1998-05-22 21:29:20',153,13),
    (227,'Graham Stevens','2000-01-13 02:20:36',19,16),
    (228,'Anastasia Murphy','2001-08-29 23:49:07',110,11),
    (229,'Olivia Miller','1997-10-01 07:08:12',15,11),
    (230,'Russell Finley','2003-07-04 06:02:07',98,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (231,'Bernard Holder','1998-04-19 10:41:43',44,17),
    (232,'Kiara Mullins','1997-08-09 05:53:54',91,13),
    (233,'Ira Meadows','2003-11-20 16:40:59',66,18),
    (234,'Russell Delacruz','1999-06-16 06:58:06',23,11),
    (235,'Camille Morgan','1999-03-06 02:31:31',111,13),
    (236,'Jescie Sears','2002-08-26 04:57:48',143,15),
    (237,'Prescott Joyce','1997-12-19 03:10:59',13,10),
    (238,'Henry Cleveland','1999-05-21 14:43:23',69,12),
    (239,'Wesley Cooley','1996-01-22 08:52:32',150,19),
    (240,'Daryl Powers','1999-04-19 11:11:26',158,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (241,'Cairo Clarke','1995-10-14 01:42:15',36,12),
    (242,'Hoyt Oliver','1998-02-14 00:48:55',40,11),
    (243,'Marny Pickett','2003-06-04 14:33:13',120,14),
    (244,'Jordan Hendricks','2003-02-03 16:07:43',135,15),
    (245,'Dara Hatfield','1997-01-30 00:21:29',63,17),
    (246,'Kieran Finley','2002-10-16 09:08:42',111,13),
    (247,'Madeline Puckett','1997-05-10 03:26:24',14,16),
    (248,'Evangeline Butler','2003-12-10 23:30:17',159,14),
    (249,'Matthew Cochran','1998-11-27 15:15:50',26,19),
    (250,'Fritz Leach','1996-11-02 09:51:32',111,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (251,'Nicholas Guy','2002-01-20 23:13:22',72,11),
    (252,'Leandra Hartman','1995-04-20 11:46:19',176,19),
    (253,'Honorato Glover','2000-08-22 03:05:28',94,16),
    (254,'Castor Merrill','1999-08-09 00:53:51',13,14),
    (255,'Julie Warner','2001-01-18 21:49:32',121,19),
    (256,'Marsden Brennan','1998-05-19 19:55:18',166,11),
    (257,'Mari Walsh','1998-08-20 10:41:33',104,20),
    (258,'Malcolm Sandoval','1996-10-04 03:46:32',136,18),
    (259,'Griffith Riley','1995-09-05 19:16:17',81,13),
    (260,'Savannah Salinas','2003-07-10 23:29:01',40,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (261,'Freya Greene','2003-04-13 11:50:01',89,17),
    (262,'Emerson Chang','1998-07-22 14:24:51',136,10),
    (263,'Castor Shepard','2004-09-02 06:10:03',54,13),
    (264,'Byron Blake','1999-05-23 07:17:00',21,12),
    (265,'Calvin Meadows','2000-11-06 07:35:01',175,20),
    (266,'Larissa Gentry','2002-05-10 16:57:15',130,15),
    (267,'Leandra Stevenson','2002-10-09 23:49:10',88,17),
    (268,'Amos Rollins','1996-04-01 17:23:58',157,12),
    (269,'Doris Wallace','2002-06-23 10:47:08',140,18),
    (270,'Neil Gaines','2001-06-11 18:37:20',164,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (271,'Rooney Dyer','2000-06-22 10:23:50',54,18),
    (272,'Galena Lester','2003-06-03 20:49:38',50,19),
    (273,'Rudyard Andrews','1999-03-02 11:15:01',37,14),
    (274,'Macey Cherry','2001-07-08 14:15:20',3,15),
    (275,'Patrick Bonner','1998-05-20 03:52:56',41,13),
    (276,'Giacomo Mcpherson','1999-06-27 16:06:04',55,14),
    (277,'Zeus Schroeder','2001-07-30 20:56:42',30,16),
    (278,'Avye Bryant','1995-06-24 22:41:48',10,12),
    (279,'Patrick Goff','1996-07-05 18:36:24',65,17),
    (280,'Mikayla Lindsey','1996-01-11 16:37:30',168,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (281,'Zachary Palmer','1996-11-01 23:04:11',143,18),
    (282,'Matthew Landry','1995-09-30 14:54:50',6,17),
    (283,'Lev Rice','1998-12-12 14:42:42',157,14),
    (284,'Francis Hayes','1998-03-08 03:23:36',42,19),
    (285,'Noble Hopper','2001-05-07 18:20:36',120,20),
    (286,'Armando Blevins','1998-05-06 01:04:38',156,13),
    (287,'Kaitlin Griffith','2003-12-15 02:26:31',54,14),
    (288,'Kaseem Hickman','1996-05-28 08:03:39',82,12),
    (289,'Nola Wright','1996-09-10 12:54:38',84,18),
    (290,'Rigel David','1999-02-09 13:26:57',131,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (291,'Kenneth Roth','2000-01-13 12:59:06',136,18),
    (292,'Lavinia Ray','2004-02-21 01:25:56',86,14),
    (293,'Winifred Mcleod','2004-10-03 03:25:18',2,12),
    (294,'Barrett Rutledge','1999-10-15 05:30:55',63,10),
    (295,'Sybil House','2002-05-30 07:09:35',51,12),
    (296,'Xyla Frost','2003-09-27 15:28:49',134,12),
    (297,'Hashim Kane','2000-05-16 07:56:30',132,14),
    (298,'Duncan Ward','1996-08-18 02:02:28',12,13),
    (299,'Harlan Rodriquez','2002-11-12 16:37:51',74,14),
    (300,'Shannon England','1999-06-28 21:34:00',104,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (301,'Jaden Burt','1997-09-28 05:23:10',170,15),
    (302,'Rafael Sanford','2000-12-29 02:14:22',9,17),
    (303,'Freya Butler','1999-09-09 08:52:45',107,17),
    (304,'Carl Fry','1999-05-28 02:14:48',128,17),
    (305,'Jasmine Fry','1995-02-17 15:49:51',31,13),
    (306,'Petra Lee','1999-09-26 01:45:49',138,11),
    (307,'Quynn Walker','2000-07-01 09:12:48',161,11),
    (308,'Aimee Lindsay','1995-04-22 08:45:23',137,14),
    (309,'Kasper Sparks','2003-03-01 20:12:11',29,14),
    (310,'Sacha Brooks','1996-06-21 00:54:59',132,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (311,'Unity Hays','2001-04-25 06:38:02',31,17),
    (312,'Nero Page','2002-05-29 11:10:17',60,11),
    (313,'Deacon Miller','1999-08-26 00:51:27',21,12),
    (314,'Drake Kent','2000-11-11 08:38:38',100,12),
    (315,'Austin Townsend','2001-09-21 10:04:30',73,13),
    (316,'Lesley George','1999-04-29 08:56:26',73,13),
    (317,'Hamish Chavez','1997-10-24 20:13:04',137,15),
    (318,'Chaim Ingram','1996-12-28 18:15:13',10,10),
    (319,'Audrey Patrick','2000-07-09 14:42:07',37,18),
    (320,'Lionel Houston','1995-07-07 14:31:08',72,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (321,'Kalia Bell','1995-11-05 05:58:46',74,19),
    (322,'Maxine Simon','2000-11-28 08:50:07',128,10),
    (323,'Keefe Woodard','1999-11-26 04:41:01',32,12),
    (324,'Mufutau Wyatt','2000-09-07 05:36:30',3,11),
    (325,'Kylee Shelton','2004-10-01 16:12:17',79,12),
    (326,'Vera Barlow','2003-03-19 00:07:16',45,18),
    (327,'Flavia Spencer','2000-05-01 19:00:31',152,20),
    (328,'Maia Albert','2004-04-21 09:40:18',136,14),
    (329,'Colt Rollins','2004-04-21 18:51:15',158,16),
    (330,'Zachary Wilkerson','2001-10-09 03:26:25',26,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (331,'Coby Small','1999-08-29 12:24:45',89,18),
    (332,'Merritt Conrad','1997-08-09 20:58:24',155,12),
    (333,'Nathaniel Marquez','1999-09-02 03:00:39',143,16),
    (334,'Violet Banks','1999-03-25 17:10:29',61,18),
    (335,'May Cantu','2003-02-12 22:28:08',91,17),
    (336,'Rhoda Keller','1999-09-12 10:57:42',94,17),
    (337,'Arthur Hooper','2000-12-27 17:31:48',153,18),
    (338,'Cullen Reynolds','1995-02-08 06:17:11',72,13),
    (339,'Rajah Farley','1997-08-22 13:16:11',111,16),
    (340,'Chase Thornton','1997-11-12 06:01:23',169,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (341,'Hammett Flynn','1997-09-15 15:04:14',148,11),
    (342,'Gage Johns','2004-12-03 05:19:56',93,16),
    (343,'Wendy Alvarez','1997-10-02 18:44:27',130,13),
    (344,'Kirby Wong','1997-11-02 04:23:18',114,19),
    (345,'Charity Frost','2002-05-28 17:32:21',147,10),
    (346,'Myra Mckinney','1999-06-23 22:24:01',13,18),
    (347,'Athena Ray','1997-04-30 02:01:00',164,10),
    (348,'Laurel Ayers','1995-02-24 15:45:47',33,10),
    (349,'Dennis Leon','1995-01-27 16:10:11',166,12),
    (350,'Adrienne Mays','2001-10-23 09:10:55',22,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (351,'Vaughan Richardson','1999-04-18 08:06:56',174,12),
    (352,'Upton York','2001-08-03 08:38:50',140,10),
    (353,'Keegan Hull','2003-06-12 06:07:50',48,10),
    (354,'Phillip Chase','1996-07-22 13:45:47',155,13),
    (355,'Nasim Tran','2003-06-25 02:04:55',116,17),
    (356,'Jenette Bentley','1999-06-02 08:42:18',131,17),
    (357,'Yuri Schultz','2003-05-19 09:13:06',32,12),
    (358,'Kiona Rogers','2004-09-02 11:03:58',134,13),
    (359,'Sade Robinson','1998-09-02 18:47:57',70,11),
    (360,'Hammett Sherman','1995-02-15 19:57:13',123,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (361,'Vincent Torres','2001-07-08 02:48:08',26,17),
    (362,'Carly Hayden','1996-02-05 04:59:23',159,19),
    (363,'Sylvia Duran','1999-07-05 21:03:35',86,12),
    (364,'Louis Cooley','2003-02-15 20:50:16',148,13),
    (365,'Wallace Mcguire','2001-08-04 03:04:30',28,14),
    (366,'Todd Larsen','1996-09-14 13:48:52',13,19),
    (367,'Felix Nielsen','2003-12-05 07:26:09',39,14),
    (368,'Octavia Tillman','1999-08-06 21:17:26',102,12),
    (369,'Karina Winters','2000-09-11 22:13:42',165,18),
    (370,'August Mayo','1997-12-28 09:28:27',95,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (371,'Gray Greene','2000-01-16 05:00:34',20,19),
    (372,'Fulton Sharp','1995-12-18 22:24:40',7,10),
    (373,'Patience Frost','1996-07-04 22:09:03',65,17),
    (374,'Cairo Mills','2002-09-03 19:57:41',82,12),
    (375,'Julian Castillo','1998-01-20 07:53:41',76,16),
    (376,'Jana Gentry','2002-12-06 19:17:32',161,16),
    (377,'Denise Greene','1996-04-13 18:07:39',34,16),
    (378,'Hayfa Poole','2004-07-02 22:52:05',75,14),
    (379,'Carly Williams','1999-12-14 17:09:45',144,20),
    (380,'Fallon Spence','2001-01-22 08:24:25',95,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (381,'Richard Atkins','1997-06-27 14:13:14',129,17),
    (382,'Odette Stevenson','1995-04-13 06:19:33',24,16),
    (383,'Dane Larson','2004-07-02 04:28:32',177,12),
    (384,'Adele Davenport','2002-03-19 06:04:51',131,17),
    (385,'Tara Bauer','1996-10-28 10:51:08',175,12),
    (386,'Brielle English','1996-04-02 13:46:04',38,17),
    (387,'Armand O''brien','2001-01-01 12:24:22',123,16),
    (388,'Honorato Hudson','1996-03-05 16:33:22',103,15),
    (389,'Alice Fuller','2000-11-16 09:12:25',25,19),
    (390,'Wyatt Bauer','1995-01-02 21:10:44',22,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (391,'Baxter Myers','1999-10-21 16:24:00',106,15),
    (392,'Quin Collins','1997-07-08 23:18:30',135,10),
    (393,'Garth Rosario','1997-02-24 05:08:29',46,18),
    (394,'Demetria Briggs','2002-11-17 23:41:20',86,17),
    (395,'Patrick Larsen','2001-07-21 15:47:56',56,20),
    (396,'Jescie Benson','2000-10-21 07:20:00',111,15),
    (397,'Vanna Rojas','1996-08-12 15:54:00',133,11),
    (398,'Odysseus Rogers','1995-05-09 15:21:50',6,20),
    (399,'Vivien Medina','2001-07-04 10:36:42',45,10),
    (400,'Debra Moses','1998-05-25 08:45:25',48,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (401,'Fulton Ramos','1999-04-30 05:57:21',42,11),
    (402,'Shoshana Robertson','1997-02-16 05:33:23',41,18),
    (403,'Scarlett Hopper','2003-07-09 22:17:54',5,11),
    (404,'Yen Schwartz','2000-03-06 06:55:50',124,20),
    (405,'Jane Carter','2000-05-15 22:36:00',120,18),
    (406,'Nomlanga Oneal','1997-02-20 16:28:42',90,12),
    (407,'Quin Wiggins','1999-02-11 09:43:28',76,14),
    (408,'Chadwick Mcfadden','2000-05-16 14:11:59',147,10),
    (409,'Hayden Bruce','2004-06-08 04:26:54',147,12),
    (410,'Tobias Newton','2000-04-25 02:03:43',33,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (411,'Ezra Snow','1998-01-29 00:19:05',147,14),
    (412,'Kimberley Pennington','1998-05-09 02:47:28',165,13),
    (413,'Sybil Tate','1998-05-08 18:11:58',144,17),
    (414,'Carly Pruitt','1998-12-22 19:45:08',10,16),
    (415,'Jael Barton','1999-12-28 11:25:02',124,10),
    (416,'Maggy Roy','2000-04-21 09:23:31',172,18),
    (417,'Ginger Roth','1997-11-28 14:56:55',128,12),
    (418,'Chancellor Lancaster','2001-11-20 02:04:41',164,17),
    (419,'Nichole Randall','2000-06-17 04:17:31',103,19),
    (420,'Britanni Maddox','2000-08-05 09:46:02',31,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (421,'Leonard Cox','1997-10-15 21:25:25',119,13),
    (422,'Stuart Castillo','2004-04-03 11:34:54',80,19),
    (423,'Marsden Pratt','2001-10-02 10:55:59',29,19),
    (424,'Gage Lee','2002-03-18 23:13:24',110,15),
    (425,'Yeo Scott','1997-07-16 07:44:01',43,13),
    (426,'Ira Dudley','1999-12-24 15:04:30',39,16),
    (427,'Ocean Fischer','2001-05-03 14:56:09',10,15),
    (428,'Fletcher Skinner','2003-04-10 01:18:00',53,18),
    (429,'Stuart Tate','1998-06-05 12:03:53',165,20),
    (430,'Keaton Pollard','2001-03-07 21:03:59',21,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (431,'Ulric Wilson','2004-01-09 14:32:29',99,18),
    (432,'Keely Walton','1998-09-18 03:42:51',144,17),
    (433,'Arsenio Bowen','2000-05-15 05:02:22',102,17),
    (434,'Chancellor Burris','2004-11-26 10:47:10',37,19),
    (435,'Aretha Fernandez','1996-05-18 14:00:00',130,14),
    (436,'Leonard Rivers','2004-10-25 11:08:49',139,16),
    (437,'Jackson Petersen','1999-10-14 03:04:20',17,16),
    (438,'Georgia Johnson','1999-02-04 17:30:25',148,13),
    (439,'Quynn Middleton','1995-11-30 20:20:52',28,15),
    (440,'Ronan Erickson','2002-02-19 11:43:04',52,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (441,'Owen Dudley','2004-11-15 05:07:15',129,16),
    (442,'Armand Craig','1996-10-02 12:05:54',86,16),
    (443,'Lucas Peck','2000-10-04 03:50:24',0,19),
    (444,'Phoebe Burgess','2000-03-01 04:32:03',161,11),
    (445,'Tate Case','1998-08-05 07:45:11',173,13),
    (446,'Leigh Webb','2000-08-06 13:13:19',175,14),
    (447,'Breanna Lynch','1997-08-31 23:46:54',125,10),
    (448,'Tyler Pollard','2000-10-09 00:37:28',162,13),
    (449,'Tara Perkins','2000-09-12 23:58:37',38,15),
    (450,'Hedy Sykes','1996-05-15 12:50:25',22,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (451,'Leo Vincent','1998-06-06 05:53:14',134,18),
    (452,'Amy Reeves','2004-08-01 12:09:11',17,13),
    (453,'Quin Holloway','1995-05-27 12:25:25',4,19),
    (454,'Harriet Stephens','2004-12-02 03:47:10',93,16),
    (455,'Pandora Marks','1997-02-09 04:57:18',133,18),
    (456,'Jared Adams','1997-06-26 17:48:56',23,17),
    (457,'Quin Black','1998-03-13 09:56:55',27,19),
    (458,'Debra Chapman','1995-12-27 02:22:13',17,12),
    (459,'Graiden Horton','1999-11-28 03:02:08',125,19),
    (460,'Allistair Lancaster','1997-01-16 00:48:51',110,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (461,'Justine Gamble','1995-02-13 09:25:00',32,19),
    (462,'Lacy Webster','2003-10-26 13:00:18',73,17),
    (463,'Kenneth Cleveland','2000-02-14 12:21:58',134,15),
    (464,'Cameron Rollins','2003-10-31 18:56:17',63,19),
    (465,'Kato Ashley','1995-01-13 10:15:08',34,11),
    (466,'William Carr','2004-05-03 02:44:31',73,17),
    (467,'Lani George','2002-02-18 09:12:18',10,13),
    (468,'Martin Albert','2002-09-02 09:36:47',14,12),
    (469,'Malachi Zamora','2000-03-30 12:01:51',68,15),
    (470,'Shana Merritt','2001-01-03 23:16:34',144,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (471,'Jaden Cook','1996-03-18 19:31:55',162,12),
    (472,'Alexandra Bonner','2004-01-05 10:19:15',149,18),
    (473,'Darius Golden','1996-09-06 02:40:06',175,13),
    (474,'Ina Hale','2004-11-16 15:17:10',168,15),
    (475,'Charity Wolfe','1997-06-21 08:13:17',48,18),
    (476,'Mannix Mann','1999-07-02 07:49:47',124,14),
    (477,'Sopoline Garrett','1997-05-21 08:30:28',71,17),
    (478,'Micah Thornton','1995-09-23 13:38:46',32,20),
    (479,'Ivy Stout','2002-11-23 21:04:50',148,16),
    (480,'Renee Vaughan','1999-07-16 13:53:04',150,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (481,'Erin Gentry','2000-02-01 02:32:17',86,12),
    (482,'Zephania Padilla','1996-06-13 21:22:51',64,11),
    (483,'Camille Hobbs','1998-08-15 22:51:44',70,20),
    (484,'Genevieve Alvarez','2000-04-26 12:26:54',45,17),
    (485,'Evelyn England','2001-12-23 07:59:35',135,16),
    (486,'Cody Bullock','2003-01-26 11:35:12',57,15),
    (487,'Vance Barker','1996-05-11 12:55:54',4,11),
    (488,'Byron Moore','1999-02-06 03:58:21',105,12),
    (489,'Gary Beach','1998-12-26 22:12:24',98,13),
    (490,'August Meadows','1996-11-03 03:25:40',13,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (491,'Aphrodite Floyd','1999-06-03 17:13:40',152,12),
    (492,'Cedric Hardin','2001-09-26 21:15:49',113,17),
    (493,'Bruno Wynn','1997-01-14 20:05:50',126,15),
    (494,'Ruth Flores','1996-05-10 05:02:10',142,12),
    (495,'Phillip Matthews','2003-08-20 12:34:07',29,20),
    (496,'Nicole Greene','2000-01-10 08:29:17',73,18),
    (497,'Kenneth Larson','1996-02-03 22:08:27',88,15),
    (498,'Hilary Hess','2001-03-03 08:32:10',105,15),
    (499,'Xenos Ewing','1995-11-30 06:49:22',69,14),
    (500,'Cole Berry','1997-02-08 09:40:00',15,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (501,'Ciara Anderson','1997-12-17 15:49:33',129,19),
    (502,'Herman Gutierrez','1999-07-24 11:35:21',29,16),
    (503,'Noble Charles','2000-12-30 00:33:26',43,15),
    (504,'Amela Carr','2002-07-14 18:24:30',135,18),
    (505,'Noelle Montgomery','2003-11-04 03:24:56',141,18),
    (506,'Hu Fox','2003-06-19 16:57:23',52,20),
    (507,'Abraham Hunter','2001-12-27 10:12:02',54,13),
    (508,'Chiquita Combs','2002-05-23 23:29:40',53,14),
    (509,'Adria Stout','1996-07-14 07:33:44',16,17),
    (510,'Audra Cabrera','1999-05-07 12:12:37',141,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (511,'Zenaida Meadows','2003-11-15 18:34:10',107,12),
    (512,'Felicia Martinez','2001-10-16 09:53:03',107,12),
    (513,'Faith Cohen','2002-10-26 08:47:48',64,13),
    (514,'Dahlia Sykes','1995-05-24 00:21:07',65,16),
    (515,'Fallon Mathis','1996-06-14 23:13:16',56,19),
    (516,'Hayden Hammond','2001-04-07 21:52:25',140,15),
    (517,'Ferdinand Woodward','2002-04-10 11:16:43',111,11),
    (518,'Branden Harmon','2003-01-18 04:41:49',90,13),
    (519,'Moses Short','2002-09-04 08:24:10',9,17),
    (520,'Armando Blankenship','2000-05-14 10:54:15',23,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (521,'Dominic Chen','1995-10-29 11:18:04',6,10),
    (522,'Hiroko Randall','2001-03-19 11:29:19',3,16),
    (523,'Jane Yang','1998-10-16 21:37:52',75,19),
    (524,'Silas Nicholson','1995-05-23 20:02:33',55,18),
    (525,'Mannix Horn','2000-05-18 23:33:08',177,11),
    (526,'Alec Foreman','2002-11-06 00:01:28',9,14),
    (527,'Asher Griffith','1999-08-21 22:52:07',11,16),
    (528,'Joseph Steele','2002-08-06 03:18:04',122,16),
    (529,'Hyatt Wong','1995-10-08 00:52:08',98,19),
    (530,'Kadeem Holloway','2004-07-26 07:14:10',122,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (531,'Barrett Vaughn','1995-11-18 09:16:01',26,11),
    (532,'Megan Cortez','1998-01-21 00:12:18',133,14),
    (533,'Sharon Figueroa','1997-03-11 04:11:21',7,12),
    (534,'Ifeoma Rivers','2003-04-11 19:11:40',115,19),
    (535,'Tyler Wiggins','2002-09-28 22:27:34',40,12),
    (536,'Skyler Callahan','1996-04-03 19:14:14',94,20),
    (537,'Bruce Madden','1995-09-06 01:17:45',3,10),
    (538,'Octavia Irwin','1995-01-08 13:03:33',69,18),
    (539,'Mallory Ortiz','1998-09-20 16:09:01',43,12),
    (540,'Gillian Jackson','1995-01-25 08:53:17',42,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (541,'Thaddeus Le','1995-05-27 06:49:49',41,12),
    (542,'Lillian Mcmillan','1998-12-17 03:19:32',117,13),
    (543,'Nayda Calderon','1995-03-22 16:49:46',128,11),
    (544,'Jorden Drake','2003-06-12 19:20:38',72,12),
    (545,'Bradley Weber','2001-11-24 12:00:18',42,18),
    (546,'Vernon Fitzpatrick','1998-11-02 08:06:01',31,19),
    (547,'Plato Weiss','2002-11-24 10:50:37',98,17),
    (548,'Stella Reese','2002-03-04 22:56:15',162,11),
    (549,'Jolene Joseph','1998-01-09 14:24:20',74,17),
    (550,'Oliver Massey','2000-09-09 00:13:39',48,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (551,'Ann Conrad','2002-12-26 05:37:52',169,16),
    (552,'Moses Cruz','1999-08-01 08:35:16',159,11),
    (553,'Mona Cortez','2002-01-10 07:30:18',83,20),
    (554,'Candice Hull','2001-11-02 06:58:15',81,12),
    (555,'Jolie Sharp','2001-07-25 15:13:46',83,17),
    (556,'Madeson Ware','1998-06-01 18:28:49',140,15),
    (557,'Nolan Sparks','2001-01-24 20:41:16',118,15),
    (558,'Joseph Morin','2001-01-19 14:36:53',80,15),
    (559,'Ina Tyler','1998-07-21 01:10:33',156,19),
    (560,'Shea Middleton','2004-11-14 15:21:32',154,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (561,'Tana Mathews','2004-06-04 10:24:57',85,18),
    (562,'Yetta Charles','1999-07-29 11:13:22',24,15),
    (563,'Hector Workman','1998-12-04 03:23:17',110,18),
    (564,'Helen Kirkland','1998-05-29 21:27:24',37,18),
    (565,'Lev Cross','1998-09-02 02:51:14',121,19),
    (566,'Hashim Nicholson','1995-12-07 16:13:35',61,11),
    (567,'Drew Chavez','2000-03-25 12:42:34',136,15),
    (568,'Lacota Vang','2001-02-06 20:41:21',63,19),
    (569,'Chelsea Shepard','1999-07-25 22:07:45',44,12),
    (570,'Minerva Pierce','1999-10-10 16:04:43',97,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (571,'Hakeem Owens','2004-02-08 03:30:20',45,20),
    (572,'Jerry Rutledge','2000-12-23 15:30:00',133,20),
    (573,'Lila Caldwell','1998-05-03 07:09:44',48,14),
    (574,'Emery Cannon','2001-01-21 00:59:26',101,14),
    (575,'Amena Ferguson','2002-12-02 23:02:59',115,15),
    (576,'Mason Fowler','2004-04-09 18:58:01',24,19),
    (577,'Ruby Nichols','1997-05-17 15:25:49',162,18),
    (578,'Wendy Lott','1999-12-01 01:12:10',107,17),
    (579,'Orson Tillman','2001-05-16 08:19:47',40,12),
    (580,'Hope Morris','2004-10-16 09:45:51',160,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (581,'Raymond Meyers','1999-06-10 22:51:42',167,12),
    (582,'Ian Sherman','2001-12-09 20:55:17',173,18),
    (583,'Driscoll Price','1998-03-01 08:40:55',71,16),
    (584,'Audra England','1995-06-06 10:26:04',42,20),
    (585,'Paula Reese','2002-12-29 04:58:46',134,20),
    (586,'Jakeem Hatfield','2004-02-26 04:29:13',170,20),
    (587,'Deacon Marshall','1999-04-22 18:41:10',172,11),
    (588,'Yasir Fry','2002-08-12 05:46:39',39,13),
    (589,'Chandler Stanton','1998-10-04 11:28:47',84,13),
    (590,'Kibo Hancock','2004-08-20 21:06:09',174,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (591,'Geoffrey Vinson','2000-07-03 23:12:22',53,16),
    (592,'Illana Dixon','2001-07-19 18:39:59',159,12),
    (593,'Carter Delacruz','1996-11-01 03:56:16',136,18),
    (594,'Castor Peterson','1998-08-24 20:09:28',84,18),
    (595,'Maryam Shaw','1997-01-28 09:35:42',139,12),
    (596,'Nyssa Petersen','2000-08-29 20:26:12',117,13),
    (597,'Violet Wiggins','1998-03-18 21:16:59',179,12),
    (598,'Scott Fuentes','1998-02-20 17:12:54',50,14),
    (599,'Maia Velasquez','2002-10-31 18:38:49',21,14),
    (600,'Yael Suarez','2002-07-14 13:51:16',144,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (601,'Andrew Hyde','2003-01-20 16:57:49',72,13),
    (602,'Amal Waller','1995-02-23 10:45:57',162,18),
    (603,'Jackson Bell','1995-07-04 10:36:38',131,12),
    (604,'Colette Miller','1998-09-12 22:14:57',100,11),
    (605,'Illana Knight','2002-12-24 20:45:18',149,17),
    (606,'Martin Harrington','1997-02-16 20:59:04',45,10),
    (607,'Barclay Howell','1996-01-23 00:33:23',41,13),
    (608,'Marcia Reeves','2000-12-14 10:00:28',53,15),
    (609,'Honorato Hanson','2001-12-29 01:17:44',109,16),
    (610,'Lael Wells','1996-01-22 00:54:52',119,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (611,'Heather Mann','2004-01-08 01:57:44',142,10),
    (612,'Zeus Cotton','1997-02-02 05:10:19',11,20),
    (613,'Oren Cunningham','1999-01-29 04:09:36',11,20),
    (614,'Kristen Gonzales','2001-03-17 09:55:05',60,19),
    (615,'Lois Donaldson','1997-04-11 20:05:40',38,17),
    (616,'Rogan Wall','2002-05-30 00:25:08',38,19),
    (617,'Dane Hamilton','1998-08-22 09:26:13',157,13),
    (618,'Yael Hays','2004-11-22 06:05:34',53,15),
    (619,'Odette Townsend','2002-07-24 04:49:51',105,13),
    (620,'Bernard Strickland','1997-12-08 16:39:49',150,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (621,'Damon Booth','2002-02-03 15:14:16',172,13),
    (622,'Shay Harrington','1995-04-13 10:55:36',139,19),
    (623,'Macon Webster','1996-12-03 07:04:07',135,18),
    (624,'Maris Zamora','2001-01-17 06:09:15',20,17),
    (625,'Myles Dudley','1995-08-04 07:00:59',115,17),
    (626,'Micah Lester','2003-10-11 07:41:41',97,18),
    (627,'Blythe Keith','1997-03-20 12:05:47',158,14),
    (628,'Jerome Atkinson','2000-04-07 00:18:15',73,17),
    (629,'Veda Castro','2004-04-30 20:10:22',108,19),
    (630,'Vance Schwartz','2002-05-11 03:41:46',106,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (631,'Emma Bishop','1995-05-19 15:52:34',26,14),
    (632,'Reuben Flowers','2002-08-17 22:40:36',101,17),
    (633,'Hilel Hines','2000-05-15 17:42:52',118,18),
    (634,'Ira Hogan','2003-01-08 02:06:33',3,13),
    (635,'Murphy Castro','1999-08-23 22:42:45',14,13),
    (636,'Sydney Allen','2004-06-04 19:34:44',170,10),
    (637,'Vaughan Gould','2000-09-27 22:59:53',85,11),
    (638,'Tanisha Juarez','1998-03-19 17:35:13',51,12),
    (639,'Lewis Velasquez','1997-04-01 00:16:17',169,16),
    (640,'Yoshi Maddox','1996-04-02 21:08:22',150,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (641,'Charde West','1996-03-25 06:21:33',162,20),
    (642,'Jana Dalton','1995-03-20 09:46:27',50,12),
    (643,'Mechelle Hester','1999-12-19 16:03:14',95,19),
    (644,'Ali Spencer','1995-09-09 23:00:12',170,20),
    (645,'Iliana Davidson','2000-05-22 23:43:16',90,19),
    (646,'Amela Hays','2003-03-05 11:51:57',70,12),
    (647,'Scarlet Zimmerman','2001-11-22 02:25:41',123,16),
    (648,'Echo Franco','1997-10-30 13:01:50',6,17),
    (649,'Arden Lowery','1996-02-02 04:09:54',103,15),
    (650,'Jocelyn Carlson','2004-01-29 12:49:50',139,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (651,'Kermit Frye','2003-04-05 00:00:11',29,14),
    (652,'Shafira Langley','2000-10-02 19:25:20',96,12),
    (653,'Henry Decker','2004-01-28 10:26:12',6,13),
    (654,'Lewis Workman','1997-10-22 22:30:45',3,15),
    (655,'Lara Hurst','1998-07-19 18:47:52',166,18),
    (656,'Vernon Chaney','1996-12-03 14:46:34',39,14),
    (657,'Kitra Middleton','2004-09-30 23:10:43',105,16),
    (658,'Fitzgerald Mccullough','1996-09-12 19:34:00',14,13),
    (659,'Salvador Donaldson','2001-01-07 23:14:25',40,20),
    (660,'Tanya Boyle','2002-06-07 12:51:19',67,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (661,'Steel Mcdaniel','1998-08-12 17:42:16',6,11),
    (662,'Adria Miller','2001-10-27 10:54:13',139,20),
    (663,'Barrett Serrano','2000-12-25 11:42:06',14,20),
    (664,'Marny Hopper','1996-04-17 14:24:13',170,18),
    (665,'Neil Mccullough','2004-11-30 17:10:22',42,17),
    (666,'Anne Donovan','2000-02-19 16:04:00',38,12),
    (667,'Kasimir Dale','2002-05-02 12:05:49',66,19),
    (668,'Hector Mcconnell','2000-02-18 23:04:52',106,17),
    (669,'Illana Carpenter','2003-06-01 09:43:46',18,15),
    (670,'Edan Barrett','2001-06-02 21:09:25',180,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (671,'Damon Mckenzie','2001-12-27 21:25:29',79,13),
    (672,'Stuart Webb','2000-02-11 22:43:55',175,10),
    (673,'Chiquita Chan','2002-09-14 07:48:11',90,18),
    (674,'Basil Randolph','2001-08-20 15:32:40',12,13),
    (675,'Noah Blankenship','1996-01-04 00:45:51',96,13),
    (676,'Keegan Dickson','1996-05-21 14:52:14',74,20),
    (677,'Gretchen Cline','2003-02-22 11:48:20',27,12),
    (678,'Hedy Burnett','2000-04-10 09:47:37',14,16),
    (679,'Caryn Carroll','1995-09-29 12:32:34',139,18),
    (680,'Zephr Holman','1995-09-05 01:35:38',91,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (681,'Gemma Erickson','2001-10-30 09:10:38',125,14),
    (682,'Craig Hull','2004-06-26 12:45:51',149,12),
    (683,'Talon Long','1999-03-17 02:27:31',13,11),
    (684,'Melyssa Singleton','1998-06-14 22:10:04',155,12),
    (685,'Ayanna Buck','1996-06-21 09:58:37',81,11),
    (686,'Aidan Hicks','1995-10-25 17:46:43',84,10),
    (687,'Shad White','2002-10-16 23:44:57',59,16),
    (688,'Serina Mcmillan','1998-09-20 10:49:26',24,18),
    (689,'Thaddeus French','1996-06-12 11:42:15',177,19),
    (690,'Leslie Ford','1998-09-16 14:23:30',121,10);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (691,'Cade Byers','1996-12-18 18:43:20',54,15),
    (692,'Sara Carter','1995-03-04 14:01:59',94,19),
    (693,'Palmer Gilmore','2000-08-28 00:49:45',98,11),
    (694,'Len Fitzpatrick','1995-09-26 21:11:28',147,12),
    (695,'Adena Lowe','1997-02-26 16:07:12',39,14),
    (696,'Zena Combs','2004-02-27 07:55:16',46,14),
    (697,'Keaton Lynn','1995-08-30 13:34:07',90,19),
    (698,'Arthur Burch','1995-09-01 23:17:16',44,12),
    (699,'Wang Barnes','1997-06-11 16:22:53',139,10),
    (700,'Britanni Lucas','2003-10-08 17:33:39',69,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (701,'Edward Byers','1995-05-06 22:22:30',107,17),
    (702,'Jeremy Patrick','1998-01-22 17:33:37',44,17),
    (703,'Rebekah Stewart','2000-09-10 06:20:03',33,13),
    (704,'Jonas Velez','2004-11-19 19:51:47',143,14),
    (705,'Garrison Bruce','1995-06-21 19:29:00',135,20),
    (706,'Bradley Weaver','1996-07-04 08:42:15',161,12),
    (707,'Pearl Middleton','1997-11-26 07:58:32',39,13),
    (708,'Denton Shelton','2000-06-30 19:28:21',70,20),
    (709,'Wynne Mills','2003-07-02 08:26:13',34,16),
    (710,'Amethyst Green','1998-11-16 11:59:40',98,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (711,'Ori Daniels','2003-03-20 02:03:04',121,17),
    (712,'Victor Cameron','1996-01-01 12:09:15',19,14),
    (713,'Illiana Holmes','2002-10-27 10:16:05',68,17),
    (714,'Mollie Booth','2004-08-25 19:31:29',57,20),
    (715,'Evelyn Ramos','2004-07-24 11:12:08',47,14),
    (716,'Ralph Hurst','1995-02-15 15:08:25',113,15),
    (717,'Arthur Marsh','2002-11-26 21:10:08',55,12),
    (718,'Charles Espinoza','2004-02-08 16:18:27',130,17),
    (719,'Stephen Lowe','1998-06-28 23:48:00',103,12),
    (720,'Sade Herrera','1997-03-29 01:21:20',71,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (721,'Reed Jensen','2004-12-07 08:46:42',19,14),
    (722,'Constance Baxter','2001-08-04 08:23:49',64,13),
    (723,'Cleo Alexander','2003-04-19 10:25:47',47,19),
    (724,'Laith Benson','1995-09-04 04:42:22',54,20),
    (725,'Galvin Harding','1997-02-13 11:19:56',14,15),
    (726,'Francis Melendez','2002-08-18 08:19:30',56,18),
    (727,'Mia Combs','1998-03-25 02:51:42',108,12),
    (728,'Hilel Hodges','1995-12-13 09:47:41',61,18),
    (729,'Jeremy Mack','2001-04-30 03:57:23',180,12),
    (730,'Amela Carey','2001-06-26 18:55:55',110,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (731,'Virginia Mclean','2002-07-25 15:31:28',20,10),
    (732,'Jayme Conley','1996-01-21 13:44:53',78,10),
    (733,'Signe Chase','1996-12-15 02:06:14',53,19),
    (734,'Hanae Mcgee','1995-12-14 21:40:55',94,13),
    (735,'Abel Meadows','2004-01-02 18:24:40',15,17),
    (736,'Wendy Hamilton','2001-12-27 18:03:57',83,19),
    (737,'Amber Henry','2001-07-11 05:55:39',132,12),
    (738,'Castor Fulton','1996-01-04 10:42:25',8,14),
    (739,'Laith Stout','2001-04-19 09:20:46',57,13),
    (740,'Galena Faulkner','2004-02-20 21:10:21',61,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (741,'Carter Wilcox','2002-08-02 05:54:06',7,13),
    (742,'Damian Rice','1997-02-19 15:45:21',25,12),
    (743,'Neil Kelly','2001-01-26 10:31:59',40,16),
    (744,'Dana Cameron','2003-10-16 21:29:51',74,17),
    (745,'Dominique Rivas','2000-06-05 17:59:26',169,10),
    (746,'Uta Cohen','1997-05-15 09:01:37',166,13),
    (747,'Ariana Rivera','1998-11-14 03:00:51',44,14),
    (748,'Clark Weber','2002-07-13 15:56:10',162,16),
    (749,'Simon Horn','2003-08-11 11:42:19',20,11),
    (750,'Hu Marsh','2000-03-20 21:49:37',24,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (751,'Bevis Peterson','1995-04-15 21:47:16',57,16),
    (752,'Ariana Townsend','2003-11-05 00:59:54',13,14),
    (753,'Rebekah Fields','2002-01-12 06:12:10',166,14),
    (754,'Susan Lawrence','2001-12-14 03:58:19',55,12),
    (755,'Bradley Carey','2001-06-08 08:23:55',68,12),
    (756,'Gareth Gaines','1996-05-26 04:18:19',72,14),
    (757,'Imani Sloan','1998-09-24 18:43:04',51,11),
    (758,'Sharon Daugherty','2000-11-23 16:54:31',161,11),
    (759,'Veronica Emerson','2001-07-07 10:59:17',1,12),
    (760,'Noah Francis','2002-05-15 23:43:43',49,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (761,'Jaquelyn Potts','2002-03-31 10:10:54',89,10),
    (762,'Harlan Ratliff','1997-11-08 15:10:38',98,10),
    (763,'Malcolm Adams','2002-09-03 22:25:09',33,20),
    (764,'Ramona Moore','1996-09-06 16:37:33',68,16),
    (765,'Catherine Griffith','1999-01-27 20:06:05',166,17),
    (766,'Ciara Hoffman','1999-06-11 03:51:23',146,13),
    (767,'Bertha Simmons','2000-06-25 07:31:30',131,20),
    (768,'Jaime Warner','1995-07-22 16:36:23',27,12),
    (769,'Skyler Soto','2002-02-08 17:22:52',88,15),
    (770,'Kelly Walls','1995-12-22 23:26:42',162,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (771,'Plato Maxwell','1998-08-15 12:00:58',169,11),
    (772,'Dylan Chandler','1996-10-26 11:09:24',107,18),
    (773,'Harper Gonzalez','2001-01-23 22:18:07',121,12),
    (774,'Kenneth Erickson','1995-07-24 17:43:22',69,17),
    (775,'Hayden Schmidt','2001-02-11 04:11:33',111,14),
    (776,'Jameson Richardson','2004-10-18 04:35:45',24,14),
    (777,'Germaine Pratt','2000-11-03 17:49:15',142,14),
    (778,'Maia Ferguson','1998-03-17 22:31:06',102,20),
    (779,'Pandora Horn','2003-01-13 18:53:11',128,16),
    (780,'Otto Hull','1999-10-02 09:25:24',66,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (781,'Jael Washington','1996-07-12 14:26:28',159,14),
    (782,'Phyllis Emerson','2002-02-25 15:07:01',99,12),
    (783,'Sopoline Pope','1996-08-05 04:35:55',131,17),
    (784,'Fuller Wagner','1996-05-07 00:46:19',146,17),
    (785,'Martina Marks','2002-02-01 07:35:00',30,19),
    (786,'Xena Tucker','2004-01-04 02:59:10',65,14),
    (787,'Scott Chapman','1997-08-02 10:17:50',38,12),
    (788,'Hedley Cannon','1999-11-05 13:45:53',89,14),
    (789,'Stephen Walsh','1996-07-28 07:20:50',40,15),
    (790,'Bethany Pickett','1998-08-27 13:19:40',85,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (791,'Jennifer Maxwell','2001-07-11 16:49:26',24,16),
    (792,'Jermaine Fisher','1996-07-23 18:32:38',65,12),
    (793,'Cadman Bonner','1995-05-03 11:15:13',113,18),
    (794,'Len Melton','2004-09-01 00:48:38',23,14),
    (795,'Ashely Roman','1998-05-23 08:52:42',88,14),
    (796,'Germane Jefferson','1997-02-10 07:22:40',19,18),
    (797,'Desirae Dunlap','2003-01-10 12:35:29',41,18),
    (798,'Xantha Hopper','1998-01-20 17:54:17',102,19),
    (799,'Joelle Hewitt','2003-06-06 17:31:10',111,17),
    (800,'Lacota Hendricks','1996-12-26 19:49:35',86,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (801,'Neil Case','1997-07-12 23:55:51',102,13),
    (802,'Selma Bailey','1995-10-04 13:31:41',127,16),
    (803,'Zahir Walter','2001-03-19 02:51:21',35,13),
    (804,'Duncan Rivera','1998-02-12 21:02:55',83,10),
    (805,'Julian Kelly','1998-12-12 02:10:00',119,19),
    (806,'Abel Puckett','1996-01-16 15:10:28',176,17),
    (807,'Paki Hicks','2004-11-23 20:59:49',24,15),
    (808,'Warren Leblanc','2000-02-17 20:22:15',75,15),
    (809,'Dane Barrett','2001-06-11 17:06:20',151,12),
    (810,'Alvin Barker','2004-03-21 04:00:04',148,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (811,'Tasha Pratt','1997-06-12 20:18:08',137,12),
    (812,'Anne Black','1998-04-27 00:26:05',124,11),
    (813,'Veronica Woodard','1996-11-13 07:02:54',60,14),
    (814,'Hamish Kemp','2004-09-17 12:35:20',82,16),
    (815,'Lunea Rosales','2000-04-17 15:58:49',163,15),
    (816,'Armand Macdonald','2004-06-09 18:28:33',118,15),
    (817,'Allen Nolan','2002-04-22 09:26:22',123,11),
    (818,'August Cantrell','1997-06-04 17:09:15',26,11),
    (819,'Chandler James','1999-06-03 16:00:10',117,14),
    (820,'Florence Guerra','2000-09-07 15:24:47',77,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (821,'Eleanor Mayo','2000-04-24 22:18:56',25,19),
    (822,'Mohammad Daniels','1996-02-03 20:39:44',112,13),
    (823,'Lionel Bailey','1998-02-28 01:19:02',124,12),
    (824,'Jarrod Blevins','2001-07-07 01:22:50',8,11),
    (825,'Maris Clayton','1997-03-17 08:07:28',82,12),
    (826,'Connor Underwood','1996-11-22 16:42:00',80,11),
    (827,'Elijah Zimmerman','1995-09-24 14:44:38',28,15),
    (828,'Chancellor Battle','1995-04-15 14:26:24',4,19),
    (829,'Thane Vaughan','2002-03-18 15:46:51',112,19),
    (830,'Daryl Ewing','1998-01-05 23:28:52',136,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (831,'Robin Suarez','2001-09-27 01:39:22',175,11),
    (832,'Fuller Wynn','2001-11-24 10:05:31',72,12),
    (833,'Kyla Delaney','2000-07-22 14:48:26',5,19),
    (834,'Cole Sanford','1997-07-03 21:00:01',41,14),
    (835,'Brianna Glover','1997-09-07 23:18:50',73,14),
    (836,'Nash Skinner','1995-05-09 17:11:08',118,19),
    (837,'Madison Dotson','2001-08-19 17:57:27',106,18),
    (838,'Yasir Velasquez','1998-04-28 21:57:50',81,15),
    (839,'Zahir Lopez','1995-03-10 02:34:44',141,11),
    (840,'Jeremy Hess','2000-05-19 11:31:33',128,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (841,'Tanisha Carr','1996-05-24 23:10:46',73,15),
    (842,'Driscoll Fields','2002-12-05 08:30:37',123,20),
    (843,'Cherokee Doyle','2000-03-21 01:40:51',154,15),
    (844,'Jana Cleveland','2001-10-08 15:40:39',28,17),
    (845,'Aquila Waters','1999-09-04 07:27:12',101,15),
    (846,'Chadwick Blair','2000-02-05 07:49:02',86,18),
    (847,'Imani Stone','2002-11-26 20:38:48',56,17),
    (848,'Thor Morrison','2002-02-13 20:13:22',129,15),
    (849,'Yvonne William','2001-12-22 20:28:59',64,15),
    (850,'Donovan Monroe','2001-04-05 05:38:50',119,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (851,'Tanner Buchanan','1999-10-18 08:23:23',115,18),
    (852,'Hanna Horne','2002-12-03 17:06:57',77,18),
    (853,'Cathleen Pennington','1999-11-20 23:34:51',87,16),
    (854,'Cameron Pickett','1997-12-18 15:11:58',171,15),
    (855,'Winifred Weeks','1997-12-17 00:16:24',85,15),
    (856,'Brennan Peters','1996-05-15 08:23:09',163,13),
    (857,'George Nixon','2003-09-27 22:04:48',168,12),
    (858,'Basia Mooney','1998-08-15 18:28:50',115,17),
    (859,'Latifah Guerrero','2002-12-10 17:29:11',50,20),
    (860,'Brandon Glass','2003-12-29 23:10:14',16,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (861,'Aladdin Tanner','2003-05-02 07:40:51',141,10),
    (862,'Hector Burris','2004-12-29 11:01:29',98,13),
    (863,'Yoshio Jefferson','2001-07-11 16:44:15',135,13),
    (864,'Illana Kramer','1999-02-28 12:57:48',172,11),
    (865,'Maxine Holmes','2000-09-24 10:08:54',51,16),
    (866,'Odessa May','2004-10-22 02:46:30',136,19),
    (867,'Wyatt Ferrell','2000-06-07 05:55:59',36,17),
    (868,'Ferdinand Hardin','1997-10-17 21:00:59',159,16),
    (869,'Sonya Castaneda','1998-02-03 11:53:23',19,16),
    (870,'Stone Thomas','1995-04-02 07:51:06',95,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (871,'Akeem Compton','2002-01-08 11:29:04',17,19),
    (872,'Peter Chapman','1999-02-03 15:50:03',167,19),
    (873,'Colton Francis','1997-07-11 09:40:41',121,14),
    (874,'Knox Walker','2000-12-18 11:24:33',144,19),
    (875,'Indigo Coleman','1996-09-05 22:25:01',57,16),
    (876,'Hall Rasmussen','2004-08-08 04:00:12',119,10),
    (877,'Chadwick Phillips','2002-12-24 21:03:46',160,16),
    (878,'Wynter Vance','2003-04-13 04:24:48',9,17),
    (879,'Kirby Flowers','2001-10-10 14:53:33',73,11),
    (880,'Jasper Knight','1999-09-23 02:22:44',121,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (881,'Axel Shaffer','2000-09-10 23:31:35',114,13),
    (882,'Nevada Flowers','2000-12-08 22:53:11',124,15),
    (883,'Nissim Cash','2000-12-29 18:25:25',155,20),
    (884,'Carlos Wright','1996-06-23 01:08:58',119,11),
    (885,'Devin Calhoun','1998-01-13 21:55:04',41,20),
    (886,'Winifred Lynn','1998-12-27 18:23:59',69,17),
    (887,'Drew Kinney','2000-02-13 00:25:33',55,17),
    (888,'Imelda Patel','1998-10-05 08:22:26',163,20),
    (889,'Sigourney Robinson','2000-01-27 17:58:50',63,11),
    (890,'Quail Morgan','1995-08-01 09:15:15',89,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (891,'Cameron Carroll','1999-06-10 06:16:41',43,10),
    (892,'Roanna Pope','2004-02-28 17:36:55',54,14),
    (893,'Bernard Ayala','2000-12-10 00:38:13',56,19),
    (894,'Allen Downs','1996-10-01 07:28:45',25,12),
    (895,'Steel Hebert','2003-03-27 13:20:54',175,11),
    (896,'Chancellor Terrell','2002-01-23 20:13:42',62,13),
    (897,'Benedict Navarro','2000-04-28 20:49:05',65,15),
    (898,'Owen Hoffman','1996-12-12 11:23:35',16,15),
    (899,'Mikayla Knight','1996-05-12 14:30:45',148,15),
    (900,'Hyacinth Vega','2000-05-12 10:43:36',117,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (901,'Lavinia Johnston','1998-12-05 21:53:31',112,20),
    (902,'Xyla Morin','2004-10-26 07:01:10',70,19),
    (903,'Justina Church','2000-06-09 21:40:55',68,19),
    (904,'Pascale Gillespie','2000-10-17 10:36:04',127,18),
    (905,'Jada Cote','2003-09-24 03:23:50',177,16),
    (906,'Brianna Meyer','2001-06-23 22:19:29',13,14),
    (907,'Oren Gutierrez','2000-10-14 19:20:08',34,13),
    (908,'Ira Morrison','1998-08-28 01:38:24',34,16),
    (909,'Kameko Mccray','1997-02-04 06:53:56',160,17),
    (910,'Julian Rosa','2000-03-18 22:00:26',146,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (911,'Kevyn Atkins','1995-08-30 16:06:58',152,17),
    (912,'Danielle Forbes','1995-08-12 14:45:41',77,12),
    (913,'Neville Wilder','1998-09-17 18:29:20',175,11),
    (914,'Serena Irwin','1998-09-10 15:37:55',95,20),
    (915,'Chloe Mcdaniel','2002-02-23 19:17:00',60,17),
    (916,'Amir Salas','1996-11-10 10:18:44',64,15),
    (917,'Fletcher Norton','1999-05-16 21:50:50',10,16),
    (918,'Shoshana Shepherd','1997-06-16 14:45:21',74,12),
    (919,'Richard Morrow','2000-12-03 19:45:48',88,17),
    (920,'Hannah Haney','1998-06-30 20:10:10',71,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (921,'Cassady Gonzalez','1999-12-04 10:05:07',151,12),
    (922,'Todd Blake','1996-04-09 19:02:58',53,18),
    (923,'Walter Gomez','2004-07-28 09:21:49',69,15),
    (924,'Felicia Sparks','2003-01-16 23:48:33',50,16),
    (925,'Stephanie Barker','1999-02-19 18:16:08',99,14),
    (926,'Jelani Rose','2000-08-02 22:52:24',100,11),
    (927,'Ivory Blankenship','1997-04-14 04:25:24',21,13),
    (928,'Mallory Ortiz','1997-11-06 04:07:45',49,13),
    (929,'Jermaine Ball','1998-02-03 03:48:36',105,17),
    (930,'Willa Stevenson','1997-05-02 12:08:42',173,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (931,'Jasper Clark','2002-06-22 17:14:10',10,18),
    (932,'Cleo Crawford','1996-12-06 21:58:18',114,16),
    (933,'Vaughan Brewer','2003-09-23 01:08:45',90,13),
    (934,'Sade Pate','2003-01-25 05:37:57',96,13),
    (935,'Lucy Velasquez','1998-11-08 03:26:11',30,12),
    (936,'Thaddeus Little','1997-06-28 09:21:34',92,18),
    (937,'Lawrence Ashley','1999-09-09 09:43:14',86,18),
    (938,'Erin Fox','1998-12-06 20:18:29',95,17),
    (939,'Jada Macdonald','1995-07-13 17:00:44',26,13),
    (940,'Josephine Morse','1999-01-08 22:07:37',72,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (941,'Chastity Pruitt','1996-02-14 07:12:58',93,12),
    (942,'Benedict Osborn','2004-05-26 02:01:28',96,13),
    (943,'Allistair Maldonado','2003-12-18 01:59:36',15,10),
    (944,'Hamilton Johns','1996-12-31 04:09:37',143,13),
    (945,'Nasim Solis','2001-04-29 05:07:56',150,19),
    (946,'Jackson Higgins','2001-09-11 11:47:40',85,17),
    (947,'Hadassah Key','2000-02-29 14:58:31',115,18),
    (948,'Sybill Clements','1998-04-10 15:52:54',169,14),
    (949,'Salvador Schultz','1996-03-15 05:04:41',96,14),
    (950,'Graham Craig','2001-02-08 14:12:04',131,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (951,'Solomon Ferrell','1999-05-31 22:52:36',101,19),
    (952,'Kane Steele','1997-04-10 21:23:24',148,16),
    (953,'Yeo Jordan','2001-11-05 15:15:08',70,15),
    (954,'Jenna Beach','1997-05-17 23:38:55',94,14),
    (955,'Dean Maynard','2002-05-28 21:10:49',159,19),
    (956,'Shellie Conway','1996-06-20 09:44:31',130,16),
    (957,'Beau Mcfarland','1995-07-06 15:38:07',94,18),
    (958,'Zachary Cline','2001-12-21 01:56:41',97,18),
    (959,'Chester Johns','2003-04-28 20:00:11',79,19),
    (960,'Renee Snider','1996-08-03 09:52:12',110,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (961,'Joy Compton','2003-01-27 10:49:37',160,19),
    (962,'Uriah Wright','2002-06-02 02:21:21',86,14),
    (963,'Regan Cantu','2000-04-02 14:22:20',133,14),
    (964,'Britanney Haley','2000-03-13 22:30:18',138,18),
    (965,'Thomas Owen','2004-04-02 12:08:36',57,17),
    (966,'Rylee Bullock','2004-02-02 20:41:14',135,18),
    (967,'Amelia Sanford','2004-06-25 00:26:29',154,12),
    (968,'Damian Duffy','2000-10-07 19:19:43',5,17),
    (969,'Benedict Holman','2003-07-22 04:20:03',21,14),
    (970,'Sacha Crane','2004-01-11 04:15:20',86,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (971,'Arden Cameron','1999-10-17 19:58:32',107,19),
    (972,'Chadwick Navarro','2002-09-05 23:26:05',171,12),
    (973,'Ivory Blackwell','2003-07-02 00:01:43',152,19),
    (974,'Chaim Hughes','1995-03-31 00:17:49',85,18),
    (975,'Kyle Gamble','2003-09-20 13:49:33',40,17),
    (976,'Omar O''connor','2003-02-22 14:15:44',85,14),
    (977,'Baker Warner','1997-07-29 08:48:15',17,13),
    (978,'Shay Simmons','2003-06-15 21:42:44',118,19),
    (979,'Stephen Sykes','2001-03-06 01:43:30',142,19),
    (980,'Victoria Mayo','1997-12-26 13:15:35',137,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (981,'Bradley Cameron','1996-08-28 20:02:32',20,10),
    (982,'Linus Sutton','1999-02-25 12:18:08',175,16),
    (983,'Briar James','1999-12-09 00:05:37',112,13),
    (984,'Lee Bryan','1996-12-12 11:10:29',180,12),
    (985,'Cecilia Ratliff','2003-01-18 18:11:35',91,20),
    (986,'Tad Dale','1995-11-11 11:06:49',119,12),
    (987,'Solomon Marquez','2001-08-11 22:03:59',154,12),
    (988,'Zephr Harrison','1999-03-07 10:44:12',141,15),
    (989,'Aidan Michael','2004-06-19 23:01:53',88,11),
    (990,'Nathan Parks','2002-03-04 18:53:40',151,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (991,'Constance Rosario','1999-12-03 14:24:58',138,12),
    (992,'Cheyenne Rasmussen','1995-12-16 15:42:08',48,12),
    (993,'Noble Grimes','2001-04-24 10:01:24',48,12),
    (994,'Jasmine Peters','1995-01-11 23:35:15',149,11),
    (995,'Amal Moss','2002-03-19 03:38:40',43,11),
    (996,'Rylee Prince','1995-12-21 07:19:58',111,16),
    (997,'Fuller Marsh','1995-03-15 16:47:15',130,10),
    (998,'Karleigh Byrd','2000-10-14 23:26:16',116,16),
    (999,'Eve Galloway','1999-12-14 11:48:56',81,14),
    (1000,'Daryl Barron','2003-08-12 06:50:39',127,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1001,'Dana Winters','1995-07-26 20:54:27',32,19),
    (1002,'Palmer Freeman','2003-01-29 21:04:40',169,12),
    (1003,'Brooke Blankenship','2002-04-24 15:31:05',111,18),
    (1004,'Carol Velazquez','2001-07-21 20:54:09',74,11),
    (1005,'Carlos Thornton','1997-05-07 16:02:35',132,11),
    (1006,'Cody Bates','2002-11-11 13:59:47',61,13),
    (1007,'Sonia Watts','2002-03-23 01:15:57',156,12),
    (1008,'Sasha Blankenship','2003-10-05 05:52:32',12,18),
    (1009,'Kiona Holman','2002-01-30 17:37:32',120,13),
    (1010,'Dieter Nielsen','2001-05-08 03:33:47',162,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1011,'Timothy House','2002-01-21 17:22:06',89,12),
    (1012,'Ulysses Pate','2001-07-23 23:58:26',17,17),
    (1013,'Roanna Gilliam','1997-06-25 16:45:06',79,15),
    (1014,'Luke Blevins','1997-07-28 01:07:09',135,12),
    (1015,'Gray Shelton','1995-10-18 21:56:04',175,19),
    (1016,'Raphael Cantu','2004-06-10 05:45:03',31,13),
    (1017,'Phoebe Norris','1998-11-10 05:07:50',9,19),
    (1018,'Daquan Atkins','2003-07-05 10:05:26',171,10),
    (1019,'Cally Armstrong','1999-11-02 15:22:54',62,10),
    (1020,'Brody Hodges','2001-12-01 07:16:15',136,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1021,'Lillian Carson','2001-02-08 14:06:37',139,16),
    (1022,'Sean Hamilton','2000-01-20 02:08:28',83,14),
    (1023,'Katell Allen','1996-10-07 18:51:29',118,12),
    (1024,'Timon Berger','2003-03-20 03:25:14',103,12),
    (1025,'Rina Griffin','1996-07-28 08:41:33',162,16),
    (1026,'Clare Sawyer','1999-01-23 21:45:44',42,19),
    (1027,'Lisandra Beck','1997-11-28 19:16:02',85,14),
    (1028,'Tobias Kirkland','2003-10-08 10:06:39',54,11),
    (1029,'Tobias Chambers','1996-05-08 08:37:02',177,13),
    (1030,'Gareth Trujillo','1997-09-19 12:39:01',47,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1031,'Stacy Key','1997-06-25 23:38:21',134,20),
    (1032,'Kathleen Buckner','2004-12-08 04:21:29',33,13),
    (1033,'Quinn Wade','2002-05-02 20:20:53',60,15),
    (1034,'Mechelle Mcfadden','2002-07-31 04:29:25',117,19),
    (1035,'Nadine Mcleod','1999-01-20 13:48:54',91,12),
    (1036,'Joy Cross','1999-01-02 11:00:15',61,10),
    (1037,'Lane Reilly','2004-01-19 17:20:33',140,17),
    (1038,'Cody Slater','1998-12-21 18:30:49',72,12),
    (1039,'Noble Hubbard','2001-06-29 10:38:14',88,19),
    (1040,'Erasmus Hunt','1999-04-02 08:39:45',155,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1041,'Clayton Combs','1995-08-21 11:34:12',5,16),
    (1042,'Burton Doyle','2000-11-04 14:09:50',66,20),
    (1043,'Sharon Giles','2002-07-30 05:12:29',66,18),
    (1044,'Abra Petersen','1999-11-02 13:14:02',161,15),
    (1045,'Brennan Pratt','2004-06-23 10:05:17',54,15),
    (1046,'Callum Sanford','2003-07-20 00:16:30',95,17),
    (1047,'Jolene Knight','2004-01-07 02:04:55',96,10),
    (1048,'Brooke Sampson','2002-08-13 09:20:54',147,19),
    (1049,'Keaton Mcconnell','2002-04-18 15:14:06',138,17),
    (1050,'Leroy Mcfarland','1995-11-17 17:43:29',133,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1051,'Dane Craig','2002-03-02 03:52:04',88,18),
    (1052,'Roary Branch','1996-04-24 08:54:15',124,18),
    (1053,'Reese Davis','1995-02-08 12:40:40',7,16),
    (1054,'Macey Buckley','1996-09-02 12:01:51',63,16),
    (1055,'Aimee Vargas','2003-03-24 08:35:20',24,19),
    (1056,'Abel Willis','1995-07-18 18:22:52',32,13),
    (1057,'Shaeleigh Donovan','2004-09-29 11:50:52',81,20),
    (1058,'Norman Hutchinson','1999-08-24 21:13:19',143,19),
    (1059,'Reece Camacho','2000-05-22 17:19:03',33,19),
    (1060,'Doris Rich','1999-04-19 23:44:04',64,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1061,'Ishmael Hopper','2002-12-21 19:55:26',117,18),
    (1062,'Lael Hooper','2000-11-18 03:27:44',3,14),
    (1063,'Benedict Boyer','1996-07-20 18:58:40',16,18),
    (1064,'Isaiah Marquez','1996-12-04 23:12:43',96,17),
    (1065,'Cameron Gould','1999-10-03 13:26:11',119,20),
    (1066,'Dominic Spence','2001-04-16 23:40:57',101,15),
    (1067,'Quail Dennis','1998-02-01 23:24:52',162,19),
    (1068,'Avram Stone','1996-12-19 22:40:41',18,10),
    (1069,'Oliver Fletcher','2000-06-02 20:17:56',107,19),
    (1070,'Hyacinth Cantrell','1998-09-23 15:15:32',8,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1071,'Candace Rogers','1998-02-13 22:53:11',55,19),
    (1072,'Latifah Gardner','2002-07-10 16:49:31',178,11),
    (1073,'Honorato Bray','2001-07-24 07:09:25',95,14),
    (1074,'Jolie Solomon','2003-02-08 03:58:55',164,20),
    (1075,'Andrew Sampson','1998-06-13 11:31:29',29,14),
    (1076,'Yetta Johnson','1999-07-23 00:44:42',70,10),
    (1077,'Amber Norris','1998-01-22 03:35:53',169,13),
    (1078,'Keelie Kidd','1999-07-07 12:26:06',153,16),
    (1079,'Silas Farrell','1999-02-10 23:49:28',108,16),
    (1080,'Nissim Cline','1995-02-21 08:48:04',148,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1081,'Hiram Garner','2003-12-29 20:22:55',0,19),
    (1082,'Vera Camacho','2000-04-28 20:25:35',85,18),
    (1083,'Ulysses Cleveland','2002-09-30 04:13:20',53,10),
    (1084,'Jerry Norton','1998-09-14 15:09:10',5,19),
    (1085,'Raya Nunez','1997-01-15 18:48:36',140,12),
    (1086,'Phyllis Foster','2001-04-14 01:53:27',41,17),
    (1087,'Indira Trujillo','1997-05-04 16:37:59',79,19),
    (1088,'Joseph Mcbride','1996-03-26 10:02:33',119,12),
    (1089,'Urielle Wiggins','2003-12-30 14:49:33',7,20),
    (1090,'Maia Hoover','2004-10-11 21:14:39',176,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1091,'Ignatius Swanson','1995-04-05 07:04:10',111,17),
    (1092,'Melodie Bridges','2003-02-12 10:29:17',73,10),
    (1093,'Bree Goff','1998-01-10 10:35:06',149,13),
    (1094,'Lois Ware','1996-12-07 21:53:18',38,12),
    (1095,'Angela Wright','1998-05-16 21:01:57',133,12),
    (1096,'Reece Velez','2000-05-30 23:11:30',112,11),
    (1097,'Alec Schroeder','2003-07-14 00:12:25',119,11),
    (1098,'Clementine Burgess','1999-06-23 18:22:24',158,17),
    (1099,'Keegan Conley','1997-09-15 06:14:52',85,19),
    (1100,'Audrey Ballard','1996-09-18 22:27:18',149,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1101,'Colby Pennington','1998-07-13 18:20:55',18,14),
    (1102,'Joy Farmer','1997-02-19 15:05:57',144,15),
    (1103,'Blythe Garza','2002-10-16 01:30:04',106,14),
    (1104,'Kennedy Rodriguez','2001-03-21 16:44:58',98,17),
    (1105,'Bruno Roman','1995-12-20 18:22:59',141,17),
    (1106,'Melinda Delacruz','2004-04-28 21:47:30',95,18),
    (1107,'Dexter Dudley','1998-10-13 13:52:25',99,11),
    (1108,'Aspen Carpenter','2004-01-02 20:51:45',16,13),
    (1109,'Margaret Huffman','2001-12-04 03:26:20',150,10),
    (1110,'Alfonso Byrd','2002-11-17 12:02:15',88,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1111,'Gloria Gross','1996-05-28 13:26:22',171,16),
    (1112,'Palmer Hodge','1999-01-30 23:33:39',124,11),
    (1113,'Hyacinth Blake','1998-08-15 17:34:35',54,10),
    (1114,'Slade Hood','1997-12-28 16:23:21',103,19),
    (1115,'Ethan English','2004-05-25 03:49:29',96,12),
    (1116,'Freya Robles','2004-07-05 22:52:18',135,14),
    (1117,'Olivia Blair','2003-02-14 12:26:19',28,13),
    (1118,'Jamalia Nixon','1996-07-03 18:01:00',86,13),
    (1119,'Belle Carlson','2001-10-06 23:56:06',125,17),
    (1120,'Jael Phillips','2003-08-20 20:40:19',123,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1121,'Nicholas Ruiz','2000-10-05 07:39:00',79,17),
    (1122,'Lilah Harrington','2000-12-13 08:23:21',158,16),
    (1123,'Erich Shaw','2001-07-04 09:05:54',8,12),
    (1124,'Mira Barnett','1998-10-22 02:41:41',84,12),
    (1125,'Beck Weber','1998-01-28 10:36:47',10,18),
    (1126,'Theodore Gutierrez','2001-09-20 15:24:20',36,19),
    (1127,'Leo Cole','2000-06-23 14:22:37',87,12),
    (1128,'Idona Cervantes','2004-07-10 20:47:58',32,17),
    (1129,'Jada Mason','1999-05-15 06:34:18',52,11),
    (1130,'Lilah White','1995-06-12 10:34:14',170,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1131,'Dora Joyner','1995-12-05 18:30:55',58,15),
    (1132,'Walter Ryan','1998-01-06 18:39:30',35,13),
    (1133,'Odette Holcomb','2000-06-09 14:10:00',152,12),
    (1134,'Maia Saunders','1997-05-30 06:39:25',115,17),
    (1135,'Kyra Torres','2004-03-18 21:32:19',113,18),
    (1136,'Fletcher Brock','2002-04-18 20:06:07',33,13),
    (1137,'David Sykes','2000-12-08 03:53:38',65,13),
    (1138,'Roary Byrd','2003-11-07 01:07:07',91,12),
    (1139,'Meredith Wolf','1996-04-07 18:21:44',160,11),
    (1140,'Rajah Woodward','2001-03-28 22:53:31',101,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1141,'Germane Randolph','1996-11-06 20:41:40',75,13),
    (1142,'Amir Callahan','1996-08-18 08:38:45',51,12),
    (1143,'Imogene Skinner','1999-09-15 05:21:31',29,17),
    (1144,'Colorado Holman','2002-03-30 11:50:55',147,12),
    (1145,'Phillip Mccormick','1999-02-06 18:36:08',128,20),
    (1146,'Minerva Watson','2001-02-18 06:14:45',10,19),
    (1147,'Camden Harding','2002-07-30 09:47:40',110,14),
    (1148,'Brianna Anthony','2003-07-17 13:19:35',3,10),
    (1149,'Aline Kane','1997-03-27 11:25:23',146,12),
    (1150,'Colette Leblanc','2002-07-09 23:17:45',180,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1151,'Burton Barlow','1995-07-06 23:17:15',79,15),
    (1152,'Abraham Benton','2004-01-21 12:48:03',15,19),
    (1153,'Charissa Acosta','2002-08-12 14:16:15',98,11),
    (1154,'Deirdre Cooke','2004-01-04 17:04:27',127,16),
    (1155,'Lara Gonzales','2003-04-10 20:18:18',55,18),
    (1156,'Lunea Berg','1996-09-17 06:37:30',62,18),
    (1157,'Oscar Chan','1998-05-11 05:28:41',52,18),
    (1158,'Gemma Moody','1999-04-08 17:33:00',1,12),
    (1159,'Clark Valenzuela','1996-03-10 17:04:30',150,18),
    (1160,'Nathan Lester','2004-08-28 07:19:32',149,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1161,'Lois Craig','2000-02-26 07:03:09',69,13),
    (1162,'Cole Horton','2000-08-12 11:34:49',31,16),
    (1163,'Jesse Logan','1995-02-23 01:16:43',36,19),
    (1164,'Orla Schwartz','1998-01-09 20:27:40',157,12),
    (1165,'Illiana Kent','1999-05-15 16:47:29',121,17),
    (1166,'Malik Ortega','2002-05-12 20:54:26',59,18),
    (1167,'Raphael Mccullough','1996-08-05 19:32:34',135,10),
    (1168,'Shannon Baker','2000-11-09 09:39:10',53,11),
    (1169,'Kimberley Burt','2000-05-18 18:53:07',47,18),
    (1170,'Brenda Skinner','1995-09-27 23:03:42',19,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1171,'Lucius Blake','2001-01-08 12:02:13',96,12),
    (1172,'Cassandra Orr','1998-08-03 09:50:13',124,19),
    (1173,'Michael Hawkins','1998-06-09 17:07:40',114,15),
    (1174,'Phyllis Fields','1997-10-08 18:14:04',114,17),
    (1175,'Haley Chang','2004-07-18 02:43:38',1,13),
    (1176,'Knox Juarez','2000-10-06 13:44:03',31,18),
    (1177,'Eleanor Steele','1997-12-21 22:54:49',145,10),
    (1178,'Laurel Foster','1998-05-28 03:22:58',78,13),
    (1179,'Ivana Vaughan','1996-12-24 12:37:54',32,11),
    (1180,'Zoe Knight','2000-04-01 17:59:49',125,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1181,'Carissa Mack','1997-06-04 21:23:50',3,16),
    (1182,'Keane Ware','1996-08-25 01:50:46',13,13),
    (1183,'Denise Stone','1999-09-04 19:03:05',129,16),
    (1184,'Uta Mcfadden','1999-06-11 20:08:07',60,12),
    (1185,'Quentin Malone','1996-01-15 00:25:49',97,12),
    (1186,'Fitzgerald Sanford','1995-07-02 08:34:07',32,14),
    (1187,'Xantha Finley','1998-01-31 13:35:24',16,16),
    (1188,'Cameran Cochran','1998-12-31 07:07:14',34,16),
    (1189,'Edan Potter','2002-11-23 14:51:49',126,10),
    (1190,'Herrod Fleming','2000-09-01 21:35:23',173,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1191,'Bevis Hyde','1995-12-23 04:48:04',14,18),
    (1192,'Daphne Emerson','2000-07-30 08:22:05',119,15),
    (1193,'Maxwell Hooper','2003-06-12 02:21:05',19,19),
    (1194,'Genevieve Mclean','2004-11-10 11:48:18',136,16),
    (1195,'Arsenio Boyer','1995-01-15 09:56:51',138,10),
    (1196,'Irma Maldonado','2003-08-29 00:09:08',131,18),
    (1197,'Arden Hodge','2002-06-02 09:09:29',69,13),
    (1198,'Ivana Shelton','2000-11-10 11:12:34',132,14),
    (1199,'Brian Hernandez','2003-01-13 03:00:11',86,17),
    (1200,'Ross Hood','2000-01-10 17:29:54',170,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1201,'Blaze Carson','1999-07-03 19:48:35',73,18),
    (1202,'Whitney Moran','1996-09-20 00:02:26',17,10),
    (1203,'Jacqueline Ferguson','1995-05-15 16:06:14',96,12),
    (1204,'Yuri Sanders','2002-10-12 11:02:30',108,17),
    (1205,'Amal Rasmussen','1997-03-12 23:04:34',33,17),
    (1206,'Fiona Meadows','1995-02-02 00:22:55',129,15),
    (1207,'Britanney Taylor','2000-09-25 23:12:07',105,12),
    (1208,'Wang Huffman','2003-09-04 16:55:35',4,17),
    (1209,'Regina Baird','1997-09-10 06:54:19',115,14),
    (1210,'Colette Baird','1998-10-13 17:18:17',39,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1211,'Ethan Hampton','2003-10-20 13:35:02',111,16),
    (1212,'Hammett Kim','2001-06-03 20:42:00',153,12),
    (1213,'Ivan Gould','1996-04-01 00:44:01',141,16),
    (1214,'Hermione Armstrong','2000-04-29 03:35:30',180,12),
    (1215,'Edward Shaffer','2001-06-22 01:38:58',32,15),
    (1216,'Imogene Raymond','2002-06-28 16:12:06',102,14),
    (1217,'Ifeoma Chaney','2002-10-23 20:08:09',136,20),
    (1218,'Harriet Juarez','2003-08-25 14:51:26',103,16),
    (1219,'Evan Vaughn','1997-12-17 02:09:46',43,14),
    (1220,'Iris Good','2001-07-22 09:48:38',94,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1221,'Tatiana Brewer','1996-09-04 16:36:57',67,13),
    (1222,'Larissa Trevino','1997-04-21 13:15:13',91,18),
    (1223,'Guy Phelps','1996-11-04 16:46:58',168,14),
    (1224,'Kyle Stafford','1996-02-17 19:57:36',94,13),
    (1225,'Ingrid Burch','1996-10-11 14:26:21',9,20),
    (1226,'Velma Solomon','1997-12-14 14:29:34',110,17),
    (1227,'Quin Mcbride','2002-03-04 14:32:01',32,14),
    (1228,'Jackson Palmer','2003-11-22 07:57:39',126,15),
    (1229,'Dustin Huff','1996-10-03 19:19:47',110,13),
    (1230,'Britanney Chen','1997-04-23 11:37:36',27,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1231,'Wyatt Chapman','2004-01-24 09:11:44',80,11),
    (1232,'Hayley Hickman','2002-11-13 09:47:03',152,17),
    (1233,'Keelie Curry','1998-12-14 13:11:48',41,17),
    (1234,'Sonia Knox','1996-04-10 12:12:04',6,10),
    (1235,'Inez Boyle','2003-08-27 03:15:34',41,18),
    (1236,'Megan Vazquez','2002-02-06 03:25:56',74,16),
    (1237,'Shaeleigh Butler','2000-11-06 10:30:30',120,17),
    (1238,'Jack Small','2001-11-10 08:41:20',81,15),
    (1239,'Simon Wilkinson','2000-12-24 17:20:07',127,15),
    (1240,'Hanae Mcbride','1997-04-15 01:26:15',70,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1241,'Tiger Meadows','1999-06-22 06:34:24',40,13),
    (1242,'Zane Yates','1996-01-24 14:59:27',131,16),
    (1243,'Thaddeus Walter','2000-11-02 15:07:30',96,12),
    (1244,'Libby O''Neill','2001-10-15 20:06:09',101,15),
    (1245,'Jena Garner','2002-04-29 17:04:00',36,16),
    (1246,'Bernard Parrish','2001-06-19 16:09:22',166,16),
    (1247,'Alexa Valenzuela','2001-01-17 05:04:35',163,15),
    (1248,'Isabella Michael','1999-08-04 04:48:28',81,15),
    (1249,'Micah Roberts','1997-04-06 22:19:44',128,11),
    (1250,'Bert Doyle','1997-04-27 06:46:30',77,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1251,'Rafael Mcfarland','2004-12-09 03:22:52',100,15),
    (1252,'Gwendolyn Cole','2003-09-09 09:51:46',178,16),
    (1253,'Lawrence Pruitt','2001-06-08 19:49:41',84,15),
    (1254,'Harper Hess','1995-07-12 07:04:44',105,13),
    (1255,'Odette Figueroa','2002-01-27 20:08:02',29,15),
    (1256,'Rudyard Oneal','2003-08-14 17:45:56',13,12),
    (1257,'Desirae Whitley','2000-08-26 01:07:11',104,16),
    (1258,'Gretchen Bailey','2002-10-26 07:25:41',62,16),
    (1259,'Gay Osborn','2002-02-24 16:57:59',138,17),
    (1260,'Whilemina Morrow','2001-06-17 18:40:30',57,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1261,'Harrison Gates','2003-09-21 04:10:49',145,11),
    (1262,'Ahmed Hendrix','1997-11-06 10:20:54',11,14),
    (1263,'Reagan Whitaker','1996-01-22 15:57:13',52,19),
    (1264,'Nayda Dudley','1997-02-25 18:32:25',156,18),
    (1265,'Nathan Mendez','1996-04-18 06:05:49',125,12),
    (1266,'Linus Buckley','2003-09-10 06:13:25',55,13),
    (1267,'Imelda Mckay','2003-02-26 21:54:49',152,17),
    (1268,'Nathan Stokes','1995-05-01 18:41:50',53,12),
    (1269,'Sybill Dyer','1996-09-02 16:42:39',36,16),
    (1270,'Zephania Vinson','2002-02-21 01:15:26',14,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1271,'India Faulkner','2003-07-06 04:48:14',176,18),
    (1272,'Avye Roberson','1997-03-27 09:33:55',8,20),
    (1273,'Adam Porter','2003-09-02 09:42:55',121,17),
    (1274,'Reagan Irwin','1998-01-17 00:15:20',147,14),
    (1275,'Raymond Hodges','1998-05-08 03:01:21',109,13),
    (1276,'Chandler Skinner','2001-03-26 04:12:02',48,17),
    (1277,'Signe Burgess','1999-09-21 05:02:21',15,11),
    (1278,'Vielka Bender','1997-03-17 17:21:05',150,12),
    (1279,'Keith Salinas','1996-11-14 16:17:13',106,20),
    (1280,'Raphael Boyer','1999-05-22 01:18:06',155,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1281,'Christen White','2003-07-15 22:50:39',130,17),
    (1282,'Quin Sellers','2004-06-10 18:16:32',168,16),
    (1283,'Aquila Carroll','1995-06-10 20:21:14',146,15),
    (1284,'Rebecca Harmon','1996-12-08 23:29:55',111,14),
    (1285,'Wayne Ford','2002-02-17 05:11:40',60,18),
    (1286,'Travis Cain','2003-10-05 01:32:54',9,17),
    (1287,'Kermit Guerrero','2004-11-12 14:29:43',17,11),
    (1288,'Ethan Ayers','1996-12-07 22:54:24',107,12),
    (1289,'Florence Walker','2004-09-16 07:49:52',29,16),
    (1290,'Ariana Rich','2004-01-16 14:21:59',25,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1291,'Shelby Hess','2001-09-17 23:21:28',140,11),
    (1292,'Austin Holmes','2004-05-06 13:48:52',118,18),
    (1293,'Alvin Benson','2001-03-15 20:28:54',54,13),
    (1294,'Laurel Moon','1998-07-29 07:06:42',26,17),
    (1295,'Fleur Walker','1998-05-19 17:05:56',179,11),
    (1296,'Kerry Nguyen','1999-04-26 11:36:16',102,14),
    (1297,'Cara Nolan','2002-01-14 05:44:08',171,15),
    (1298,'Scarlet Harmon','2004-06-11 12:18:29',121,10),
    (1299,'Grant Perez','1997-03-15 05:48:41',103,11),
    (1300,'Kirk Wheeler','2001-01-03 02:51:14',33,10);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1301,'Gavin Robertson','1998-03-14 06:43:35',79,13),
    (1302,'Geoffrey Thompson','2003-01-25 14:58:58',37,16),
    (1303,'Minerva Randall','2000-06-10 15:10:22',27,13),
    (1304,'Emily Hall','1995-05-14 17:07:01',38,18),
    (1305,'Arthur Graham','1999-12-18 01:59:46',14,16),
    (1306,'Janna Allison','1995-04-19 17:14:41',96,18),
    (1307,'Moana Mcgee','2000-11-24 21:02:10',82,11),
    (1308,'Daquan Petersen','1998-03-22 22:02:42',20,11),
    (1309,'Lois Moses','2001-01-15 19:28:58',4,20),
    (1310,'Ainsley Atkins','2001-12-13 20:06:37',56,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1311,'Andrew Gregory','1995-05-14 06:56:55',107,12),
    (1312,'Thaddeus Landry','1997-08-01 04:14:46',91,12),
    (1313,'Wesley Gillespie','1996-01-31 07:35:45',170,13),
    (1314,'Len Gilmore','1996-11-09 23:30:19',51,14),
    (1315,'Zahir Grimes','1999-04-15 12:43:42',56,16),
    (1316,'Dexter Fuller','1999-09-11 21:30:28',111,16),
    (1317,'Eve Peterson','1998-03-31 15:51:17',73,19),
    (1318,'Nolan Savage','2002-08-20 07:30:02',141,19),
    (1319,'Tara Flowers','2003-05-23 10:53:21',46,18),
    (1320,'Evangeline Wells','1999-01-12 11:27:52',16,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1321,'Isaac Whitaker','2001-10-14 16:09:23',136,11),
    (1322,'Portia Alvarado','2002-09-17 19:55:55',38,12),
    (1323,'Mollie Burgess','2001-11-05 11:07:26',77,20),
    (1324,'Hanna Graham','2002-08-19 05:14:27',65,16),
    (1325,'Jenna Fernandez','2001-02-25 06:24:43',74,17),
    (1326,'Aretha Middleton','2002-10-27 17:27:07',132,13),
    (1327,'Jameson Frye','2003-05-01 19:27:42',140,15),
    (1328,'Axel Mcfarland','2002-01-05 22:57:10',25,13),
    (1329,'Rhona Andrews','2000-12-01 11:09:54',29,13),
    (1330,'Ignatius Johns','2000-05-19 22:34:44',79,10);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1331,'Raja Sears','2000-06-05 14:18:32',32,13),
    (1332,'Nerea Powers','2000-03-24 11:24:00',132,15),
    (1333,'Quemby Cote','2004-11-19 20:17:22',155,18),
    (1334,'Ishmael Valencia','2000-05-23 12:43:48',111,15),
    (1335,'Stephen Oliver','2002-07-27 10:32:26',148,19),
    (1336,'Callie Church','2004-11-03 05:34:46',31,16),
    (1337,'Lacey Walter','2000-05-31 01:07:57',158,17),
    (1338,'Isabella Walter','1999-03-10 03:47:32',42,18),
    (1339,'Chadwick Holden','2002-01-10 16:26:29',12,13),
    (1340,'Daniel Jensen','1997-07-31 16:04:11',34,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1341,'Quyn Walters','1999-01-12 07:28:29',134,14),
    (1342,'Lila Mendoza','2000-08-31 01:14:26',92,10),
    (1343,'Claudia Perry','2001-10-29 00:43:14',37,12),
    (1344,'Kelsie Hopkins','2004-02-27 03:28:13',108,13),
    (1345,'Amir Massey','2003-02-03 05:22:07',66,18),
    (1346,'Pamela Cantu','1999-02-02 07:34:25',155,16),
    (1347,'Damon Spencer','1999-08-08 22:23:22',134,13),
    (1348,'Micah Rosales','2004-02-20 08:14:38',110,17),
    (1349,'Nolan Shepherd','2001-03-30 15:37:01',174,15),
    (1350,'Palmer Jennings','2000-01-07 14:11:48',20,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1351,'Derek Sandoval','2004-03-05 08:19:58',36,18),
    (1352,'Felicia Gilbert','1995-09-17 15:08:21',11,19),
    (1353,'Austin Marks','1999-06-10 06:13:16',27,13),
    (1354,'Blaze Mercado','1999-09-08 09:38:08',152,20),
    (1355,'Morgan Rosa','1998-09-21 15:23:55',101,13),
    (1356,'Grady Norman','1995-12-17 22:17:21',43,14),
    (1357,'Abbot Nelson','2001-09-17 21:42:33',46,12),
    (1358,'Jescie Whitfield','1995-06-07 17:18:29',31,18),
    (1359,'Callie Battle','1997-11-21 06:10:17',64,17),
    (1360,'Ferris Leach','1997-07-14 00:36:13',48,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1361,'Boris Perkins','2000-07-18 06:38:04',178,16),
    (1362,'Benjamin Ferguson','2001-02-18 10:15:30',7,19),
    (1363,'Eugenia Mueller','2004-07-10 17:56:14',97,19),
    (1364,'Shay Beasley','2001-10-30 05:26:14',60,15),
    (1365,'Martin Hopper','2000-03-30 02:46:20',20,11),
    (1366,'Lee Hardy','1995-11-20 21:39:09',63,17),
    (1367,'Paki Dejesus','2000-10-04 06:25:43',87,14),
    (1368,'Dawn Mosley','2002-10-25 17:44:54',50,16),
    (1369,'Dorothy Mcfarland','2000-06-21 05:11:08',68,14),
    (1370,'Zia Nieves','2004-03-07 15:19:15',26,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1371,'Aladdin Carter','1998-01-24 23:18:09',42,20),
    (1372,'Giacomo Bradley','1995-07-13 13:42:21',171,15),
    (1373,'Hope Dalton','2001-03-25 21:50:27',15,17),
    (1374,'Brenda Strong','1999-05-27 08:24:49',142,20),
    (1375,'Beverly Whitaker','2003-03-22 14:35:56',35,12),
    (1376,'Quyn Gillespie','2003-11-26 17:23:18',33,18),
    (1377,'Kuame Kelley','2000-03-02 20:03:20',128,12),
    (1378,'Olga Ingram','1998-05-01 16:28:18',38,18),
    (1379,'Kyle Rowland','1999-10-07 08:43:21',138,12),
    (1380,'Maile Farley','1997-10-29 03:55:55',105,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1381,'Justine Pierce','1996-10-29 13:14:51',92,14),
    (1382,'Mannix William','1998-03-11 16:35:30',130,12),
    (1383,'Julian Fitzpatrick','1995-10-16 22:37:24',63,18),
    (1384,'Lacy Welch','2003-05-16 21:41:37',140,12),
    (1385,'Gillian Mcleod','1999-05-28 05:17:36',103,14),
    (1386,'Isabella Norton','2001-01-13 05:49:14',5,19),
    (1387,'Camille Patrick','2002-02-17 04:49:37',66,15),
    (1388,'Dale Estes','1995-07-05 11:17:38',144,19),
    (1389,'Charde Mullins','1996-08-10 00:43:21',64,19),
    (1390,'Phillip Oliver','2002-05-31 06:24:41',67,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1391,'Shafira Dennis','2000-07-25 05:22:06',70,11),
    (1392,'Anjolie Wilkins','2001-05-30 03:12:44',21,16),
    (1393,'Rebecca Herring','1998-04-15 13:15:02',28,18),
    (1394,'Renee Lambert','1998-04-16 02:44:29',137,19),
    (1395,'Alma Lee','2000-02-04 08:28:36',47,18),
    (1396,'Judith Stone','2002-09-10 03:23:37',65,13),
    (1397,'Conan Navarro','2002-11-18 15:02:47',111,12),
    (1398,'Adria Norton','1995-08-28 21:13:10',93,11),
    (1399,'Chadwick Erickson','1998-06-10 00:46:33',39,12),
    (1400,'Mona Davis','2004-10-18 00:38:59',124,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1401,'Catherine Benjamin','2001-12-16 23:05:08',73,11),
    (1402,'Glenna Langley','2000-02-17 05:15:22',133,17),
    (1403,'Francesca Wyatt','1999-04-10 03:29:24',167,14),
    (1404,'Lars Rodriquez','1996-09-19 15:38:21',146,15),
    (1405,'Kameko Barber','2002-06-05 07:51:33',171,15),
    (1406,'Kimberley Dawson','2004-02-02 04:05:41',109,11),
    (1407,'Justin Sheppard','1995-07-31 01:00:13',98,19),
    (1408,'Acton Sargent','2001-02-14 20:44:56',67,18),
    (1409,'Alec Goff','2003-11-15 01:07:42',137,17),
    (1410,'Chelsea Prince','1997-10-08 21:59:05',166,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1411,'Rachel Steele','2001-04-10 02:55:34',116,15),
    (1412,'George Burnett','1996-09-29 16:21:51',122,13),
    (1413,'Ian Ellis','2002-02-17 10:41:14',89,12),
    (1414,'Renee Barry','1996-05-19 07:16:31',145,14),
    (1415,'Camille Hunter','2003-05-26 16:53:16',130,18),
    (1416,'William Roberts','2004-11-25 09:44:22',161,11),
    (1417,'Addison Lawson','1998-04-18 04:34:46',110,16),
    (1418,'Keith William','2002-06-20 03:09:51',7,15),
    (1419,'Reuben Fleming','2003-07-27 22:54:28',43,17),
    (1420,'Sopoline Boyle','2001-10-29 13:16:26',23,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1421,'Nora Kline','1998-04-20 04:13:54',92,15),
    (1422,'Honorato Buckner','2001-03-31 23:41:15',23,16),
    (1423,'Kennan Reynolds','1995-05-17 05:12:41',4,12),
    (1424,'Garrett Garza','2002-07-25 17:12:07',66,17),
    (1425,'Kieran Fox','2003-07-24 08:54:16',5,18),
    (1426,'Bertha Mitchell','2004-07-29 08:45:38',67,20),
    (1427,'Alvin Mitchell','1999-12-13 19:05:29',163,14),
    (1428,'Kimberley Patrick','2000-10-14 02:53:29',83,11),
    (1429,'Joseph Becker','2000-03-13 22:51:26',27,11),
    (1430,'Julian Carr','2001-04-17 16:53:42',37,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1431,'Jillian Newman','1997-10-23 20:12:19',126,13),
    (1432,'Inga Matthews','1999-09-04 23:50:20',12,19),
    (1433,'Raymond Zamora','2002-04-28 15:44:22',173,13),
    (1434,'Leilani Holden','1997-09-08 20:02:37',91,11),
    (1435,'Zephania Maynard','1999-07-03 10:59:02',118,13),
    (1436,'Paul Harmon','2003-11-03 14:25:06',153,17),
    (1437,'Jordan Eaton','2002-09-03 01:44:34',146,18),
    (1438,'Megan Hardin','1998-05-02 02:42:50',5,18),
    (1439,'Simon Rocha','1995-12-23 09:39:45',72,19),
    (1440,'Jolie Branch','1997-02-19 18:44:15',16,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1441,'John Strickland','1995-05-29 22:03:09',1,18),
    (1442,'Velma Tanner','1997-11-19 02:06:05',36,14),
    (1443,'Uma Medina','2003-04-22 19:41:21',74,16),
    (1444,'Micah Tyler','2001-09-25 13:57:44',23,15),
    (1445,'Amelia Hyde','1996-07-01 01:39:22',114,19),
    (1446,'Holmes Paul','1995-04-02 02:51:44',54,17),
    (1447,'Hamilton Heath','1995-11-18 18:27:15',124,14),
    (1448,'Jason Klein','2002-09-23 07:13:12',67,15),
    (1449,'Kato Poole','1996-05-29 20:58:05',134,19),
    (1450,'Hayden Haley','2001-03-11 14:31:49',167,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1451,'Doris Emerson','1996-03-17 14:03:10',143,14),
    (1452,'Armando Sargent','1995-02-07 21:05:54',19,18),
    (1453,'Victor Mccullough','1997-08-26 04:23:44',156,13),
    (1454,'Simon Williams','1999-06-05 02:25:59',47,12),
    (1455,'Eric Cummings','2000-08-28 05:11:17',121,19),
    (1456,'Adria Murphy','2001-04-02 02:15:33',29,13),
    (1457,'Leo Soto','1997-01-03 08:30:32',96,15),
    (1458,'Judith Daniels','1996-09-09 16:50:42',100,14),
    (1459,'Salvador Jackson','1995-09-26 13:22:31',170,18),
    (1460,'Ivana Mann','2001-04-03 02:10:00',158,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1461,'Jena Delacruz','1997-12-25 15:47:52',138,11),
    (1462,'Demetria Franklin','1998-01-17 13:03:17',83,18),
    (1463,'Ulla Callahan','2001-11-08 05:00:04',43,17),
    (1464,'Kyle Dixon','2004-12-04 05:20:04',22,19),
    (1465,'Ezekiel Dawson','2002-11-28 01:48:07',20,17),
    (1466,'Willow Walter','1997-04-04 07:41:31',147,14),
    (1467,'Violet Caldwell','1997-06-09 05:58:09',105,17),
    (1468,'Danielle Mercado','1998-04-01 08:21:51',111,14),
    (1469,'Sawyer Calderon','2004-10-02 11:15:28',106,16),
    (1470,'Daryl Rodgers','1996-11-25 01:31:06',170,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1471,'Whoopi Rivas','2000-02-16 12:25:43',172,11),
    (1472,'Ulysses Duffy','2003-06-22 05:30:01',109,16),
    (1473,'Yetta Guy','1999-07-10 12:26:06',64,18),
    (1474,'Imani Chavez','1997-04-16 18:44:08',83,15),
    (1475,'Kiayada Foreman','2002-08-30 08:21:27',14,16),
    (1476,'Cally Hayden','1996-02-23 03:20:13',1,16),
    (1477,'Fiona Avila','1995-04-23 06:30:35',51,17),
    (1478,'Raja Rodriguez','2004-09-23 20:23:23',131,17),
    (1479,'Uriah Carr','1997-08-10 02:40:10',87,19),
    (1480,'Kadeem Chang','1998-12-31 00:52:48',14,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1481,'Shana Gilbert','1996-10-05 06:52:54',139,11),
    (1482,'Roary Wilcox','1999-02-03 20:13:32',135,12),
    (1483,'Ainsley Macdonald','2004-08-23 09:08:00',63,19),
    (1484,'Lionel Mejia','1995-03-21 05:24:39',121,19),
    (1485,'Brielle Cobb','2003-07-21 17:28:19',9,18),
    (1486,'Kessie Tate','2000-10-02 02:53:57',75,10),
    (1487,'Tamara Dillon','1997-05-22 18:29:50',36,17),
    (1488,'Gray Peters','2002-10-28 14:21:28',51,15),
    (1489,'Kessie Peterson','2000-10-13 22:42:53',101,14),
    (1490,'Ciaran Moore','2004-11-25 10:49:19',122,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1491,'Conan Barton','2000-02-16 10:40:47',53,17),
    (1492,'Bryar Clements','2002-10-14 06:49:10',96,16),
    (1493,'Ruth Beach','2002-10-14 09:13:13',154,19),
    (1494,'Fulton Skinner','1998-01-07 18:48:55',100,17),
    (1495,'Paul Walter','2004-03-09 13:31:16',112,19),
    (1496,'Raven Lara','2003-01-08 03:45:32',43,11),
    (1497,'Adrian Pearson','2004-02-09 21:57:26',126,16),
    (1498,'Asher Harding','2004-05-01 11:13:22',107,12),
    (1499,'Ulla Herring','1999-06-17 04:21:09',81,11),
    (1500,'Benjamin Shaw','2001-02-11 05:12:21',41,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1501,'Anastasia Lucas','2000-01-13 06:50:41',113,12),
    (1502,'Adara Bean','2002-11-13 20:35:09',163,13),
    (1503,'Lacey Gallagher','2004-11-15 14:55:31',83,15),
    (1504,'Ignatius Hodges','2001-09-09 22:00:59',23,16),
    (1505,'Akeem Rivers','1996-08-03 18:06:57',21,14),
    (1506,'Gage Glass','2003-03-07 01:16:19',85,13),
    (1507,'Barrett Foreman','1996-02-09 13:19:52',69,18),
    (1508,'Amery Booth','1996-08-13 21:55:37',33,14),
    (1509,'Wayne Bennett','1999-01-11 15:50:27',44,11),
    (1510,'Clayton Robles','1995-07-25 17:52:30',114,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1511,'Shelley Middleton','1997-11-13 14:01:03',109,18),
    (1512,'Bianca Barr','1999-03-29 14:01:34',47,19),
    (1513,'Hunter Romero','2000-11-30 19:06:49',95,15),
    (1514,'Rafael Newman','2004-12-24 07:50:17',46,15),
    (1515,'Zena Key','1996-05-17 05:01:25',64,15),
    (1516,'Diana Logan','2000-09-19 10:32:34',28,17),
    (1517,'Cameron Huber','2002-12-31 12:06:56',78,10),
    (1518,'Sade Alford','1995-07-28 17:10:36',13,15),
    (1519,'Mallory Anthony','2004-05-27 11:13:35',53,14),
    (1520,'Brynn Salinas','1998-03-26 02:55:29',165,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1521,'Finn Hurst','2003-05-16 00:50:13',28,12),
    (1522,'Iris Meadows','1999-08-04 06:24:48',76,15),
    (1523,'Emery Castillo','1996-07-26 03:04:40',177,20),
    (1524,'Jaquelyn Cash','1995-07-15 19:01:54',17,18),
    (1525,'Doris Lowe','2004-08-26 06:12:36',122,12),
    (1526,'Colby Goodman','1995-04-05 10:26:35',65,12),
    (1527,'Irene Huber','2004-03-09 15:53:25',56,12),
    (1528,'Fallon Lane','1998-07-17 22:21:26',69,14),
    (1529,'Leslie Parrish','1998-10-12 23:13:00',63,16),
    (1530,'Lunea Potts','1997-01-23 08:42:47',168,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1531,'Cameron Carson','1997-01-30 19:28:52',67,19),
    (1532,'Irene Velasquez','1999-11-14 11:09:44',166,14),
    (1533,'John Drake','2001-01-10 18:27:04',73,12),
    (1534,'Denise Dickerson','1995-03-12 09:30:22',48,19),
    (1535,'Nina Briggs','2002-12-03 08:55:37',168,15),
    (1536,'Ashton Delaney','2003-04-07 13:20:10',84,11),
    (1537,'Alika Bennett','2002-05-31 19:18:58',172,13),
    (1538,'Cleo Holden','2000-12-03 17:13:35',86,11),
    (1539,'Mason Todd','2000-12-01 21:27:55',99,13),
    (1540,'Jemima Blair','2003-12-11 09:03:46',142,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1541,'Luke Bishop','1995-07-27 16:44:21',142,12),
    (1542,'Quinn Randall','1999-10-16 15:29:45',51,12),
    (1543,'Carla King','2000-09-18 14:59:59',143,13),
    (1544,'Barclay Valenzuela','1997-06-08 18:46:45',170,20),
    (1545,'Quentin Lester','2001-08-27 23:07:24',142,11),
    (1546,'Vera Wooten','2002-05-24 23:56:13',14,10),
    (1547,'Rafael Workman','2003-07-04 09:36:17',47,18),
    (1548,'Ava Fisher','2002-09-12 13:24:35',81,13),
    (1549,'Chaim Decker','1999-03-22 00:34:57',126,16),
    (1550,'April Fitzpatrick','2000-06-12 20:00:43',122,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1551,'Preston Shaffer','1995-08-26 21:24:55',104,17),
    (1552,'Martin Woods','1999-08-12 23:59:26',94,13),
    (1553,'Wyoming Fuller','2002-06-26 12:15:15',42,15),
    (1554,'Isadora Haney','2003-11-01 23:22:32',142,16),
    (1555,'Peter Donovan','1999-01-03 21:47:33',46,16),
    (1556,'Gabriel Day','2004-12-16 23:03:49',38,16),
    (1557,'Cody Hoffman','2000-09-19 04:55:07',166,13),
    (1558,'Britanney Walters','1997-04-28 02:25:01',67,10),
    (1559,'Ralph Hinton','1996-05-24 10:15:11',113,18),
    (1560,'Bradley Andrews','2004-05-27 10:08:50',40,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1561,'Lani Phillips','1996-10-30 00:29:20',15,12),
    (1562,'Gregory Melton','2003-07-12 17:08:36',158,16),
    (1563,'Chancellor Cabrera','2002-08-11 04:31:59',73,12),
    (1564,'Hollee Foley','1999-06-28 14:43:17',73,12),
    (1565,'Eagan Gonzalez','1999-05-23 04:04:27',115,11),
    (1566,'Zeus Brady','1995-10-30 12:03:28',174,15),
    (1567,'Jacqueline Griffith','1996-08-23 21:51:43',172,18),
    (1568,'Martena Rodgers','2000-03-15 18:49:40',145,14),
    (1569,'Heather Rich','1996-06-04 03:44:15',56,15),
    (1570,'Adele Spence','2002-03-21 09:22:15',116,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1571,'Jena Estes','2002-12-22 13:33:45',35,19),
    (1572,'Amir Rosales','1997-01-22 21:09:33',175,19),
    (1573,'Oleg Gibbs','1996-07-24 18:20:45',32,14),
    (1574,'Shelby Miles','2004-09-10 21:35:12',84,12),
    (1575,'Lydia Luna','2000-01-18 18:01:08',150,20),
    (1576,'Benjamin Woodard','2001-07-03 20:31:50',24,19),
    (1577,'Tatyana Small','2002-02-10 16:21:04',39,13),
    (1578,'Philip Reed','1998-11-26 11:46:15',173,13),
    (1579,'Xena Alford','1995-10-13 01:08:37',44,13),
    (1580,'Dean Solis','2000-01-09 19:01:28',5,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1581,'Maggy Ruiz','2000-11-06 21:21:53',78,14),
    (1582,'Georgia Dean','2004-07-26 15:36:48',14,18),
    (1583,'Joseph Buck','2001-04-17 12:41:43',39,16),
    (1584,'Mohammad Daugherty','1997-02-04 03:53:00',89,19),
    (1585,'Erica Brooks','1998-10-19 18:43:40',91,18),
    (1586,'Gray Griffith','1998-04-12 15:15:40',4,16),
    (1587,'Beverly Stanley','1998-06-07 10:18:45',156,11),
    (1588,'Selma Martin','1996-05-03 23:23:08',91,15),
    (1589,'Kylynn Morales','2000-12-14 12:56:46',89,11),
    (1590,'Len Mcdowell','1996-12-14 08:04:35',111,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1591,'Yoshio Wall','1999-07-24 03:35:51',65,10),
    (1592,'Deanna Horne','1995-10-26 00:31:38',43,12),
    (1593,'Theodore Meyers','1996-07-22 04:54:08',63,17),
    (1594,'Cally Romero','1999-02-18 07:34:48',164,11),
    (1595,'Wyatt Kane','2001-07-26 09:44:09',143,12),
    (1596,'Philip Ellison','1996-09-23 12:49:54',85,12),
    (1597,'Christen Mays','1998-06-29 05:16:45',1,18),
    (1598,'Zena Bentley','1995-12-10 08:32:19',22,12),
    (1599,'Vaughan Little','1997-01-30 20:36:37',143,13),
    (1600,'Raja Fox','2001-11-17 11:12:37',123,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1601,'Hedda Pierce','2002-09-28 23:15:54',174,15),
    (1602,'Breanna Golden','1997-08-29 19:49:39',146,14),
    (1603,'Berk Keller','1997-06-11 12:24:58',5,17),
    (1604,'Sade Tucker','1999-06-06 02:02:44',54,19),
    (1605,'Elliott Jones','1996-03-14 19:44:34',138,13),
    (1606,'Chancellor Stout','1995-01-21 15:04:22',2,17),
    (1607,'Jeanette Foley','1996-04-02 08:27:15',43,14),
    (1608,'Penelope Whitehead','1998-04-23 21:52:24',68,11),
    (1609,'Lysandra Massey','2004-01-27 00:18:40',111,19),
    (1610,'Ryder Hamilton','2004-06-25 19:37:06',71,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1611,'Rylee Levy','1996-06-03 11:24:49',10,13),
    (1612,'Judith Weeks','1999-07-16 13:32:21',5,17),
    (1613,'Odessa Kelley','1995-01-21 18:26:47',123,17),
    (1614,'Pandora Murphy','1996-01-19 01:21:17',113,10),
    (1615,'Hop Fitzgerald','1999-12-10 08:49:50',88,13),
    (1616,'Ferris Fuentes','1998-08-26 13:31:02',65,13),
    (1617,'Cairo Ayers','1999-03-09 10:35:45',45,13),
    (1618,'Simone Sanders','1998-06-06 15:19:39',76,17),
    (1619,'Genevieve Ferguson','1999-05-13 01:09:42',87,12),
    (1620,'Michelle Nunez','2002-11-29 10:31:12',133,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1621,'Wyatt Velasquez','2003-10-08 06:51:59',79,15),
    (1622,'Hope Hendrix','2001-09-27 16:16:32',24,11),
    (1623,'Talon Rush','2002-03-10 21:42:18',115,13),
    (1624,'Nicole Ewing','2004-04-16 04:11:51',142,13),
    (1625,'Christen Burt','1996-08-13 14:08:57',28,10),
    (1626,'Burke Bailey','1995-08-04 14:19:55',114,15),
    (1627,'Mary Whitfield','1997-03-02 12:14:18',170,15),
    (1628,'Kirby Rose','2001-09-28 14:29:23',161,20),
    (1629,'Lacey Bradford','2004-12-19 02:38:21',151,16),
    (1630,'Kerry Mcknight','2001-12-09 02:13:24',55,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1631,'Lana Farrell','2003-07-04 02:42:23',85,19),
    (1632,'Tarik Leach','2001-01-28 06:22:50',68,13),
    (1633,'Aurelia Rush','2004-06-20 14:12:29',15,18),
    (1634,'Fay Ryan','1999-02-06 16:52:16',159,17),
    (1635,'Aphrodite Crane','2003-05-27 11:17:17',155,15),
    (1636,'Medge Daniel','2001-06-18 09:06:26',174,19),
    (1637,'Meghan Rose','1995-09-15 14:41:17',86,16),
    (1638,'Zane Cole','1997-11-10 05:19:19',124,15),
    (1639,'Pearl Nielsen','2003-03-05 22:35:57',43,18),
    (1640,'Hunter Vang','2000-11-22 22:19:57',165,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1641,'Travis Fernandez','1998-11-11 00:12:58',159,18),
    (1642,'Winifred Pope','2004-02-07 17:05:26',83,19),
    (1643,'Phelan Hendrix','2003-04-01 20:57:12',141,11),
    (1644,'Henry Little','1995-03-22 13:56:38',96,16),
    (1645,'Zachary Giles','2003-10-02 20:24:55',137,20),
    (1646,'Austin Snyder','1999-02-13 23:02:01',96,12),
    (1647,'Hyacinth Huber','2003-01-12 13:23:33',158,11),
    (1648,'Philip Barker','2004-10-07 14:46:43',104,11),
    (1649,'Quon Morin','2001-09-09 07:23:18',169,17),
    (1650,'Aphrodite Hansen','1997-05-10 09:22:52',147,10);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1651,'Merrill Golden','1996-03-11 09:08:19',130,19),
    (1652,'Ezekiel Harvey','2001-12-08 06:54:09',34,15),
    (1653,'Indira Hale','2003-09-24 21:35:05',53,16),
    (1654,'Wang Thompson','2001-06-10 02:35:29',161,15),
    (1655,'Stephen Pratt','2001-09-09 22:03:00',124,13),
    (1656,'Nola Castillo','1997-02-05 01:08:03',134,16),
    (1657,'Danielle Sims','2003-08-06 04:37:01',74,12),
    (1658,'Gage Mclaughlin','1996-04-06 13:50:33',56,15),
    (1659,'Piper Cherry','2003-01-07 10:20:57',19,15),
    (1660,'Nita Barron','2004-02-01 23:59:00',75,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1661,'Otto Berg','2000-04-22 17:53:52',116,18),
    (1662,'Reece Diaz','1997-03-08 01:42:03',126,15),
    (1663,'Prescott Whitaker','2000-07-24 07:25:40',54,16),
    (1664,'Linus Prince','1995-11-23 03:11:44',56,16),
    (1665,'Geoffrey Fisher','1999-05-11 07:49:24',37,14),
    (1666,'Sasha Copeland','2000-08-25 20:08:31',39,16),
    (1667,'Charity Gay','2003-10-25 03:39:11',94,20),
    (1668,'Quynn Pitts','2002-02-20 07:09:45',118,18),
    (1669,'Austin Nelson','2003-07-07 15:00:18',121,15),
    (1670,'Damon Gates','2001-06-05 20:24:16',151,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1671,'Beau Cook','2001-09-16 21:36:50',130,16),
    (1672,'Sacha Caldwell','1996-10-29 03:36:13',5,20),
    (1673,'Myles Horne','2000-12-04 17:25:35',123,17),
    (1674,'Liberty Carrillo','1996-09-01 08:34:02',7,14),
    (1675,'Illiana Mayer','2001-09-15 20:21:44',140,18),
    (1676,'Joseph Mullen','2002-09-09 04:56:30',167,10),
    (1677,'Castor Benson','2003-07-12 16:59:37',67,19),
    (1678,'Jaden West','2004-12-21 18:08:17',30,17),
    (1679,'Guinevere Barton','2003-04-22 05:58:25',49,18),
    (1680,'Theodore Dyer','2003-02-15 12:10:06',178,14);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1681,'Kibo Rivas','2001-11-29 13:19:34',117,13),
    (1682,'Hilel Chapman','1999-04-18 00:15:04',19,11),
    (1683,'Zachary Ward','2001-08-03 20:54:14',161,19),
    (1684,'Jonas Jarvis','1997-06-18 03:13:36',132,16),
    (1685,'Garrett Fields','2001-06-13 03:58:34',66,12),
    (1686,'Eaton Rivera','2002-10-06 21:59:40',148,19),
    (1687,'Wade Graves','2001-05-14 22:56:05',72,15),
    (1688,'Cameron Ball','1998-10-31 00:30:50',35,17),
    (1689,'Olga Carr','1999-03-01 03:00:40',28,12),
    (1690,'Norman Kane','1996-09-11 18:09:30',7,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1691,'Micah Hunter','1995-11-06 17:39:24',50,15),
    (1692,'Tate Guerrero','2003-08-27 06:59:34',0,11),
    (1693,'Eve Benjamin','2000-05-08 23:24:21',72,13),
    (1694,'Paki Hinton','1999-07-18 21:04:37',47,16),
    (1695,'Althea Spence','2004-10-27 23:16:38',22,19),
    (1696,'Uma Nunez','2000-07-10 16:31:15',35,18),
    (1697,'Haviva Duffy','2001-10-19 04:41:59',123,17),
    (1698,'Felix Powell','1998-05-16 00:38:24',152,20),
    (1699,'Octavia Stanton','1995-10-20 06:36:45',37,15),
    (1700,'Dolan Duncan','1995-12-03 03:32:26',152,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1701,'Christine Pitts','2002-06-16 11:50:31',174,15),
    (1702,'Ivor Weiss','1999-03-15 21:58:53',169,14),
    (1703,'Raymond Cobb','1997-04-07 04:34:03',27,16),
    (1704,'Wang Rogers','1998-07-10 05:02:03',44,20),
    (1705,'Veronica Norris','2002-03-04 01:54:43',21,16),
    (1706,'Nell Aguilar','1999-05-03 02:45:31',29,18),
    (1707,'Denton Swanson','1995-02-20 11:20:14',140,14),
    (1708,'Angela Acosta','1996-06-15 09:57:21',77,12),
    (1709,'Henry Rosa','2003-05-29 11:26:31',138,17),
    (1710,'Brenden Sanchez','2003-09-25 06:40:20',67,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1711,'Lucius Conrad','1998-02-18 23:15:42',64,16),
    (1712,'Fallon Hardin','1999-01-28 21:17:49',105,15),
    (1713,'Vielka Bradford','2001-08-05 00:34:59',78,18),
    (1714,'Nasim Lynch','2003-09-05 09:34:46',44,19),
    (1715,'Sara Savage','2004-10-26 13:00:41',49,15),
    (1716,'Micah Harrison','2002-11-19 07:57:59',34,11),
    (1717,'Brenda Schwartz','1997-10-08 14:53:33',82,10),
    (1718,'Gwendolyn Snyder','1996-01-02 02:29:57',9,17),
    (1719,'Irma Bradshaw','1997-09-25 18:57:32',72,15),
    (1720,'Clementine Cantu','2003-11-03 17:26:51',22,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1721,'Jescie Lopez','2004-04-29 11:58:09',173,18),
    (1722,'Naomi Lester','2002-10-22 01:07:05',50,17),
    (1723,'Guy Tyson','1997-12-23 22:40:35',84,13),
    (1724,'Eleanor Benjamin','1998-09-09 11:00:01',100,18),
    (1725,'September Emerson','2002-11-19 12:20:38',75,11),
    (1726,'Lesley Hess','2002-12-12 10:44:55',98,11),
    (1727,'Salvador Vance','2002-07-16 03:22:46',31,11),
    (1728,'Matthew Carroll','1998-07-11 02:46:13',158,17),
    (1729,'Elton Little','2004-02-01 00:06:09',144,20),
    (1730,'Jada Welch','2001-09-24 06:13:07',144,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1731,'Nathan Mckenzie','2001-05-17 05:36:49',40,16),
    (1732,'Eve Gilbert','2001-12-21 00:16:28',32,15),
    (1733,'Latifah Daugherty','2000-03-12 07:39:20',16,19),
    (1734,'Skyler Copeland','1995-01-04 21:49:27',9,11),
    (1735,'Yardley Jackson','2004-06-23 11:14:59',179,11),
    (1736,'Sigourney Herring','2002-11-27 07:20:45',46,12),
    (1737,'Yardley Silva','2000-01-30 18:11:40',50,15),
    (1738,'Miranda Chavez','1996-05-10 05:07:37',125,10),
    (1739,'Reese Madden','1998-06-10 03:49:15',174,15),
    (1740,'Desiree Murray','1999-07-19 21:07:12',4,16);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1741,'Jin Deleon','1996-12-11 07:39:10',104,19),
    (1742,'Graham Dejesus','1996-01-01 09:21:40',35,19),
    (1743,'Luke Johnston','2003-06-23 23:50:36',7,11),
    (1744,'Ezra Jefferson','2000-07-23 05:02:45',24,15),
    (1745,'Lars Larson','1997-08-20 06:40:43',49,10),
    (1746,'Thomas Raymond','2000-02-01 04:55:16',60,10),
    (1747,'Upton Cortez','2002-12-30 21:00:46',142,20),
    (1748,'Paul Christensen','1997-01-11 21:43:55',20,18),
    (1749,'Delilah Finley','1997-08-31 15:19:49',13,15),
    (1750,'Rogan Sexton','2004-11-07 07:48:18',56,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1751,'Dahlia Osborn','2003-06-11 04:04:28',133,12),
    (1752,'Quinlan Castaneda','1998-02-08 05:43:33',105,15),
    (1753,'Madison Sheppard','1996-04-19 15:10:25',163,13),
    (1754,'Jena Hester','2002-01-09 02:04:10',138,12),
    (1755,'Tiger Mcguire','2001-03-04 21:09:31',163,19),
    (1756,'Carolyn Hopkins','2004-11-08 23:12:34',149,11),
    (1757,'Timon Livingston','2001-11-11 01:42:28',148,20),
    (1758,'Coby Barker','1998-01-05 18:55:47',122,16),
    (1759,'Hiroko Kelley','2004-11-19 07:42:40',97,12),
    (1760,'Doris Clarke','2001-11-13 00:39:40',152,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1761,'Audrey Adams','1998-11-16 01:34:37',87,13),
    (1762,'Donovan Romero','1999-08-18 03:47:00',43,13),
    (1763,'Jonas Hart','1995-07-10 00:46:26',151,13),
    (1764,'Alexa Burt','1998-09-17 04:04:45',15,11),
    (1765,'Amity Berg','2002-03-17 06:32:10',172,13),
    (1766,'Gay Conrad','1999-06-30 23:52:36',9,14),
    (1767,'Carter Mcintosh','1999-10-28 15:32:36',55,14),
    (1768,'Unity French','2004-11-06 14:52:20',128,19),
    (1769,'Morgan Booth','1999-04-27 11:54:48',7,15),
    (1770,'Keegan Leblanc','1998-10-31 02:02:56',49,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1771,'Brenda Kidd','1999-02-01 06:10:26',148,11),
    (1772,'Galena Young','1995-05-19 16:23:41',65,11),
    (1773,'Bernard Mclaughlin','2004-01-30 07:39:25',41,17),
    (1774,'Imelda Herrera','1995-09-20 20:03:22',57,11),
    (1775,'Leigh Albert','1998-12-07 16:06:08',107,11),
    (1776,'Delilah Harrington','1995-01-13 11:14:07',165,18),
    (1777,'Imelda Cannon','2003-06-06 21:25:52',114,18),
    (1778,'Haviva Zimmerman','2003-07-26 14:09:00',3,13),
    (1779,'Barclay Donovan','2003-04-25 03:59:19',23,10),
    (1780,'Yolanda Cantrell','1996-07-31 10:28:24',49,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1781,'Jamal Tran','1999-05-25 13:11:41',24,11),
    (1782,'Graiden Mcclure','2002-04-21 03:42:51',165,11),
    (1783,'Amery Franklin','2002-10-18 10:48:19',144,16),
    (1784,'Hamilton Mooney','1997-12-28 08:44:16',63,16),
    (1785,'Candice Hobbs','2000-08-09 02:39:23',113,16),
    (1786,'Cassandra Chavez','2000-07-27 19:05:25',98,14),
    (1787,'Ciaran Armstrong','2000-06-23 16:42:29',165,19),
    (1788,'Addison Britt','1998-06-25 16:44:04',83,10),
    (1789,'Whitney Short','2001-12-06 20:55:19',119,16),
    (1790,'Joan Zimmerman','1998-12-04 04:18:52',122,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1791,'Garrison Lara','1996-07-02 22:07:53',123,12),
    (1792,'Gannon Huff','1995-05-13 20:50:48',24,12),
    (1793,'Aquila Oneil','2001-05-30 07:47:17',95,17),
    (1794,'Zephr Dawson','1997-01-13 20:36:20',11,14),
    (1795,'Leah Workman','2003-04-03 18:29:14',120,18),
    (1796,'Hyatt Conway','2002-10-23 23:30:10',78,19),
    (1797,'Caesar Alvarado','2000-10-18 19:54:26',142,10),
    (1798,'Karly Fernandez','2001-07-09 02:45:39',161,14),
    (1799,'Rebecca Henderson','2000-07-08 16:57:22',107,15),
    (1800,'Ivory Jensen','1998-11-19 14:07:40',102,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1801,'Timon Sosa','1998-12-20 22:16:16',162,16),
    (1802,'Ria Fulton','2001-02-25 15:21:09',174,13),
    (1803,'Jamal Parker','2004-09-12 03:48:55',179,13),
    (1804,'Ariel Dorsey','1995-07-15 09:47:08',12,12),
    (1805,'Karen David','1999-07-08 04:44:14',150,11),
    (1806,'Kylynn Patton','1996-04-24 17:04:25',149,17),
    (1807,'Randall Stark','1998-04-08 22:10:35',39,16),
    (1808,'Macey Goff','2000-10-16 20:20:20',110,17),
    (1809,'Aimee Evans','2001-05-19 19:13:25',64,17),
    (1810,'Maia Moon','1998-09-09 07:18:28',144,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1811,'Cathleen Love','2002-06-16 22:01:27',125,14),
    (1812,'Tashya Hayden','1997-09-20 14:13:46',157,10),
    (1813,'Delilah Hutchinson','1996-05-12 00:50:53',72,17),
    (1814,'Knox Mccall','2001-12-21 21:36:03',133,13),
    (1815,'Elliott Griffith','2001-07-22 04:13:52',81,11),
    (1816,'Gwendolyn Wilkins','2001-06-12 21:50:52',164,18),
    (1817,'Perry Aguilar','2004-10-11 23:08:05',166,12),
    (1818,'Micah Page','2003-01-12 02:16:04',97,12),
    (1819,'Alan Henderson','2002-02-17 01:10:17',125,16),
    (1820,'Regan Horn','1995-03-27 13:53:35',117,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1821,'Caldwell Macdonald','1996-11-27 17:05:11',115,18),
    (1822,'Jakeem Schroeder','2001-11-14 09:54:30',30,11),
    (1823,'Kaseem Cook','2002-04-21 02:44:43',104,17),
    (1824,'Moana Diaz','1997-04-29 18:32:16',126,12),
    (1825,'Melyssa Tillman','2001-03-18 17:36:06',77,10),
    (1826,'Rae Mooney','2004-11-08 04:42:38',141,14),
    (1827,'Hashim Daugherty','1997-12-04 20:12:03',138,17),
    (1828,'Hermione Flynn','1996-03-06 14:56:13',31,19),
    (1829,'Heather Parker','1995-05-31 23:30:29',118,15),
    (1830,'Shoshana Atkins','2000-01-12 22:53:54',156,20);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1831,'Elijah Nielsen','2001-12-11 00:42:52',72,19),
    (1832,'Dalton Petty','2004-06-13 14:36:49',29,13),
    (1833,'Abbot Schultz','2001-02-22 17:22:52',134,12),
    (1834,'Gannon Bartlett','1997-05-19 08:42:53',64,15),
    (1835,'Justine Bartlett','1996-11-04 14:53:03',71,20),
    (1836,'Raymond Avila','1997-03-18 18:16:27',110,15),
    (1837,'Fuller Nielsen','1999-08-31 12:34:18',49,11),
    (1838,'Lyle Henson','2001-08-16 07:47:23',60,20),
    (1839,'Todd Hartman','2002-12-07 22:36:11',100,12),
    (1840,'Keely Castro','1999-12-20 16:46:21',4,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1841,'Cruz Wilkerson','1997-11-24 04:37:00',86,14),
    (1842,'Erich Mclean','2003-06-08 21:14:45',80,13),
    (1843,'Aristotle Alvarado','1995-10-15 13:11:21',65,15),
    (1844,'Moses Glover','1995-02-07 22:47:37',54,16),
    (1845,'Joseph Maldonado','1997-09-10 03:11:19',36,15),
    (1846,'Brenden Wilcox','2001-09-28 15:58:00',9,17),
    (1847,'Kameko Baxter','1997-01-14 20:39:02',92,14),
    (1848,'Berk Rollins','1996-05-22 19:06:16',77,12),
    (1849,'Kamal Terry','1997-02-28 14:22:11',97,11),
    (1850,'Frances Guy','2002-08-16 02:09:33',142,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1851,'Emerson Maldonado','1999-04-03 12:01:35',28,17),
    (1852,'Evan Bentley','2003-04-10 15:39:39',84,11),
    (1853,'Wynne Booker','2002-05-09 12:29:19',171,15),
    (1854,'Hamilton Dunlap','1995-05-01 15:47:48',74,14),
    (1855,'Bell Miranda','1999-06-23 07:40:13',102,19),
    (1856,'Teagan Brooks','2000-08-13 17:20:53',62,10),
    (1857,'Brendan Gentry','2003-12-04 03:06:24',87,15),
    (1858,'Charde Mason','1996-03-02 11:57:58',47,11),
    (1859,'Stacy Alston','1998-10-15 16:24:48',107,15),
    (1860,'Craig Mcneil','1999-04-28 12:46:58',77,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1861,'Lyle Vargas','1997-01-09 23:26:32',152,20),
    (1862,'Jordan Sharpe','2001-01-21 20:58:22',123,15),
    (1863,'Levi Nelson','1998-01-29 04:36:33',4,15),
    (1864,'Palmer Dudley','2003-08-02 06:23:22',36,18),
    (1865,'Teegan Decker','1999-08-18 15:45:32',76,12),
    (1866,'Kirk Maddox','1998-07-09 11:33:49',70,13),
    (1867,'Michelle Britt','1996-10-25 03:13:55',4,17),
    (1868,'Jorden Reilly','1997-04-15 15:57:11',15,17),
    (1869,'Uriah Mckenzie','1995-03-28 11:18:30',68,16),
    (1870,'Scarlet Mcdaniel','1995-02-15 00:59:44',68,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1871,'Grace Galloway','1998-12-31 03:50:32',89,20),
    (1872,'Seth Jordan','1995-12-09 15:49:16',71,16),
    (1873,'Stewart Rowe','1998-11-12 19:02:39',62,20),
    (1874,'Hamilton Reeves','1995-10-06 03:42:27',32,17),
    (1875,'Winifred Benton','1996-03-11 22:59:32',149,12),
    (1876,'Lillith Hart','1997-11-21 04:45:53',136,12),
    (1877,'Callum Fuller','2003-05-28 11:42:31',147,14),
    (1878,'Kaseem Lang','1996-02-17 09:51:40',128,16),
    (1879,'Iris Franklin','1996-03-16 01:48:31',48,10),
    (1880,'Halla Hopper','2004-10-26 12:48:25',91,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1881,'Ethan O''Neill','1995-01-10 04:21:49',47,19),
    (1882,'Baxter Vincent','2001-08-03 10:32:15',167,19),
    (1883,'Geraldine Gay','1997-09-28 08:26:11',54,15),
    (1884,'Norman Mercado','2002-01-19 06:54:20',100,18),
    (1885,'Jordan Frye','2000-10-14 03:21:19',36,19),
    (1886,'Raja Waller','2003-10-23 13:46:59',61,10),
    (1887,'Colin Silva','2004-03-27 11:55:54',32,16),
    (1888,'Kato Mcneil','1998-11-25 01:47:30',104,11),
    (1889,'Christine Ray','1997-01-21 22:14:24',94,16),
    (1890,'Addison Daugherty','1998-11-10 04:55:06',170,13);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1891,'Kieran Garrison','2004-07-31 08:11:39',173,15),
    (1892,'Plato Avila','2000-09-25 10:39:25',79,19),
    (1893,'Jorden Montoya','1998-06-11 01:03:19',108,19),
    (1894,'Kenneth Dudley','1996-04-27 07:34:20',51,13),
    (1895,'Aquila Vance','1995-09-06 14:00:59',102,16),
    (1896,'Madison Booth','1997-10-05 07:04:33',144,13),
    (1897,'Marny Duncan','2000-08-26 00:27:58',134,16),
    (1898,'Cleo Ramirez','2000-12-18 03:04:02',48,17),
    (1899,'Thane Carpenter','1997-04-04 07:09:00',53,16),
    (1900,'Tasha Potter','2003-08-20 22:21:58',89,18);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1901,'Fredericka Pickett','1999-06-23 22:11:31',147,12),
    (1902,'Wylie Mcdonald','2004-08-12 08:28:02',125,10),
    (1903,'Mallory Mccullough','2000-07-27 01:35:47',161,12),
    (1904,'Ali Kelley','2001-08-15 03:57:14',121,18),
    (1905,'Kim Guerrero','2004-12-31 11:52:20',19,15),
    (1906,'Meghan Harrell','2000-10-31 10:17:41',138,13),
    (1907,'Lesley Sanders','2000-05-29 01:22:27',15,13),
    (1908,'Erasmus Espinoza','1995-07-01 08:53:57',62,16),
    (1909,'Gillian Cunningham','2001-07-17 01:01:15',73,16),
    (1910,'Ila Collier','1996-10-09 05:21:09',76,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1911,'Jacob Goodman','2000-11-09 05:51:03',150,15),
    (1912,'Maxine Moore','2003-02-28 18:46:18',9,12),
    (1913,'Raymond Sampson','2002-07-20 17:46:36',124,19),
    (1914,'Katelyn Byers','2001-06-28 21:47:29',158,17),
    (1915,'Gisela Campbell','2002-11-29 20:32:52',46,11),
    (1916,'Amaya Foster','2002-11-12 12:11:34',88,12),
    (1917,'Ursa Decker','2004-04-05 02:41:18',126,17),
    (1918,'Maris Pratt','1996-03-18 10:22:53',177,19),
    (1919,'Ryder Spears','2004-04-27 15:17:21',150,11),
    (1920,'Whoopi Colon','2004-04-28 01:51:10',146,10);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1921,'Daria Knapp','2003-06-15 12:56:29',45,17),
    (1922,'Daphne Logan','1995-10-18 15:27:50',122,16),
    (1923,'Morgan Harding','2001-04-05 13:34:32',93,14),
    (1924,'Adele Travis','1999-12-08 06:32:07',173,13),
    (1925,'Xander Whitney','2001-03-31 22:32:50',29,15),
    (1926,'Nora Tran','1995-04-25 16:18:04',172,12),
    (1927,'Jescie Dale','1997-01-05 13:24:53',9,20),
    (1928,'Eliana Kline','1996-08-12 23:48:35',167,10),
    (1929,'Kane Beach','1995-06-12 22:46:04',138,14),
    (1930,'Keith Mcdaniel','2001-05-25 01:29:45',152,19);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1931,'Maris Tillman','1997-06-28 05:05:10',12,14),
    (1932,'Nelle Sargent','2002-10-10 21:25:54',128,10),
    (1933,'Denton Rogers','1997-08-24 17:36:58',134,16),
    (1934,'Whoopi Harper','1996-04-23 12:36:52',20,11),
    (1935,'Keely Whitehead','1999-08-06 14:03:50',16,12),
    (1936,'Lamar Harvey','2001-08-29 13:40:04',43,18),
    (1937,'Macon Peck','2003-11-08 01:06:47',172,16),
    (1938,'Cairo Parks','2003-08-04 10:18:12',126,15),
    (1939,'Porter Franklin','2004-01-22 15:22:06',156,18),
    (1940,'Fleur Roth','2003-08-28 04:42:55',89,17);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1941,'Jackson Henderson','2003-01-30 21:18:20',86,19),
    (1942,'Lawrence Joyce','1996-10-13 00:54:00',126,14),
    (1943,'Tamara Pennington','2001-09-08 01:50:49',165,14),
    (1944,'Lee Carter','2002-07-04 11:44:59',135,14),
    (1945,'Inga Robertson','1995-03-10 23:44:32',42,15),
    (1946,'Isabelle Watkins','2001-03-12 23:22:10',98,16),
    (1947,'Stephen Wilkins','2002-12-16 15:49:02',151,14),
    (1948,'Evelyn Hatfield','2000-03-13 16:08:33',32,18),
    (1949,'Libby Fry','1999-01-19 10:00:02',33,14),
    (1950,'Rylee Kerr','1996-12-13 20:28:30',3,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1951,'Blaze Norman','1997-06-05 12:18:21',107,12),
    (1952,'Sheila Savage','1998-01-06 08:57:03',28,15),
    (1953,'Melinda Nicholson','2000-04-20 09:56:18',97,12),
    (1954,'Dorian Hooper','1998-02-04 14:11:57',124,15),
    (1955,'Judah Garza','1998-06-06 17:23:55',5,16),
    (1956,'Stone Walker','1996-10-24 04:13:35',66,16),
    (1957,'Thaddeus Black','1997-01-24 13:24:27',118,17),
    (1958,'Rina Joyce','1995-11-17 22:08:26',37,16),
    (1959,'Marah Harrington','1998-06-22 20:10:57',17,13),
    (1960,'Claudia Beasley','2001-07-29 20:21:47',110,12);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1961,'Piper Forbes','2002-07-14 23:24:48',66,11),
    (1962,'Walter Beard','2000-12-06 06:37:58',157,11),
    (1963,'Ann Macdonald','2004-01-14 09:37:01',104,18),
    (1964,'Lionel Scott','2000-09-19 09:18:19',142,13),
    (1965,'Noah Sheppard','2004-12-31 11:07:38',40,10),
    (1966,'Rylee Fuller','1999-06-29 11:07:27',57,11),
    (1967,'Shad Lang','1996-02-04 07:56:04',162,19),
    (1968,'Victoria Sellers','1999-03-02 06:10:52',118,16),
    (1969,'Sonia Tyler','1997-06-05 08:40:45',40,20),
    (1970,'Rashad Brown','1998-07-21 07:27:08',162,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1971,'Ethan Harrell','2003-03-03 18:07:04',92,19),
    (1972,'Paul Ramirez','2002-12-11 06:36:34',112,17),
    (1973,'Dieter Sweet','2000-02-27 21:14:39',20,12),
    (1974,'Chandler Paul','1995-09-15 13:34:12',18,14),
    (1975,'Cedric Vance','2000-12-04 16:05:17',19,19),
    (1976,'Duncan Gill','1997-10-25 21:21:35',43,20),
    (1977,'Ima Bauer','2001-10-02 13:36:44',134,11),
    (1978,'Bert Holcomb','2001-04-07 21:59:09',69,12),
    (1979,'Walker Tanner','1998-09-02 05:04:05',142,16),
    (1980,'Macaulay Garrison','2003-12-11 14:53:43',79,15);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1981,'Griffin Mueller','1995-04-08 01:58:43',131,20),
    (1982,'Colorado Key','1996-02-28 04:06:10',32,12),
    (1983,'Aretha Macdonald','2004-06-19 06:29:18',86,17),
    (1984,'Amela Grimes','2002-12-26 16:38:40',126,11),
    (1985,'Candace Adams','2004-02-17 00:11:09',46,10),
    (1986,'Baxter Stevenson','2001-08-09 01:37:08',10,16),
    (1987,'Ariel Mcleod','1995-03-04 20:27:26',17,17),
    (1988,'Bevis Hendrix','1999-10-05 22:13:24',20,16),
    (1989,'Kyle Dejesus','1999-07-25 08:19:59',179,19),
    (1990,'Imani Beasley','2004-09-22 10:10:17',12,11);
INSERT INTO students (id,name,birth_date,completed_credits,average_grade)
VALUES
    (1991,'Kaye Whitehead','1998-12-16 18:59:33',50,19),
    (1992,'Haviva Valencia','2001-07-24 03:41:57',161,12),
    (1993,'Alexander Norton','1997-08-08 09:31:13',170,11),
    (1994,'Christian Nichols','1997-06-15 19:38:20',97,16),
    (1995,'Asher Walker','2003-07-07 14:54:43',119,16),
    (1996,'Sara Flores','1996-02-29 04:47:33',26,13),
    (1997,'Ferdinand Mejia','1999-03-05 19:14:59',79,14),
    (1998,'Jordan Fernandez','1999-06-12 18:56:34',139,13),
    (1999,'Adena Macdonald','2004-01-19 20:02:21',76,14),
    (2000,'Axel Holden','2002-09-24 17:58:38',155,19);



INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1,1,1),
    (2,1,2),
    (3,1,3),
    (4,1,4),
    (5,1,5),
    (6,1,6),
    (7,1,7),
    (8,1,8),
    (9,1,9),
    (10,1,10);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (11,1,11),
    (12,1,12),
    (13,1,13),
    (14,1,14),
    (15,1,15),
    (16,1,16),
    (17,1,17),
    (18,1,18),
    (19,1,19),
    (20,1,20);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (21,1,21),
    (22,1,22),
    (23,1,23),
    (24,1,24),
    (25,1,25),
    (26,1,26),
    (27,1,27),
    (28,1,28),
    (29,1,29),
    (30,1,30);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (31,1,31),
    (32,1,32),
    (33,1,33),
    (34,1,34),
    (35,1,35),
    (36,1,36),
    (37,1,37),
    (38,1,38),
    (39,1,39),
    (40,1,40);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (41,1,41),
    (42,1,42),
    (43,1,43),
    (44,1,44),
    (45,1,45),
    (46,1,46),
    (47,1,47),
    (48,1,48),
    (49,1,49),
    (50,1,50);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (51,2,51),
    (52,2,52),
    (53,2,53),
    (54,2,54),
    (55,2,55),
    (56,2,56),
    (57,2,57),
    (58,2,58),
    (59,2,59),
    (60,2,60);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (61,2,61),
    (62,2,62),
    (63,2,63),
    (64,2,64),
    (65,2,65),
    (66,2,66),
    (67,2,67),
    (68,2,68),
    (69,2,69),
    (70,2,70);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (71,2,71),
    (72,2,72),
    (73,2,73),
    (74,2,74),
    (75,2,75),
    (76,2,76),
    (77,2,77),
    (78,2,78),
    (79,2,79),
    (80,2,80);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (81,2,81),
    (82,2,82),
    (83,2,83),
    (84,2,84),
    (85,2,85),
    (86,2,86),
    (87,2,87),
    (88,2,88),
    (89,2,89),
    (90,2,90);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (91,2,91),
    (92,2,92),
    (93,2,93),
    (94,2,94),
    (95,2,95),
    (96,2,96),
    (97,2,97),
    (98,2,98),
    (99,2,99),
    (100,2,100);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (101,3,101),
    (102,3,102),
    (103,3,103),
    (104,3,104),
    (105,3,105),
    (106,3,106),
    (107,3,107),
    (108,3,108),
    (109,3,109),
    (110,3,110);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (111,3,111),
    (112,3,112),
    (113,3,113),
    (114,3,114),
    (115,3,115),
    (116,3,116),
    (117,3,117),
    (118,3,118),
    (119,3,119),
    (120,3,120);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (121,3,121),
    (122,3,122),
    (123,3,123),
    (124,3,124),
    (125,3,125),
    (126,3,126),
    (127,3,127),
    (128,3,128),
    (129,3,129),
    (130,3,130);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (131,3,131),
    (132,3,132),
    (133,3,133),
    (134,3,134),
    (135,3,135),
    (136,3,136),
    (137,3,137),
    (138,3,138),
    (139,3,139),
    (140,3,140);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (141,3,141),
    (142,3,142),
    (143,3,143),
    (144,3,144),
    (145,3,145),
    (146,3,146),
    (147,3,147),
    (148,3,148),
    (149,3,149),
    (150,3,150);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (151,4,151),
    (152,4,152),
    (153,4,153),
    (154,4,154),
    (155,4,155),
    (156,4,156),
    (157,4,157),
    (158,4,158),
    (159,4,159),
    (160,4,160);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (161,4,161),
    (162,4,162),
    (163,4,163),
    (164,4,164),
    (165,4,165),
    (166,4,166),
    (167,4,167),
    (168,4,168),
    (169,4,169),
    (170,4,170);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (171,4,171),
    (172,4,172),
    (173,4,173),
    (174,4,174),
    (175,4,175),
    (176,4,176),
    (177,4,177),
    (178,4,178),
    (179,4,179),
    (180,4,180);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (181,4,181),
    (182,4,182),
    (183,4,183),
    (184,4,184),
    (185,4,185),
    (186,4,186),
    (187,4,187),
    (188,4,188),
    (189,4,189),
    (190,4,190);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (191,4,191),
    (192,4,192),
    (193,4,193),
    (194,4,194),
    (195,4,195),
    (196,4,196),
    (197,4,197),
    (198,4,198),
    (199,4,199),
    (200,4,200);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (201,4,201),
    (202,4,202),
    (203,4,203),
    (204,4,204),
    (205,4,205),
    (206,4,206),
    (207,4,207),
    (208,4,208),
    (209,4,209),
    (210,4,210);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (211,5,211),
    (212,5,212),
    (213,5,213),
    (214,5,214),
    (215,5,215),
    (216,5,216),
    (217,5,217),
    (218,5,218),
    (219,5,219),
    (220,5,220);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (221,5,221),
    (222,5,222),
    (223,5,223),
    (224,5,224),
    (225,5,225),
    (226,5,226),
    (227,5,227),
    (228,5,228),
    (229,5,229),
    (230,5,230);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (231,5,231),
    (232,5,232),
    (233,5,233),
    (234,5,234),
    (235,5,235),
    (236,5,236),
    (237,5,237),
    (238,5,238),
    (239,5,239),
    (240,5,240);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (241,5,241),
    (242,5,242),
    (243,5,243),
    (244,5,244),
    (245,5,245),
    (246,5,246),
    (247,5,247),
    (248,5,248),
    (249,5,249),
    (250,5,250);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (251,6,251),
    (252,6,252),
    (253,6,253),
    (254,6,254),
    (255,6,255),
    (256,6,256),
    (257,6,257),
    (258,6,258),
    (259,6,259),
    (260,6,260);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (261,6,261),
    (262,6,262),
    (263,6,263),
    (264,6,264),
    (265,6,265),
    (266,6,266),
    (267,6,267),
    (268,6,268),
    (269,6,269),
    (270,6,270);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (271,6,271),
    (272,6,272),
    (273,6,273),
    (274,6,274),
    (275,6,275),
    (276,6,276),
    (277,6,277),
    (278,6,278),
    (279,6,279),
    (280,6,280);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (281,6,281),
    (282,6,282),
    (283,6,283),
    (284,6,284),
    (285,6,285),
    (286,6,286),
    (287,6,287),
    (288,6,288),
    (289,6,289),
    (290,6,290);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (291,6,291),
    (292,6,292),
    (293,6,293),
    (294,6,294),
    (295,6,295),
    (296,6,296),
    (297,6,297),
    (298,6,298),
    (299,6,299),
    (300,6,300);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (301,7,301),
    (302,7,302),
    (303,7,303),
    (304,7,304),
    (305,7,305),
    (306,7,306),
    (307,7,307),
    (308,7,308),
    (309,7,309),
    (310,7,310);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (311,7,311),
    (312,7,312),
    (313,7,313),
    (314,7,314),
    (315,7,315),
    (316,7,316),
    (317,7,317),
    (318,7,318),
    (319,7,319),
    (320,7,320);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (321,8,321),
    (322,8,322),
    (323,8,323),
    (324,8,324),
    (325,8,325),
    (326,8,326),
    (327,8,327),
    (328,8,328),
    (329,8,329),
    (330,8,330);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (331,8,331),
    (332,8,332),
    (333,8,333),
    (334,8,334),
    (335,8,335),
    (336,8,336),
    (337,8,337),
    (338,8,338),
    (339,8,339),
    (340,8,340);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (341,8,341),
    (342,8,342),
    (343,8,343),
    (344,8,344),
    (345,8,345),
    (346,8,346),
    (347,8,347),
    (348,8,348),
    (349,8,349),
    (350,8,350);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (351,8,351),
    (352,8,352),
    (353,8,353),
    (354,8,354),
    (355,8,355),
    (356,8,356),
    (357,8,357),
    (358,8,358),
    (359,8,359),
    (360,8,360);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (361,8,361),
    (362,8,362),
    (363,8,363),
    (364,8,364),
    (365,8,365),
    (366,8,366),
    (367,8,367),
    (368,8,368),
    (369,8,369),
    (370,8,370);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (371,8,371),
    (372,8,372),
    (373,8,373),
    (374,8,374),
    (375,8,375),
    (376,8,376),
    (377,8,377),
    (378,8,378),
    (379,8,379),
    (380,8,380);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (381,8,381),
    (382,8,382),
    (383,8,383),
    (384,8,384),
    (385,8,385),
    (386,8,386),
    (387,8,387),
    (388,8,388),
    (389,8,389),
    (390,8,390);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (391,8,391),
    (392,8,392),
    (393,8,393),
    (394,8,394),
    (395,8,395),
    (396,8,396),
    (397,8,397),
    (398,8,398),
    (399,8,399),
    (400,8,400);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (401,9,401),
    (402,9,402),
    (403,9,403),
    (404,9,404),
    (405,9,405),
    (406,9,406),
    (407,9,407),
    (408,9,408),
    (409,9,409),
    (410,9,410);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (411,9,411),
    (412,9,412),
    (413,9,413),
    (414,9,414),
    (415,9,415),
    (416,9,416),
    (417,9,417),
    (418,9,418),
    (419,9,419),
    (420,9,420);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (421,9,421),
    (422,9,422),
    (423,9,423),
    (424,9,424),
    (425,9,425),
    (426,9,426),
    (427,9,427),
    (428,9,428),
    (429,9,429),
    (430,9,430);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (431,9,431),
    (432,9,432),
    (433,9,433),
    (434,9,434),
    (435,9,435),
    (436,9,436),
    (437,9,437),
    (438,9,438),
    (439,9,439),
    (440,9,440);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (441,9,441),
    (442,9,442),
    (443,9,443),
    (444,9,444),
    (445,9,445),
    (446,9,446),
    (447,9,447),
    (448,9,448),
    (449,9,449),
    (450,9,450);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (451,9,451),
    (452,9,452),
    (453,9,453),
    (454,9,454),
    (455,9,455),
    (456,9,456),
    (457,9,457),
    (458,9,458),
    (459,9,459),
    (460,9,460);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (461,9,461),
    (462,9,462),
    (463,9,463),
    (464,9,464),
    (465,9,465),
    (466,9,466),
    (467,9,467),
    (468,9,468),
    (469,9,469),
    (470,9,470);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (471,9,471);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (472,10,472),
    (473,10,473),
    (474,10,474),
    (475,10,475),
    (476,10,476),
    (477,10,477),
    (478,10,478),
    (479,10,479),
    (480,10,480),
    (481,10,481);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (482,10,482),
    (483,10,483),
    (484,10,484),
    (485,10,485),
    (486,10,486),
    (487,10,487),
    (488,10,488),
    (489,10,489),
    (490,10,490),
    (491,10,491);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (492,10,492),
    (493,10,493),
    (494,10,494),
    (495,10,495),
    (496,10,496),
    (497,10,497),
    (498,10,498),
    (499,10,499),
    (500,10,500);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (501,11,501),
    (502,11,502),
    (503,11,503),
    (504,11,504),
    (505,11,505),
    (506,11,506),
    (507,11,507),
    (508,11,508),
    (509,11,509),
    (510,11,510);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (511,11,511),
    (512,11,512),
    (513,11,513),
    (514,11,514),
    (515,11,515),
    (516,11,516),
    (517,11,517),
    (518,11,518),
    (519,11,519),
    (520,11,520);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (521,11,521),
    (522,11,522),
    (523,11,523),
    (524,11,524),
    (525,11,525),
    (526,11,526),
    (527,11,527),
    (528,11,528),
    (529,11,529),
    (530,11,530);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (531,11,531),
    (532,11,532),
    (533,11,533),
    (534,11,534),
    (535,11,535),
    (536,11,536),
    (537,11,537),
    (538,11,538),
    (539,11,539),
    (540,11,540);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (541,11,541),
    (542,11,542),
    (543,11,543),
    (544,11,544),
    (545,11,545),
    (546,11,546),
    (547,11,547),
    (548,11,548),
    (549,11,549),
    (550,11,550);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (551,12,551),
    (552,12,552),
    (553,12,553),
    (554,12,554),
    (555,12,555),
    (556,12,556),
    (557,12,557),
    (558,12,558),
    (559,12,559),
    (560,12,560);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (561,12,561),
    (562,12,562),
    (563,12,563),
    (564,12,564),
    (565,12,565),
    (566,12,566),
    (567,12,567),
    (568,12,568),
    (569,12,569),
    (570,12,570);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (571,12,571),
    (572,12,572),
    (573,12,573),
    (574,12,574),
    (575,12,575),
    (576,12,576),
    (577,12,577),
    (578,12,578),
    (579,12,579),
    (580,12,580);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (581,12,581),
    (582,12,582),
    (583,12,583),
    (584,12,584),
    (585,12,585),
    (586,12,586),
    (587,12,587),
    (588,12,588),
    (589,12,589),
    (590,12,590);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (591,12,591),
    (592,12,592),
    (593,12,593),
    (594,12,594),
    (595,12,595),
    (596,12,596),
    (597,12,597),
    (598,12,598),
    (599,12,599),
    (600,12,600);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (601,12,601),
    (602,12,602),
    (603,12,603),
    (604,12,604),
    (605,12,605),
    (606,12,606),
    (607,12,607),
    (608,12,608),
    (609,12,609),
    (610,12,610);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (611,12,611);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (612,13,612),
    (613,13,613),
    (614,13,614),
    (615,13,615),
    (616,13,616),
    (617,13,617),
    (618,13,618),
    (619,13,619),
    (620,13,620),
    (621,13,621);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (622,13,622),
    (623,13,623),
    (624,13,624),
    (625,13,625),
    (626,13,626),
    (627,13,627),
    (628,13,628),
    (629,13,629),
    (630,13,630),
    (631,13,631);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (632,13,632),
    (633,13,633),
    (634,13,634),
    (635,13,635),
    (636,13,636),
    (637,13,637),
    (638,13,638),
    (639,13,639),
    (640,13,640),
    (641,13,641);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (642,13,642),
    (643,13,643),
    (644,13,644),
    (645,13,645),
    (646,13,646),
    (647,13,647),
    (648,13,648),
    (649,13,649),
    (650,13,650);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (651,14,651),
    (652,14,652),
    (653,14,653),
    (654,14,654),
    (655,14,655),
    (656,14,656),
    (657,14,657),
    (658,14,658),
    (659,14,659),
    (660,14,660);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (661,14,661),
    (662,14,662),
    (663,14,663),
    (664,14,664),
    (665,14,665),
    (666,14,666),
    (667,14,667),
    (668,14,668),
    (669,14,669),
    (670,14,670);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (671,14,671),
    (672,14,672),
    (673,14,673),
    (674,14,674),
    (675,14,675),
    (676,14,676),
    (677,14,677),
    (678,14,678),
    (679,14,679),
    (680,14,680);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (681,14,681),
    (682,14,682),
    (683,14,683),
    (684,14,684),
    (685,14,685),
    (686,14,686),
    (687,14,687),
    (688,14,688),
    (689,14,689),
    (690,14,690);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (691,14,691),
    (692,14,692),
    (693,14,693),
    (694,14,694),
    (695,14,695),
    (696,14,696),
    (697,14,697),
    (698,14,698),
    (699,14,699),
    (700,14,700);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (701,15,701),
    (702,15,702),
    (703,15,703),
    (704,15,704),
    (705,15,705),
    (706,15,706),
    (707,15,707),
    (708,15,708),
    (709,15,709),
    (710,15,710);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (711,15,711),
    (712,15,712),
    (713,15,713),
    (714,15,714),
    (715,15,715),
    (716,15,716),
    (717,15,717),
    (718,15,718),
    (719,15,719),
    (720,15,720);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (721,15,721),
    (722,15,722),
    (723,15,723),
    (724,15,724),
    (725,15,725),
    (726,15,726),
    (727,15,727),
    (728,15,728),
    (729,15,729),
    (730,15,730);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (731,15,731),
    (732,15,732),
    (733,15,733),
    (734,15,734),
    (735,15,735),
    (736,15,736),
    (737,15,737);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (738,16,738),
    (739,16,739),
    (740,16,740),
    (741,16,741),
    (742,16,742),
    (743,16,743),
    (744,16,744),
    (745,16,745),
    (746,16,746),
    (747,16,747);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (748,16,748),
    (749,16,749),
    (750,16,750),
    (751,16,751),
    (752,16,752),
    (753,16,753),
    (754,16,754),
    (755,16,755),
    (756,16,756),
    (757,16,757);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (758,16,758),
    (759,16,759),
    (760,16,760),
    (761,16,761),
    (762,16,762),
    (763,16,763),
    (764,16,764),
    (765,16,765),
    (766,16,766),
    (767,16,767);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (768,16,768),
    (769,16,769),
    (770,16,770),
    (771,16,771),
    (772,16,772),
    (773,16,773),
    (774,16,774),
    (775,16,775),
    (776,16,776),
    (777,16,777);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (778,16,778),
    (779,16,779),
    (780,16,780),
    (781,16,781),
    (782,16,782),
    (783,16,783),
    (784,16,784),
    (785,16,785),
    (786,16,786),
    (787,16,787);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (788,16,788),
    (789,16,789),
    (790,16,790),
    (791,16,791),
    (792,16,792),
    (793,16,793),
    (794,16,794),
    (795,16,795),
    (796,16,796),
    (797,16,797);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (798,16,798),
    (799,16,799),
    (800,16,800);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (801,17,801),
    (802,17,802),
    (803,17,803),
    (804,17,804),
    (805,17,805),
    (806,17,806),
    (807,17,807),
    (808,17,808),
    (809,17,809),
    (810,17,810);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (811,17,811),
    (812,17,812),
    (813,17,813),
    (814,17,814),
    (815,17,815),
    (816,17,816),
    (817,17,817),
    (818,17,818),
    (819,17,819),
    (820,17,820);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (821,17,821),
    (822,17,822),
    (823,17,823),
    (824,17,824),
    (825,17,825),
    (826,17,826),
    (827,17,827),
    (828,17,828),
    (829,17,829),
    (830,17,830);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (831,17,831),
    (832,17,832),
    (833,17,833),
    (834,17,834),
    (835,17,835),
    (836,17,836),
    (837,17,837),
    (838,17,838),
    (839,17,839),
    (840,17,840);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (841,17,841),
    (842,17,842),
    (843,17,843),
    (844,17,844),
    (845,17,845),
    (846,17,846),
    (847,17,847),
    (848,17,848),
    (849,17,849),
    (850,17,850);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (851,18,851),
    (852,18,852),
    (853,18,853),
    (854,18,854),
    (855,18,855),
    (856,18,856),
    (857,18,857),
    (858,18,858),
    (859,18,859),
    (860,18,860);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (861,18,861),
    (862,18,862),
    (863,18,863),
    (864,18,864),
    (865,18,865),
    (866,18,866),
    (867,18,867),
    (868,18,868),
    (869,18,869),
    (870,18,870);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (871,18,871),
    (872,18,872),
    (873,18,873),
    (874,18,874),
    (875,18,875),
    (876,18,876),
    (877,18,877),
    (878,18,878),
    (879,18,879),
    (880,18,880);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (881,18,881),
    (882,18,882),
    (883,18,883),
    (884,18,884),
    (885,18,885),
    (886,18,886),
    (887,18,887),
    (888,18,888),
    (889,18,889),
    (890,18,890);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (891,18,891),
    (892,18,892),
    (893,18,893),
    (894,18,894),
    (895,18,895),
    (896,18,896),
    (897,18,897),
    (898,18,898),
    (899,18,899),
    (900,18,900);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (901,18,901),
    (902,18,902),
    (903,18,903),
    (904,18,904),
    (905,18,905),
    (906,18,906),
    (907,18,907),
    (908,18,908),
    (909,18,909),
    (910,18,910);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (911,18,911),
    (912,18,912),
    (913,18,913),
    (914,18,914),
    (915,18,915),
    (916,18,916),
    (917,18,917),
    (918,18,918),
    (919,18,919),
    (920,18,920);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (921,18,921),
    (922,18,922),
    (923,18,923),
    (924,18,924),
    (925,18,925),
    (926,18,926),
    (927,18,927),
    (928,18,928),
    (929,18,929),
    (930,18,930);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (931,18,931),
    (932,18,932),
    (933,18,933),
    (934,18,934),
    (935,18,935),
    (936,18,936),
    (937,18,937),
    (938,18,938),
    (939,18,939),
    (940,18,940);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (941,19,941),
    (942,19,942),
    (943,19,943),
    (944,19,944),
    (945,19,945),
    (946,19,946),
    (947,19,947),
    (948,19,948),
    (949,19,949),
    (950,19,950);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (951,20,951),
    (952,20,952),
    (953,20,953),
    (954,20,954),
    (955,20,955),
    (956,20,956),
    (957,20,957),
    (958,20,958),
    (959,20,959),
    (960,20,960);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (961,20,961),
    (962,20,962),
    (963,20,963),
    (964,20,964),
    (965,20,965),
    (966,20,966),
    (967,20,967),
    (968,20,968),
    (969,20,969),
    (970,20,970);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (971,20,971),
    (972,20,972),
    (973,20,973),
    (974,20,974),
    (975,20,975),
    (976,20,976),
    (977,20,977),
    (978,20,978),
    (979,20,979),
    (980,20,980);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (981,20,981),
    (982,20,982),
    (983,20,983),
    (984,20,984),
    (985,20,985),
    (986,20,986),
    (987,20,987),
    (988,20,988),
    (989,20,989),
    (990,20,990);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (991,20,991),
    (992,20,992),
    (993,20,993),
    (994,20,994),
    (995,20,995),
    (996,20,996),
    (997,20,997),
    (998,20,998),
    (999,20,999),
    (1000,20,1000);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1001,21,1001),
    (1002,21,1002),
    (1003,21,1003),
    (1004,21,1004),
    (1005,21,1005),
    (1006,21,1006),
    (1007,21,1007),
    (1008,21,1008),
    (1009,21,1009),
    (1010,21,1010);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1011,21,1011),
    (1012,21,1012),
    (1013,21,1013),
    (1014,21,1014),
    (1015,21,1015),
    (1016,21,1016),
    (1017,21,1017),
    (1018,21,1018),
    (1019,21,1019),
    (1020,21,1020);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1021,21,1021),
    (1022,21,1022),
    (1023,21,1023),
    (1024,21,1024),
    (1025,21,1025),
    (1026,21,1026),
    (1027,21,1027),
    (1028,21,1028),
    (1029,21,1029),
    (1030,21,1030);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1031,21,1031),
    (1032,21,1032),
    (1033,21,1033),
    (1034,21,1034),
    (1035,21,1035),
    (1036,21,1036),
    (1037,21,1037),
    (1038,21,1038),
    (1039,21,1039),
    (1040,21,1040);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1041,21,1041),
    (1042,21,1042),
    (1043,21,1043),
    (1044,21,1044),
    (1045,21,1045),
    (1046,21,1046),
    (1047,21,1047),
    (1048,21,1048),
    (1049,21,1049),
    (1050,21,1050);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1051,21,1051),
    (1052,21,1052),
    (1053,21,1053),
    (1054,21,1054),
    (1055,21,1055),
    (1056,21,1056),
    (1057,21,1057),
    (1058,21,1058),
    (1059,21,1059),
    (1060,21,1060);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1061,21,1061),
    (1062,21,1062),
    (1063,21,1063),
    (1064,21,1064),
    (1065,21,1065),
    (1066,21,1066),
    (1067,21,1067),
    (1068,21,1068),
    (1069,21,1069),
    (1070,21,1070);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1071,21,1071),
    (1072,21,1072),
    (1073,21,1073),
    (1074,21,1074),
    (1075,21,1075),
    (1076,21,1076),
    (1077,21,1077),
    (1078,21,1078),
    (1079,21,1079),
    (1080,21,1080);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1081,21,1081),
    (1082,21,1082),
    (1083,21,1083),
    (1084,21,1084),
    (1085,21,1085),
    (1086,21,1086),
    (1087,21,1087),
    (1088,21,1088),
    (1089,21,1089),
    (1090,21,1090);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1091,21,1091),
    (1092,21,1092),
    (1093,21,1093),
    (1094,21,1094),
    (1095,21,1095);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1096,22,1096),
    (1097,22,1097),
    (1098,22,1098),
    (1099,22,1099),
    (1100,22,1100);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1101,23,1101),
    (1102,23,1102),
    (1103,23,1103),
    (1104,23,1104),
    (1105,23,1105),
    (1106,23,1106),
    (1107,23,1107),
    (1108,23,1108),
    (1109,23,1109),
    (1110,23,1110);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1111,23,1111),
    (1112,23,1112),
    (1113,23,1113),
    (1114,23,1114),
    (1115,23,1115),
    (1116,23,1116),
    (1117,23,1117),
    (1118,23,1118),
    (1119,23,1119),
    (1120,23,1120);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1121,23,1121),
    (1122,23,1122),
    (1123,23,1123),
    (1124,23,1124),
    (1125,23,1125),
    (1126,23,1126),
    (1127,23,1127),
    (1128,23,1128),
    (1129,23,1129),
    (1130,23,1130);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1131,23,1131),
    (1132,23,1132),
    (1133,23,1133),
    (1134,23,1134),
    (1135,23,1135),
    (1136,23,1136),
    (1137,23,1137),
    (1138,23,1138),
    (1139,23,1139),
    (1140,23,1140);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1141,23,1141),
    (1142,23,1142),
    (1143,23,1143),
    (1144,23,1144),
    (1145,23,1145),
    (1146,23,1146),
    (1147,23,1147),
    (1148,23,1148),
    (1149,23,1149),
    (1150,23,1150);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1151,24,1151),
    (1152,24,1152),
    (1153,24,1153),
    (1154,24,1154),
    (1155,24,1155),
    (1156,24,1156),
    (1157,24,1157),
    (1158,24,1158),
    (1159,24,1159),
    (1160,24,1160);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1161,24,1161),
    (1162,24,1162),
    (1163,24,1163),
    (1164,24,1164),
    (1165,24,1165),
    (1166,24,1166),
    (1167,24,1167),
    (1168,24,1168),
    (1169,24,1169),
    (1170,24,1170);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1171,24,1171),
    (1172,24,1172),
    (1173,24,1173),
    (1174,24,1174),
    (1175,24,1175),
    (1176,24,1176),
    (1177,24,1177),
    (1178,24,1178),
    (1179,24,1179),
    (1180,24,1180);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1181,24,1181),
    (1182,24,1182),
    (1183,24,1183),
    (1184,24,1184),
    (1185,24,1185),
    (1186,24,1186),
    (1187,24,1187),
    (1188,24,1188),
    (1189,24,1189),
    (1190,24,1190);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1191,24,1191),
    (1192,24,1192),
    (1193,24,1193),
    (1194,24,1194),
    (1195,24,1195),
    (1196,24,1196),
    (1197,24,1197),
    (1198,24,1198),
    (1199,24,1199),
    (1200,24,1200);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1201,25,1201),
    (1202,25,1202),
    (1203,25,1203),
    (1204,25,1204),
    (1205,25,1205),
    (1206,25,1206),
    (1207,25,1207),
    (1208,25,1208),
    (1209,25,1209),
    (1210,25,1210);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1211,25,1211),
    (1212,25,1212),
    (1213,25,1213),
    (1214,25,1214),
    (1215,25,1215),
    (1216,25,1216),
    (1217,25,1217),
    (1218,25,1218),
    (1219,25,1219),
    (1220,25,1220);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1221,25,1221),
    (1222,25,1222),
    (1223,25,1223),
    (1224,25,1224),
    (1225,25,1225),
    (1226,25,1226),
    (1227,25,1227),
    (1228,25,1228),
    (1229,25,1229),
    (1230,25,1230);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1231,25,1231),
    (1232,25,1232),
    (1233,25,1233),
    (1234,25,1234),
    (1235,25,1235),
    (1236,25,1236),
    (1237,25,1237),
    (1238,25,1238),
    (1239,25,1239),
    (1240,25,1240);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1241,25,1241),
    (1242,25,1242),
    (1243,25,1243),
    (1244,25,1244),
    (1245,25,1245),
    (1246,25,1246),
    (1247,25,1247),
    (1248,25,1248),
    (1249,25,1249),
    (1250,25,1250);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1251,26,1251),
    (1252,26,1252),
    (1253,26,1253),
    (1254,26,1254),
    (1255,26,1255),
    (1256,26,1256),
    (1257,26,1257),
    (1258,26,1258),
    (1259,26,1259),
    (1260,26,1260);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1261,26,1261),
    (1262,26,1262),
    (1263,26,1263),
    (1264,26,1264),
    (1265,26,1265),
    (1266,26,1266),
    (1267,26,1267),
    (1268,26,1268),
    (1269,26,1269),
    (1270,26,1270);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1271,26,1271),
    (1272,26,1272),
    (1273,26,1273),
    (1274,26,1274),
    (1275,26,1275),
    (1276,26,1276),
    (1277,26,1277),
    (1278,26,1278),
    (1279,26,1279),
    (1280,26,1280);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1281,26,1281),
    (1282,26,1282),
    (1283,26,1283),
    (1284,26,1284),
    (1285,26,1285),
    (1286,26,1286),
    (1287,26,1287),
    (1288,26,1288),
    (1289,26,1289),
    (1290,26,1290);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1291,26,1291),
    (1292,26,1292),
    (1293,26,1293),
    (1294,26,1294),
    (1295,26,1295),
    (1296,26,1296),
    (1297,26,1297),
    (1298,26,1298),
    (1299,26,1299),
    (1300,26,1300);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1301,26,1301);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1302,27,1302),
    (1303,27,1303),
    (1304,27,1304),
    (1305,27,1305),
    (1306,27,1306),
    (1307,27,1307),
    (1308,27,1308),
    (1309,27,1309),
    (1310,27,1310),
    (1311,27,1311);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1312,27,1312),
    (1313,27,1313),
    (1314,27,1314),
    (1315,27,1315),
    (1316,27,1316),
    (1317,27,1317),
    (1318,27,1318),
    (1319,27,1319),
    (1320,27,1320),
    (1321,27,1321);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1322,27,1322),
    (1323,27,1323),
    (1324,27,1324),
    (1325,27,1325),
    (1326,27,1326),
    (1327,27,1327),
    (1328,27,1328),
    (1329,27,1329),
    (1330,27,1330),
    (1331,27,1331);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1332,27,1332),
    (1333,27,1333),
    (1334,27,1334),
    (1335,27,1335),
    (1336,27,1336),
    (1337,27,1337),
    (1338,27,1338),
    (1339,27,1339),
    (1340,27,1340),
    (1341,27,1341);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1342,27,1342),
    (1343,27,1343),
    (1344,27,1344),
    (1345,27,1345),
    (1346,27,1346),
    (1347,27,1347),
    (1348,27,1348),
    (1349,27,1349),
    (1350,27,1350);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1351,28,1351),
    (1352,28,1352),
    (1353,28,1353),
    (1354,28,1354),
    (1355,28,1355),
    (1356,28,1356),
    (1357,28,1357),
    (1358,28,1358),
    (1359,28,1359),
    (1360,28,1360);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1361,28,1361),
    (1362,28,1362),
    (1363,28,1363),
    (1364,28,1364),
    (1365,28,1365),
    (1366,28,1366),
    (1367,28,1367),
    (1368,28,1368),
    (1369,28,1369),
    (1370,28,1370);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1371,28,1371),
    (1372,28,1372),
    (1373,28,1373),
    (1374,28,1374),
    (1375,28,1375),
    (1376,28,1376),
    (1377,28,1377),
    (1378,28,1378),
    (1379,28,1379),
    (1380,28,1380);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1381,28,1381),
    (1382,28,1382),
    (1383,28,1383),
    (1384,28,1384),
    (1385,28,1385),
    (1386,28,1386),
    (1387,28,1387),
    (1388,28,1388),
    (1389,28,1389),
    (1390,28,1390);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1391,28,1391),
    (1392,28,1392),
    (1393,28,1393),
    (1394,28,1394),
    (1395,28,1395),
    (1396,28,1396),
    (1397,28,1397),
    (1398,28,1398),
    (1399,28,1399),
    (1400,28,1400);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1401,28,1401),
    (1402,28,1402),
    (1403,28,1403),
    (1404,28,1404),
    (1405,28,1405),
    (1406,28,1406),
    (1407,28,1407),
    (1408,28,1408),
    (1409,28,1409),
    (1410,28,1410);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1411,28,1411),
    (1412,28,1412),
    (1413,28,1413),
    (1414,28,1414),
    (1415,28,1415),
    (1416,28,1416),
    (1417,28,1417),
    (1418,28,1418),
    (1419,28,1419),
    (1420,28,1420);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1421,28,1421),
    (1422,28,1422),
    (1423,28,1423),
    (1424,28,1424),
    (1425,28,1425);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1426,29,1426),
    (1427,29,1427),
    (1428,29,1428),
    (1429,29,1429),
    (1430,29,1430),
    (1431,29,1431),
    (1432,29,1432),
    (1433,29,1433),
    (1434,29,1434),
    (1435,29,1435);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1436,29,1436),
    (1437,29,1437),
    (1438,29,1438),
    (1439,29,1439),
    (1440,29,1440),
    (1441,29,1441),
    (1442,29,1442),
    (1443,29,1443),
    (1444,29,1444),
    (1445,29,1445);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1446,29,1446),
    (1447,29,1447),
    (1448,29,1448),
    (1449,29,1449),
    (1450,29,1450);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1451,30,1451),
    (1452,30,1452),
    (1453,30,1453),
    (1454,30,1454),
    (1455,30,1455),
    (1456,30,1456),
    (1457,30,1457),
    (1458,30,1458),
    (1459,30,1459),
    (1460,30,1460);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1461,30,1461),
    (1462,30,1462),
    (1463,30,1463),
    (1464,30,1464),
    (1465,30,1465),
    (1466,30,1466),
    (1467,30,1467),
    (1468,30,1468),
    (1469,30,1469),
    (1470,30,1470);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1471,30,1471),
    (1472,30,1472),
    (1473,30,1473),
    (1474,30,1474),
    (1475,30,1475),
    (1476,30,1476),
    (1477,30,1477),
    (1478,30,1478),
    (1479,30,1479),
    (1480,30,1480);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1481,30,1481),
    (1482,30,1482),
    (1483,30,1483),
    (1484,30,1484),
    (1485,30,1485),
    (1486,30,1486),
    (1487,30,1487),
    (1488,30,1488),
    (1489,30,1489),
    (1490,30,1490);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1491,30,1491),
    (1492,30,1492),
    (1493,30,1493),
    (1494,30,1494),
    (1495,30,1495),
    (1496,30,1496),
    (1497,30,1497),
    (1498,30,1498),
    (1499,30,1499),
    (1500,30,1500);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1501,1,1051),
    (1502,1,1052),
    (1503,1,1053),
    (1504,1,1054),
    (1505,1,1055),
    (1506,1,1056),
    (1507,1,1057),
    (1508,1,1058),
    (1509,1,1059),
    (1510,1,1060);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1511,1,1061),
    (1512,1,1062),
    (1513,1,1063),
    (1514,1,1064),
    (1515,1,1065),
    (1516,1,1066),
    (1517,1,1067),
    (1518,1,1068),
    (1519,1,1069),
    (1520,1,1070);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1521,1,1071),
    (1522,1,1072),
    (1523,1,1073),
    (1524,1,1074),
    (1525,1,1075),
    (1526,1,1076),
    (1527,1,1077),
    (1528,1,1078),
    (1529,1,1079),
    (1530,1,1080);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1531,1,1081),
    (1532,1,1082),
    (1533,1,1083),
    (1534,1,1084),
    (1535,1,1085),
    (1536,1,1086),
    (1537,1,1087),
    (1538,1,1088),
    (1539,1,1089),
    (1540,1,1090);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1541,1,1091),
    (1542,1,1092),
    (1543,1,1093),
    (1544,1,1094),
    (1545,1,1095),
    (1546,1,1096),
    (1547,1,1097),
    (1548,1,1098),
    (1549,1,1099),
    (1550,1,1100);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1551,1,1101),
    (1552,1,1102),
    (1553,1,1103),
    (1554,1,1104),
    (1555,1,1105),
    (1556,1,1106),
    (1557,1,1107),
    (1558,1,1108),
    (1559,1,1109),
    (1560,1,1110);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1561,1,1111),
    (1562,1,1112),
    (1563,1,1113),
    (1564,1,1114),
    (1565,1,1115),
    (1566,1,1116),
    (1567,1,1117),
    (1568,1,1118),
    (1569,1,1119),
    (1570,1,1120);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1571,1,1121),
    (1572,1,1122),
    (1573,1,1123),
    (1574,1,1124),
    (1575,1,1125),
    (1576,1,1126),
    (1577,1,1127),
    (1578,1,1128),
    (1579,1,1129),
    (1580,1,1130);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1581,1,1131),
    (1582,1,1132),
    (1583,1,1133),
    (1584,1,1134),
    (1585,1,1135),
    (1586,1,1136),
    (1587,1,1137),
    (1588,1,1138),
    (1589,1,1139),
    (1590,1,1140);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1591,1,1141),
    (1592,1,1142),
    (1593,1,1143),
    (1594,1,1144),
    (1595,1,1145),
    (1596,1,1146),
    (1597,1,1147),
    (1598,1,1148),
    (1599,1,1149),
    (1600,1,1150);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1601,1,1151),
    (1602,1,1152),
    (1603,1,1153),
    (1604,1,1154),
    (1605,1,1155),
    (1606,1,1156),
    (1607,1,1157),
    (1608,1,1158),
    (1609,1,1159),
    (1610,1,1160);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1611,1,1161),
    (1612,1,1162),
    (1613,1,1163),
    (1614,1,1164),
    (1615,1,1165),
    (1616,1,1166),
    (1617,1,1167),
    (1618,1,1168),
    (1619,1,1169),
    (1620,1,1170);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1621,1,1171),
    (1622,1,1172),
    (1623,1,1173),
    (1624,1,1174),
    (1625,1,1175),
    (1626,1,1176),
    (1627,1,1177),
    (1628,1,1178),
    (1629,1,1179),
    (1630,1,1180);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1631,1,1181),
    (1632,1,1182),
    (1633,1,1183),
    (1634,1,1184),
    (1635,1,1185),
    (1636,1,1186),
    (1637,1,1187),
    (1638,1,1188),
    (1639,1,1189),
    (1640,1,1190);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1641,1,1191),
    (1642,1,1192),
    (1643,1,1193),
    (1644,1,1194),
    (1645,1,1195),
    (1646,1,1196),
    (1647,1,1197),
    (1648,1,1198),
    (1649,1,1199),
    (1650,1,1200);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1651,1,1201),
    (1652,1,1202),
    (1653,1,1203),
    (1654,1,1204),
    (1655,1,1205),
    (1656,1,1206),
    (1657,1,1207),
    (1658,1,1208),
    (1659,1,1209),
    (1660,1,1210);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1661,1,1211),
    (1662,1,1212),
    (1663,1,1213),
    (1664,1,1214),
    (1665,1,1215),
    (1666,1,1216),
    (1667,1,1217),
    (1668,1,1218),
    (1669,1,1219),
    (1670,1,1220);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1671,1,1221),
    (1672,1,1222),
    (1673,1,1223),
    (1674,1,1224),
    (1675,1,1225),
    (1676,1,1226),
    (1677,1,1227),
    (1678,1,1228),
    (1679,1,1229),
    (1680,1,1230);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1681,1,1231),
    (1682,1,1232),
    (1683,1,1233),
    (1684,1,1234),
    (1685,1,1235),
    (1686,1,1236),
    (1687,1,1237),
    (1688,1,1238),
    (1689,1,1239),
    (1690,1,1240);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1691,1,1241),
    (1692,1,1242),
    (1693,1,1243),
    (1694,1,1244),
    (1695,1,1245),
    (1696,1,1246),
    (1697,1,1247),
    (1698,1,1248),
    (1699,1,1249),
    (1700,1,1250);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1701,2,1251),
    (1702,2,1252),
    (1703,2,1253),
    (1704,2,1254),
    (1705,2,1255),
    (1706,2,1256),
    (1707,2,1257),
    (1708,2,1258),
    (1709,2,1259),
    (1710,2,1260);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1711,2,1261),
    (1712,2,1262),
    (1713,2,1263),
    (1714,2,1264),
    (1715,2,1265),
    (1716,2,1266),
    (1717,2,1267),
    (1718,2,1268),
    (1719,2,1269),
    (1720,2,1270);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1721,2,1271),
    (1722,2,1272),
    (1723,2,1273),
    (1724,2,1274),
    (1725,2,1275),
    (1726,2,1276),
    (1727,2,1277),
    (1728,2,1278),
    (1729,2,1279),
    (1730,2,1280);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1731,2,1281),
    (1732,2,1282),
    (1733,2,1283),
    (1734,2,1284),
    (1735,2,1285),
    (1736,2,1286),
    (1737,2,1287),
    (1738,2,1288),
    (1739,2,1289),
    (1740,2,1290);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1741,2,1291),
    (1742,2,1292),
    (1743,2,1293),
    (1744,2,1294),
    (1745,2,1295),
    (1746,2,1296),
    (1747,2,1297),
    (1748,2,1298),
    (1749,2,1299),
    (1750,2,1300);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1751,3,1301),
    (1752,3,1302),
    (1753,3,1303),
    (1754,3,1304),
    (1755,3,1305),
    (1756,3,1306),
    (1757,3,1307),
    (1758,3,1308),
    (1759,3,1309),
    (1760,3,1310);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1761,3,1311),
    (1762,3,1312),
    (1763,3,1313),
    (1764,3,1314),
    (1765,3,1315),
    (1766,3,1316),
    (1767,3,1317),
    (1768,3,1318),
    (1769,3,1319),
    (1770,3,1320);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1771,3,1321),
    (1772,3,1322),
    (1773,3,1323),
    (1774,3,1324),
    (1775,3,1325),
    (1776,3,1326),
    (1777,3,1327),
    (1778,3,1328),
    (1779,3,1329),
    (1780,3,1330);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1781,3,1331),
    (1782,3,1332),
    (1783,3,1333),
    (1784,3,1334),
    (1785,3,1335),
    (1786,3,1336),
    (1787,3,1337),
    (1788,3,1338),
    (1789,3,1339),
    (1790,3,1340);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1791,3,1341),
    (1792,3,1342),
    (1793,3,1343),
    (1794,3,1344),
    (1795,3,1345),
    (1796,3,1346),
    (1797,3,1347),
    (1798,3,1348),
    (1799,3,1349),
    (1800,3,1350);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1801,4,1351),
    (1802,4,1352),
    (1803,4,1353),
    (1804,4,1354),
    (1805,4,1355),
    (1806,4,1356),
    (1807,4,1357),
    (1808,4,1358),
    (1809,4,1359),
    (1810,4,1360);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1811,4,1361),
    (1812,4,1362),
    (1813,4,1363),
    (1814,4,1364),
    (1815,4,1365),
    (1816,4,1366),
    (1817,4,1367),
    (1818,4,1368),
    (1819,4,1369),
    (1820,4,1370);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1821,4,1371),
    (1822,4,1372),
    (1823,4,1373),
    (1824,4,1374),
    (1825,4,1375),
    (1826,4,1376),
    (1827,4,1377),
    (1828,4,1378),
    (1829,4,1379),
    (1830,4,1380);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1831,4,1381),
    (1832,4,1382),
    (1833,4,1383),
    (1834,4,1384),
    (1835,4,1385),
    (1836,4,1386),
    (1837,4,1387),
    (1838,4,1388),
    (1839,4,1389),
    (1840,4,1390);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1841,4,1391),
    (1842,4,1392),
    (1843,4,1393),
    (1844,4,1394),
    (1845,4,1395),
    (1846,4,1396),
    (1847,4,1397),
    (1848,4,1398),
    (1849,4,1399),
    (1850,4,1400);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1851,4,1401),
    (1852,4,1402),
    (1853,4,1403),
    (1854,4,1404),
    (1855,4,1405),
    (1856,4,1406),
    (1857,4,1407),
    (1858,4,1408),
    (1859,4,1409),
    (1860,4,1410);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1861,4,1411),
    (1862,4,1412),
    (1863,4,1413),
    (1864,4,1414),
    (1865,4,1415),
    (1866,4,1416),
    (1867,4,1417),
    (1868,4,1418),
    (1869,4,1419),
    (1870,4,1420);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1871,4,1421),
    (1872,4,1422),
    (1873,4,1423),
    (1874,4,1424),
    (1875,4,1425);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1876,5,1426),
    (1877,5,1427),
    (1878,5,1428),
    (1879,5,1429),
    (1880,5,1430),
    (1881,5,1431),
    (1882,5,1432),
    (1883,5,1433),
    (1884,5,1434),
    (1885,5,1435);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1886,5,1436),
    (1887,5,1437),
    (1888,5,1438),
    (1889,5,1439),
    (1890,5,1440),
    (1891,5,1441),
    (1892,5,1442),
    (1893,5,1443),
    (1894,5,1444),
    (1895,5,1445);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1896,5,1446),
    (1897,5,1447),
    (1898,5,1448),
    (1899,5,1449),
    (1900,5,1450);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1901,6,1451),
    (1902,6,1452),
    (1903,6,1453),
    (1904,6,1454),
    (1905,6,1455),
    (1906,6,1456),
    (1907,6,1457),
    (1908,6,1458),
    (1909,6,1459),
    (1910,6,1460);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1911,6,1461),
    (1912,6,1462),
    (1913,6,1463),
    (1914,6,1464),
    (1915,6,1465),
    (1916,6,1466),
    (1917,6,1467),
    (1918,6,1468),
    (1919,6,1469),
    (1920,6,1470);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1921,6,1471),
    (1922,6,1472),
    (1923,6,1473),
    (1924,6,1474),
    (1925,6,1475),
    (1926,6,1476),
    (1927,6,1477),
    (1928,6,1478),
    (1929,6,1479),
    (1930,6,1480);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1931,6,1481),
    (1932,6,1482),
    (1933,6,1483),
    (1934,6,1484),
    (1935,6,1485),
    (1936,6,1486),
    (1937,6,1487),
    (1938,6,1488),
    (1939,6,1489),
    (1940,6,1490);
INSERT INTO relationships (id,professor_id,student_id)
VALUES
    (1941,6,1491),
    (1942,6,1492),
    (1943,6,1493),
    (1944,6,1494),
    (1945,6,1495),
    (1946,6,1496),
    (1947,6,1497),
    (1948,6,1498),
    (1949,6,1499),
    (1950,6,1500);


