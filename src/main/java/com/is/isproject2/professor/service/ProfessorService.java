package com.is.isproject2.professor.service;

import com.is.isproject2.professor.api.payloads.ProfessorRequest;
import com.is.isproject2.professor.database.ProfessorDatabaseService;
import com.is.isproject2.professor.model.Professor;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Log4j2
@AllArgsConstructor
@Service
public class ProfessorService {

    private final ProfessorDatabaseService professorDatabaseService;

    public Flux<Professor> getAllProfessors() {

        log.info("Getting all professors");

        return professorDatabaseService.getAllProfessors();
    }

    public Mono<Professor> getProfessorById(Integer id) {

        log.info("Getting professor by id");

        return professorDatabaseService.getProfessorById(id);
    }

    public Mono<Professor> createProfessor(ProfessorRequest professorRequest) {

        log.info("Creating professor with payload {}", professorRequest);

        final var professor = Professor.builder()
                .name(professorRequest.getName())
                .build();

        return professorDatabaseService.createProfessor(professor);
    }

    public Mono<Professor> updateProfessorById(Integer id, ProfessorRequest professorRequest) {

        log.info("Updating professor with payload {}", professorRequest);

        final var professor = Professor.builder()
                .name(professorRequest.getName())
                .build();

        return professorDatabaseService
                .getProfessorById(id)
                .flatMap(professorDatabase -> {

                    professorDatabase.setName(professor.getName());

                    return professorDatabaseService.updateProfessorById(id, professorDatabase);
                });
    }

    public Mono<Void> deleteProfessorById(Integer id) {

        log.info("Deleting professor with id {}", id);

        return professorDatabaseService.deleteProfessorById(id);
    }
}
