package com.is.isproject2.professor.api;

import com.is.isproject2.professor.api.payloads.ProfessorRequest;
import com.is.isproject2.professor.model.Professor;
import com.is.isproject2.professor.service.ProfessorService;
import com.is.isproject2.relationship.service.RelationshipService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping("/professors")
public class ProfessorController {

    private final ProfessorService professorService;
    private final RelationshipService relationshipService;

    @GetMapping()
    public Flux<Professor> getAllProfessors() {

        log.info("Received request to get all professors");

        return professorService
                .getAllProfessors()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .doOnNext(professors -> log.trace("Successfully requested all professors {}", professors));
    }

    @GetMapping("/{id}")
    public Mono<Professor> getProfessorById(
            @PathVariable Integer id
    ) {

        log.info("Received request to get professor by id {}", id);

        return professorService
                .getProfessorById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .doOnNext(professor -> log.trace("Successfully requested professor by id {}", professor));
    }

    @PostMapping()
    public Mono<Professor> createProfessor(
            @RequestBody ProfessorRequest professorRequest
    ) {

        log.info("Received request to create professor {}", professorRequest);

        return professorService
                .createProfessor(professorRequest)
                .doOnNext(professor -> log.trace("Successfully created professor {}", professor));
    }

    @PutMapping("/{id}")
    public Mono<Professor> updateProfessorById(
            @PathVariable Integer id,
            @RequestBody ProfessorRequest professorRequest
    ) {

        log.info("Received request to update professor with id {} and payload {}", id, professorRequest);

        return professorService
                .updateProfessorById(id, professorRequest)
                .doOnNext(professor -> log.trace("Successfully updated professor {}", professor));
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteProfessorById(
            @PathVariable Integer id
    ) {

        log.info("Received request to delete professor with id {}", id);

        return relationshipService
                .getAllRelationships()
                .map(relationship -> {
                    if (relationship.getProfessorId().equals(id))
                        throw new RuntimeException();
                    else return relationship;
                })
                .flatMap(relationship -> professorService
                        .deleteProfessorById(id)
                        .doOnError(throwable -> {
                            throw new RuntimeException(throwable);
                        })
                        .doOnNext(unused -> log.trace("Successfully deleted professor")))
                .then();

    }
}
