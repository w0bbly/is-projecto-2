package com.is.isproject2.professor.api.payloads;

import lombok.Data;

@Data
public class ProfessorRequest {
    String name;
}
