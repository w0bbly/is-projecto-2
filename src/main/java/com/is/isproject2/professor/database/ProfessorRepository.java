package com.is.isproject2.professor.database;

import com.is.isproject2.professor.model.Professor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProfessorRepository extends ReactiveCrudRepository<Professor, Integer> {
}
