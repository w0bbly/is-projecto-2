package com.is.isproject2.professor.database;

import com.is.isproject2.professor.model.Professor;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Log4j2
@Service
@AllArgsConstructor
public class ProfessorDatabaseService {

    private final ProfessorRepository professorRepository;

    public Flux<Professor> getAllProfessors() {

        log.info("Getting all professors from the database");

        return professorRepository.findAll()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Professor> getProfessorById(Integer id) {

        log.info("Getting professor by id {} from the database", id);

        return professorRepository.findById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Professor> createProfessor(Professor professor) {

        log.info("Creating professor with payload {} on the database", professor);

        return professorRepository.save(professor);
    }

    public Mono<Professor> updateProfessorById(Integer id, Professor professor) {

        log.info("Updating professor with id {} and payload {} on the database", id, professor);

        return professorRepository.save(professor);
    }

    public Mono<Void> deleteProfessorById(Integer id) {

        log.info("Deleting professor with id {} on the database", id);

        return professorRepository.deleteById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }
}
