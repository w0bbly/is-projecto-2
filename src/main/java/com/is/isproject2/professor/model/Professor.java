package com.is.isproject2.professor.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table("professors")
public class Professor {

    @Id
    @Column("id")
    Integer id;

    @Column("name")
    String name;
}
