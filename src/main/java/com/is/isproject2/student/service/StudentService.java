package com.is.isproject2.student.service;

import com.is.isproject2.student.api.payloads.StudentRequest;
import com.is.isproject2.student.database.StudentDatabaseService;
import com.is.isproject2.student.model.Student;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Log4j2
@AllArgsConstructor
@Service
public class StudentService {

    private final StudentDatabaseService studentDatabaseService;

    public Flux<Student> getAllStudents() {

        log.info("Getting all students");

        return studentDatabaseService.getAllStudents()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Student> getStudentById(Integer id) {

        log.info("Getting student by id");

        return studentDatabaseService.getStudentById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Student> createStudent(StudentRequest studentRequest) {

        log.info("Creating student with payload {}", studentRequest);

        final var student = Student.builder()
                .name(studentRequest.getName())
                .averageGrade(studentRequest.getAverageGrade())
                .birthDate(studentRequest.getBirthDate())
                .completedCredits(studentRequest.getCompletedCredits())
                .build();

        return studentDatabaseService.createStudent(student);
    }

    public Mono<Student> updateStudentById(Integer id, StudentRequest studentRequest) {

        log.info("Creating student with payload {}", studentRequest);

        final var student = Student.builder()
                .name(studentRequest.getName())
                .averageGrade(studentRequest.getAverageGrade())
                .birthDate(studentRequest.getBirthDate())
                .completedCredits(studentRequest.getCompletedCredits())
                .build();

        return studentDatabaseService
                .getStudentById(id)
                .flatMap(studentDatabase -> {

                    studentDatabase.setName(student.getName());
                    studentDatabase.setBirthDate(student.getBirthDate());
                    studentDatabase.setAverageGrade(student.getAverageGrade());
                    studentDatabase.setCompletedCredits(student.getCompletedCredits());

                    return studentDatabaseService.updateStudentById(id, studentDatabase);
                });
    }

    public Mono<Void> deleteStudentById(Integer id) {

        log.info("Deleting student with id {}", id);

        return studentDatabaseService.deleteStudentById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }
}
