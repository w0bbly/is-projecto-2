package com.is.isproject2.student.api.payloads;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StudentRequest {

    String name;
    String birthDate;
    Integer completedCredits;
    BigDecimal averageGrade;
}
