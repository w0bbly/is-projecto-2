package com.is.isproject2.student.api;

import com.is.isproject2.relationship.service.RelationshipService;
import com.is.isproject2.student.api.payloads.StudentRequest;
import com.is.isproject2.student.model.Student;
import com.is.isproject2.student.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping("/students")
public class StudentController {

    private final StudentService studentService;
    private final RelationshipService relationshipService;

    @GetMapping()
    public Flux<Student> getAllStudents() {

        log.info("Received request to get all students");

        return studentService
                .getAllStudents()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .doOnNext(students -> log.trace("Successfully requested all students {}", students));
    }

    @GetMapping("/{id}")
    public Mono<Student> getStudentById(
            @PathVariable Integer id
    ) {

        log.info("Received request to get student by id {}", id);

        return studentService
                .getStudentById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .doOnNext(student -> log.trace("Successfully requested student by id {}", student));
    }

    @PostMapping()
    public Mono<Student> createStudent(
            @RequestBody StudentRequest studentRequest
    ) {

        log.info("Received request to create student {}", studentRequest);

        return studentService
                .createStudent(studentRequest)
                .doOnNext(student -> log.trace("Successfully created student {}", student));
    }

    @PutMapping("/{id}")
    public Mono<Student> updateStudentById(
            @PathVariable Integer id,
            @RequestBody StudentRequest studentRequest
    ) {

        log.info("Received request to update student with id {} and payload {}", id, studentRequest);

        return studentService
                .updateStudentById(id, studentRequest)
                .doOnNext(student -> log.trace("Successfully updated student {}", student));
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteStudentById(
            @PathVariable Integer id
    ) {

        log.info("Received request to delete student with id {}", id);

        return relationshipService
                .getAllRelationships()
                .map(relationship -> {
                    if (relationship.getStudentId().equals(id))
                        throw new RuntimeException();
                    else return relationship;
                })
                .flatMap(relationship -> studentService
                        .deleteStudentById(id)
                        .doOnError(throwable -> {
                            throw new RuntimeException(throwable);
                        })
                        .doOnNext(unused -> log.trace("Successfully deleted student")))
                .then();
    }
}
