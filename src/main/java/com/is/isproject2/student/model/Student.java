package com.is.isproject2.student.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table("students")
public class Student {

    @Id
    @Column("id")
    Integer id;

    @Column("name")
    String name;

    @Column("birth_date")
    String birthDate;

    @Column("completed_credits")
    Integer completedCredits;

    @Column("average_grade")
    BigDecimal averageGrade;
}
