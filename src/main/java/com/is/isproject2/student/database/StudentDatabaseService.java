package com.is.isproject2.student.database;

import com.is.isproject2.student.model.Student;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Log4j2
@Service
@AllArgsConstructor
public class StudentDatabaseService {

    private final StudentRepository studentRepository;

    public Flux<Student> getAllStudents() {

        log.info("Getting all students from the database");

        return studentRepository.findAll()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Student> getStudentById(Integer id) {

        log.info("Getting student by id {} from the database", id);

        return studentRepository.findById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Student> createStudent(Student student) {

        log.info("Creating student with payload {} on the database", student);

        return studentRepository.save(student);
    }

    public Mono<Student> updateStudentById(Integer id, Student student) {

        log.info("Updating student with id {} and payload {} on the database", id, student);

        return studentRepository.save(student);
    }

    public Mono<Void> deleteStudentById(Integer id) {

        log.info("Deleting student with id {} on the database", id);

        return studentRepository.deleteById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }
}
