package com.is.isproject2.relationship.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Data
@ToString
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "relationships")
public class Relationship {

    @Id
    @Column("id")
    Integer id;

    @Column("professor_id")
    Integer professorId;

    @Column("student_id")
    Integer studentId;
}
