package com.is.isproject2.relationship.api;

import com.is.isproject2.professor.service.ProfessorService;
import com.is.isproject2.relationship.api.payloads.RelationshipRequest;
import com.is.isproject2.relationship.model.Relationship;
import com.is.isproject2.relationship.service.RelationshipService;
import com.is.isproject2.student.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Log4j2
@RestController
@AllArgsConstructor
@RequestMapping("/relationships")
public class RelationshipController {

    private final RelationshipService relationshipService;
    private final StudentService studentService;
    private final ProfessorService professorService;

    @GetMapping("/delay")
    public Mono<Void> delay() {

        return Mono
                .delay(Duration.ofSeconds(20))
                .then();
    }

    @GetMapping()
    public Flux<Relationship> getAllRelationships() {

        log.info("Received request to get all relationships");

        return relationshipService
                .getAllRelationships()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .doOnNext(relationships -> log.trace("Successfully requested all relationships {}", relationships));
    }

    @GetMapping("/{id}")
    public Mono<Relationship> getRelationshipById(
            @PathVariable Integer id
    ) {

        log.info("Received request to get relationship by id {}", id);

        return relationshipService
                .getRelationshipById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .doOnNext(relationship -> log.trace("Successfully requested relationship by id {}", relationship));
    }

    @PostMapping()
    public Mono<Relationship> createRelationship(
            @RequestBody RelationshipRequest relationshipRequest
    ) {

        log.info("Received request to create relationship {}", relationshipRequest);

        return studentService
                .getStudentById(relationshipRequest.getStudentId())
                .zipWith(professorService.getProfessorById(relationshipRequest.getProfessorId()))
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .flatMap(tuple2 -> relationshipService
                        .createRelationship(relationshipRequest))
                .doOnNext(relationship -> log.trace("Successfully created relationship {}", relationship));
    }

    @DeleteMapping("/{id}")
    public Mono<Void> deleteRelationshipById(
            @PathVariable Integer id
    ) {

        log.info("Received request to delete relationship with id {}", id);

        return relationshipService
                .deleteRelationshipById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                })
                .doOnNext(unused -> log.trace("Successfully deleted relationship"));
    }
}
