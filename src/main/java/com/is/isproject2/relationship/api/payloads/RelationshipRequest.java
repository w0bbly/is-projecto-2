package com.is.isproject2.relationship.api.payloads;

import lombok.Data;

@Data
public class RelationshipRequest {
    Integer professorId;
    Integer studentId;
}
