package com.is.isproject2.relationship.service;

import com.is.isproject2.relationship.database.RelationshipDatabaseService;
import com.is.isproject2.relationship.model.Relationship;
import com.is.isproject2.relationship.api.payloads.RelationshipRequest;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@AllArgsConstructor
@Service
public class RelationshipService {

    private final RelationshipDatabaseService relationshipDatabaseService;

    public Flux<Relationship> getAllRelationships() {

        log.info("Getting all Relationships");

        return relationshipDatabaseService.getAllRelationships()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Relationship> getRelationshipById(Integer id) {

        log.info("Getting relationship by id");

        return relationshipDatabaseService.getRelationshipById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Relationship> createRelationship(RelationshipRequest relationshipRequest) {

        log.info("Creating relationship with payload {}", relationshipRequest);

        final var relationship = Relationship.builder()
                .studentId(relationshipRequest.getStudentId())
                .professorId(relationshipRequest.getProfessorId())
                .build();

        return relationshipDatabaseService.createRelationship(relationship);
    }

    public Mono<Void> deleteRelationshipById(Integer id) {

        log.info("Deleting relationship with id {}", id);

        return relationshipDatabaseService.deleteRelationshipById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }
}
