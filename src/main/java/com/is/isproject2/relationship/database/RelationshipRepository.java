package com.is.isproject2.relationship.database;

import com.is.isproject2.relationship.model.Relationship;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelationshipRepository extends ReactiveCrudRepository<Relationship, Integer> {
}
