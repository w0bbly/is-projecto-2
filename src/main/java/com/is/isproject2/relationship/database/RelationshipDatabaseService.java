package com.is.isproject2.relationship.database;

import com.is.isproject2.relationship.model.Relationship;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Log4j2
@Service
@AllArgsConstructor
public class RelationshipDatabaseService {

    private final RelationshipRepository relationshipRepository;

    public Flux<Relationship> getAllRelationships() {

        log.info("Getting all relationships from the database");

        return relationshipRepository.findAll()
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Relationship> getRelationshipById(Integer id) {

        log.info("Getting relationship by id {} from the database", id);

        return relationshipRepository.findById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }

    public Mono<Relationship> createRelationship(Relationship relationship) {

        log.info("Creating relationship with payload {} on the database", relationship);

        return relationshipRepository.save(relationship);
    }

    public Mono<Void> deleteRelationshipById(Integer id) {

        log.info("Deleting relationship with id {} on the database", id);

        return relationshipRepository.deleteById(id)
                .doOnError(throwable -> {
                    throw new RuntimeException(throwable);
                });
    }
}
